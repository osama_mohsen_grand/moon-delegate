package grand.app.moondelegate.map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.maputils.permission.MapPermissionFragment;

public class LocationLatLng {


    @SuppressLint("MissingPermission")
    public static void getLocation(Activity context, Fragment fragment , FetchLocationListener fetchLocationListener) {
        MapPermissionFragment mapPermission = new MapPermissionFragment(fragment);


        LocationManager locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);


        LocationListener locationListener = new MyLocationListener(fetchLocationListener);
        if (mapPermission.validLocationPermission()) {
            mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST);
            return;
        }
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 5000, 10, (locationListener));

    }


    private static class MyLocationListener implements LocationListener {

        FetchLocationListener fetchLocationListener;
        public MyLocationListener(FetchLocationListener fetchLocationListener) {
            this.fetchLocationListener = fetchLocationListener;
        }

        @Override
        public void onLocationChanged(Location location) {
            fetchLocationListener.location(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }


}
