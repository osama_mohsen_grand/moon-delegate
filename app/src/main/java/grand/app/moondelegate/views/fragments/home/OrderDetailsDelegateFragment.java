package grand.app.moondelegate.views.fragments.home;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.OrderDetailsDelegateAdapter;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentOrderDetailsDelegateBinding;
import grand.app.moondelegate.models.app.AccountTypeModel;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.models.order.OrderConfirmRequest;
import grand.app.moondelegate.models.order.details.OrderDetails;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.PopUp.PopUpInterface;
import grand.app.moondelegate.utils.PopUp.PopUpMenuHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.viewmodels.order.delegate.details.OrderDetailsDelegateViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.activities.MapAddressActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailsDelegateFragment extends BaseFragment {

    View rootView;
    private FragmentOrderDetailsDelegateBinding fragmentOrderDetailsBinding;
    private OrderDetailsDelegateViewModel orderDetailsViewModel;
    OrderDetailsDelegateAdapter orderDetailsAdapter;
    int order_id = 0, order_status_id = 0;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentOrderDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_details_delegate, container, false);

        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            order_id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.STATUS))
            order_status_id = getArguments().getInt(Constants.STATUS);
    }

    private void bind() {
        orderDetailsViewModel = new OrderDetailsDelegateViewModel(order_id, order_status_id);
        fragmentOrderDetailsBinding.setOrderDetailsViewModel(orderDetailsViewModel);
        AppUtils.initVerticalRV(fragmentOrderDetailsBinding.rvOrderDetails, fragmentOrderDetailsBinding.rvOrderDetails.getContext(), 1);
        rootView = fragmentOrderDetailsBinding.getRoot();
    }

    private void setEvent() {
        orderDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, orderDetailsViewModel.getOrderRepository().getMessage());
                if(orderDetailsViewModel.getOrderRepository().getStatus() == Constants.RESPONSE_401){
                    toastInfo(orderDetailsViewModel.getOrderRepository().getMessage());
                    restartApp();
                }
                if (action.equals(Constants.ORDER_DETAILS)) {
                    orderDetailsAdapter = new OrderDetailsDelegateAdapter(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.products);
                    fragmentOrderDetailsBinding.rvOrderDetails.setAdapter(orderDetailsAdapter);
                    if (orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate != null)
                        orderDetailsViewModel.haveDelegate = true;
                    if(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.statusId <= 2)
                        orderDetailsViewModel.showStatus = false;
                    setStatus();
                    orderDetailsViewModel.setStatusChat();
                    orderDetailsViewModel.showPage(true);
                } else if (action.equals(Constants.ORDER_SUBMIT)) {
                    reloadPrevious();
                    toastMessage(orderDetailsViewModel.getOrderRepository().getMessage());
                    //                    setStatus();
                    int status = orderDetailsViewModel.orderConfirmRequest.status;
                    orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.statusId = orderDetailsViewModel.orderConfirmRequest.status;

                    if (status <= 2) {
                        getActivityBase().finishAffinity();
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra(Constants.TYPE, Constants.ORDERS);
                        startActivity(intent);
                    } else if (status == 4) {
                        orderDetailsViewModel.showStatus = false;
                        orderDetailsViewModel.setStatusChat();
                        orderDetailsViewModel.notifyChange();
                    } else {
                        setStatus();
                        orderDetailsViewModel.setStatusChat();
                        orderDetailsViewModel.notifyChange();
                    }

//                    orderDetailsViewModel.showStatus = false;
//                    orderDetailsViewModel.notifyChange();

                } else if (action.equals(Constants.DELEGATE_LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lat));
                    intent.putExtra(Constants.LNG, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lng));
                    startActivity(intent);
                } else if (action.equals(Constants.SHOP_LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.shop.lat));
                    intent.putExtra(Constants.LNG, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.shop.lng));
                    startActivity(intent);
                }else if (action.equals(Constants.CHAT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    OrderDetails orderDetails = orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data;
                    bundle.putInt(Constants.ID,orderDetails.orderId);
                    bundle.putInt(Constants.RECEIVER_ID,orderDetails.delegate.id);
                    bundle.putInt(Constants.TYPE,orderDetails.delegate.type);
                    bundle.putString(Constants.NAME,orderDetails.delegate.name);
                    bundle.putString(Constants.IMAGE,orderDetails.delegate.image);
                    boolean allowChat = (orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.statusId==4 ? false : true);
                    bundle.putBoolean(Constants.ALLOW_CHAT,allowChat);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
            }
        });
    }

    private void setStatus() {
        ArrayList<AccountTypeModel> statusData = AppMoon.getStatus(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.statusId);
        if (statusData.size() > 0) {
            orderDetailsViewModel.showStatus = true;
            ArrayList<String> popUpModels = new ArrayList<>();
            for (AccountTypeModel accountTypeModel : statusData)
                popUpModels.add(accountTypeModel.type);
            fragmentOrderDetailsBinding.edtOrderStatus.setText(statusData.get(0).type);
            orderDetailsViewModel.orderConfirmRequest.status = Integer.parseInt(statusData.get(0).id);

            fragmentOrderDetailsBinding.edtOrderStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popUpMenuHelper.openPopUp(getActivity(), fragmentOrderDetailsBinding.edtOrderStatus, popUpModels, new PopUpInterface() {
                        @Override
                        public void submitPopUp(int position) {
                            fragmentOrderDetailsBinding.edtOrderStatus.setText(statusData.get(position).type);
                            orderDetailsViewModel.orderConfirmRequest.status = Integer.parseInt(statusData.get(position).id);
                        }
                    });
                }
            });
            orderDetailsViewModel.notifyChange();
        } else {
            orderDetailsViewModel.showStatus = false;
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (orderDetailsViewModel != null) orderDetailsViewModel.reset();
    }


}
