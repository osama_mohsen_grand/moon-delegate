package grand.app.moondelegate.views.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentOpenLocationBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.maputils.location.GPSAllowListener;
import grand.app.moondelegate.utils.maputils.location.GPSLocation;
import grand.app.moondelegate.utils.maputils.location.LocationChangeListener;
import grand.app.moondelegate.utils.maputils.location.LocationLatLng;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.viewmodels.location.OpenLocationViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.activities.MapAddressActivity;
import timber.log.Timber;

import static android.content.Context.LOCATION_SERVICE;


public class OpenLocationFragment extends BaseFragment {
    private FragmentOpenLocationBinding fragmentOpenLocationBinding;
    private OpenLocationViewModel openLocationViewModel;
    LocationLatLng locationLatLng;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentOpenLocationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_open_location, container, false);
        bind();
        return fragmentOpenLocationBinding.getRoot();
    }


    private void bind() {
        openLocationViewModel = new OpenLocationViewModel();
        setEvents();
        fragmentOpenLocationBinding.setOpenLocationViewModel(openLocationViewModel);
    }

    private void setEvents() {
        openLocationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.LOCATION_ENABLE)) {
                    enableLocation();
//                    ((ParentActivity) context).finishAffinity();
//                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });

    }

    public void enableLocation() {
        GPSLocation.EnableGPSAutoMatically(context, new GPSAllowListener() {
            @Override
            public void GPSStatus(boolean isOpen) {
//                ((ParentActivity) context).finishAffinity();
//                startActivity(new Intent(context, MainActivity.class));
                if (isOpen) {
                    Timber.e("start here");
                    locationLatLng = new LocationLatLng(getActivityBase(), OpenLocationFragment.this);
                    locationLatLng.getLocation(new LocationChangeListener() {
                        @Override
                        public void select(String address, LatLng latLng) {
                            if (latLng != null) {
                                Timber.e("start here fetch done");
                                UserHelper.saveKey(Constants.LAT, "" + latLng.latitude);
                                UserHelper.saveKey(Constants.LNG, "" + latLng.longitude);
                                Timber.e("lat:" + latLng.latitude);
                                Timber.e("lng:" + latLng.longitude);
                                UserHelper.saveKey(Constants.USER_ADDRESS, address);
                                goToPage();
                            }
                        }

                        @Override
                        public void error() {
                            Intent intent = new Intent(context, MapAddressActivity.class);
                            startActivityForResult(intent, Constants.ADDRESS_RESULT);
                        }
                    });
                } else {
                    Timber.e("location GPS disable");
                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed

        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            UserHelper.saveKey(Constants.LAT, "" + data.getDoubleExtra("lat", 0));
            UserHelper.saveKey(Constants.LNG, "" + data.getDoubleExtra("lng", 0));
            String address = locationLatLng.getAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
            UserHelper.saveKey(Constants.USER_ADDRESS, address);
            goToPage();
        }
    }


    public void goToPage(){
        if (UserHelper.getUserId() == -1) {
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.LOGIN);
            startActivity(intent);
        } else {
            ((ParentActivity) context).finishAffinity();
            startActivity(new Intent(context, MainActivity.class));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragmentOpenLocationBinding = null;
        locationLatLng = null;
        openLocationViewModel.reset();
    }
}
