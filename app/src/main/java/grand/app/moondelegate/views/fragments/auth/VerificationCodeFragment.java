package grand.app.moondelegate.views.fragments.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentVerificationCodeBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.user.VerificationCodeViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import timber.log.Timber;

public class VerificationCodeFragment extends BaseFragment {
    View rootView;
    private FragmentVerificationCodeBinding fragmentVerificationCodeBinding;
    private VerificationCodeViewModel verificationCodeViewModel;
    public String type= "",phone="", verify_id = "";
    ArrayList<String> imagesPath;
    ArrayList<String> imagesKey;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentVerificationCodeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_verification_code, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private static final String TAG = "VerificationCodeFragmen";
    private void getData() {
        if(getArguments()!= null && getArguments().containsKey(Constants.TYPE)){
            type = getArguments().getString(Constants.TYPE);
            Timber.e("TYPE page");
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {
            phone = getArguments().getString(Constants.PHONE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.VERIFY_ID)) {
            verify_id = getArguments().getString(Constants.VERIFY_ID);
        }
    }

    private void bind() {
        verificationCodeViewModel = new VerificationCodeViewModel(verify_id);
        fragmentVerificationCodeBinding.setVerificationCodeViewModel(verificationCodeViewModel);
        rootView = fragmentVerificationCodeBinding.getRoot();
    }

    private void setEvent() {
        verificationCodeViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
//                if(verificationCodeViewModel.getVerificationFirebaseSMSRepository().isSetValid())
                handleActions(action,verificationCodeViewModel.getVerificationFirebaseSMSRepository().getMessage());
//                if(verificationCodeViewModel.getLoginRepository().isSetValid())
                handleActions(action, verificationCodeViewModel.getRegisterRepository().getMessage());
                Bundle bundle = new Bundle();
                assert action != null;
                if(action.equals(Constants.SUCCESS)) {
                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    if(type.equals(Constants.FORGET_PASSWORD)) {
                        Timber.e("FORGET_PASSWORD page");
                        intent.putExtra(Constants.PAGE,Constants.NEW_PASSWORD);
                        bundle.putString(Constants.TYPE,type);
                        bundle.putString(Constants.NAME_BAR,"");
                        intent.putExtra(Constants.BUNDLE,bundle);
                        startActivity(intent);
                    }else if(type.equals(Constants.REGISTRATION)){
                        Timber.e("REGISTRATION page");
                        intent.putExtra(Constants.PAGE,Constants.REGISTRATION);
                        bundle.putString(Constants.PHONE,phone);
                        intent.putExtra(Constants.BUNDLE,bundle);
                        ((ParentActivity)context).finish();
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        verificationCodeViewModel.reset();

    }

}
