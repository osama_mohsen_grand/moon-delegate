package grand.app.moondelegate.views.fragments.base;


import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentVideoBinding;
import grand.app.moondelegate.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends BaseFragment {


    FragmentVideoBinding fragmentVideoBinding;
    public String video = "";
    SimpleExoPlayer player;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentVideoBinding =   DataBindingUtil.inflate(inflater,R.layout.fragment_video, container, false);
        getData();
//        Timber.e("video:"+video);
//        fragmentVideoBinding.video.setVideoPath(video);
//        fragmentVideoBinding.video.start();

        player = ExoPlayerFactory.newSimpleInstance(getActivityBase());
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                .createMediaSource(Uri.parse(video));
        fragmentVideoBinding.playerView.setPlayer(player);
        player.setPlayWhenReady(true);
        player.prepare(mediaSource, true, false);

        return fragmentVideoBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.VIDEO)){
            video = getArguments().getString(Constants.VIDEO);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(player != null) {
            player.stop();
            player.release();
            player.setPlayWhenReady(false);
        }
        player = null;
    }

}
