package grand.app.moondelegate.views.fragments.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.utils.L;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.OrderDelegateAdapter;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentSplashBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.maputils.location.GPSLocation;
import grand.app.moondelegate.utils.maputils.location.LocationChangeListener;
import grand.app.moondelegate.utils.maputils.location.LocationLatLng;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.viewmodels.common.SplashViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.activities.TrackingActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class SplashFragment extends BaseFragment {
    private FragmentSplashBinding fragmentSplashBinding;
    private SplashViewModel splashViewModel;
    Thread splashThread;
    Disposable disposable;
    boolean locationIsOpen = false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentSplashBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false);
        bind();
        return fragmentSplashBinding.getRoot();
    }

    private void bind() {
        splashViewModel = new SplashViewModel();
        if(splashViewModel.isDelegate()){
            thread();
        }else
            setEventListeners();
        fragmentSplashBinding.setSplashViewModel(splashViewModel);
    }

    private void setEventListeners() {
        splashViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, splashViewModel.getTripRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.TRIP)) {
                    Intent intent = null;
                    if(splashViewModel.getTripRepository().getTripCurrentResponse().tripCurrent.status != null) {
                        intent = new Intent(context, TrackingActivity.class);
                        intent.putExtra(Constants.TRIP, splashViewModel.getTripRepository().getTripCurrentResponse().tripCurrent);
                    }else{
                        intent = new Intent(context, MainActivity.class);
                    }
                    getActivityBase().finishAffinity();
                    startActivity(intent);
                }
            }
        });
    }

    private void thread() {
        openLocation();
        disposable = Observable.interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> L.e(TAG, "Throwable " + throwable.getMessage()))
                .subscribe(i -> setEvent());
    }




    private void openLocation() {

        LocationLatLng locationLatLng = new LocationLatLng(getActivityBase(), SplashFragment.this);

        if (GPSLocation.checkGps(context) && locationLatLng.mapPermission.validLocationPermission()) {
            locationLatLng.getLocation(new LocationChangeListener() {
                @Override
                public void select(String address, LatLng latLng) {
                    locationIsOpen = true;
                    UserHelper.saveKey(Constants.LAT, "" + latLng.latitude);
                    UserHelper.saveKey(Constants.LNG, "" + latLng.longitude);
                    UserHelper.saveKey(Constants.USER_ADDRESS, address);
//                    setEvent();
                }

                @Override
                public void error() {

                }
            });
        }else
            setEvent();
    }

    private static final String TAG = "SplashFragment";

    private void setEvent() {
        try {
            Intent intent = null;

            Timber.e("language" + UserHelper.retrieveKey(Constants.LANGUAGE_HAVE));
            if (UserHelper.retrieveKey(Constants.LANGUAGE_HAVE).equals("")) {
                Timber.e("language");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LANGUAGE);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.label_language));
            } else if (UserHelper.retrieveKey(Constants.COUNTRY_ID).equals("")) {
                Timber.e("country");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.country));
            } else if (locationIsOpen) {
                Timber.e("country");
                if(UserHelper.getUserId() == -1){
                    ((ParentActivity)context).finish();
                    intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.LOGIN);
                }else{
                    ((ParentActivity) context).finishAffinity();
                    intent = new Intent(context, MainActivity.class);
                }
            } else {
                Timber.e("not fetch location");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LOCATION);
            }
            disposable.dispose();
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
