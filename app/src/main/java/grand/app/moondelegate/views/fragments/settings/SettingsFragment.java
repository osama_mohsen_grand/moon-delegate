package grand.app.moondelegate.views.fragments.settings;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentSettingsBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.app.SettingsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BaseFragment {

    private FragmentSettingsBinding fragmentSettingsBinding;
    private SettingsViewModel settingsViewModel;
    private int type = 2;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        getData();
        bind();
        setEvent();
        return fragmentSettingsBinding.getRoot();
    }

    private void getData() {
//        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)){
//            type = getArguments().getInt(Constants.TYPE);
//        }
    }

    private void bind() {
        settingsViewModel = new SettingsViewModel(type);
        fragmentSettingsBinding.setSettingsViewModel(settingsViewModel);
    }

    private void setEvent() {
        settingsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, settingsViewModel.getSettingsRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){ // success add image
                    settingsViewModel.text = settingsViewModel.getSettingsRepository().getSettingsResponse().data;
                    settingsViewModel.showPage(true);
                }
            }
        });
    }


}
