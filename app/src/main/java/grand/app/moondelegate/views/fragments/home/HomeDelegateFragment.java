package grand.app.moondelegate.views.fragments.home;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.OrderDelegateAdapter;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentHomeDelegateBinding;
import grand.app.moondelegate.models.order.OrderDelegate;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.viewmodels.home.HomeDelegateViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeDelegateFragment extends BaseFragment {


    private View rootView;
    private FragmentHomeDelegateBinding fragmentHomeDelegateBinding;
    private HomeDelegateViewModel homeDelegateViewModel;
    private OrderDelegateAdapter orderDelegateAdapter;
    public int status = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentHomeDelegateBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_delegate, container, false);

        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        homeDelegateViewModel = new HomeDelegateViewModel(status);//1 for home , 2 for previous orders
        AppUtils.initVerticalRV(fragmentHomeDelegateBinding.rvOrders, fragmentHomeDelegateBinding.rvOrders.getContext(), 1);
        fragmentHomeDelegateBinding.setHomeDelegateViewModel(homeDelegateViewModel);
        rootView = fragmentHomeDelegateBinding.getRoot();
    }


    private void setEvent() {
        homeDelegateViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, homeDelegateViewModel.getOrderDelegateRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    orderDelegateAdapter = new OrderDelegateAdapter(homeDelegateViewModel.getOrderDelegateRepository().getOrdersDelegateListResponse().orders);
                    fragmentHomeDelegateBinding.rvOrders.setAdapter(orderDelegateAdapter);
                    setEventAdapter();
                    if(homeDelegateViewModel.getOrderDelegateRepository().getOrdersDelegateListResponse().orders.size() == 0){
                        homeDelegateViewModel.noData();
                    }else{
                        homeDelegateViewModel.haveData();
                    }
                }
            }
        });
    }

    private void setEventAdapter() {
        orderDelegateAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                OrderDelegate order = homeDelegateViewModel.getOrderDelegateRepository().getOrdersDelegateListResponse().orders.get(pos);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.ORDER_DETAILS);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ID,order.id);
                bundle.putInt(Constants.STATUS,order.statusId);
                intent.putExtra(Constants.BUNDLE,bundle);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.order_details));
                startActivity(intent);

            }
        });
    }


    private BroadcastReceiver newOrder = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent broadIntentExtra) {
            if(homeDelegateViewModel != null) {
                homeDelegateViewModel.getOrderDelegateRepository().getOrders(status);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(newOrder, new IntentFilter(Constants.NEW_ORDER));
    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(newOrder);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        homeDelegateViewModel.reset();

    }

}
