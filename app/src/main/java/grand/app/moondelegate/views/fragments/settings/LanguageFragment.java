package grand.app.moondelegate.views.fragments.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentLanguageBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.viewmodels.language.LanguageViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.vollyutils.MyApplication;


public class LanguageFragment extends BaseFragment {
    View rootView;
    private FragmentLanguageBinding fragmentLanguageBinding;
    private LanguageViewModel languageViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLanguageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_language, container, false);
        bind();
        rootView = fragmentLanguageBinding.getRoot();
        return rootView;
    }

    private void bind() {
        languageViewModel = new LanguageViewModel();
        setEvents();
        fragmentLanguageBinding.setLanguageViewModel(languageViewModel);
    }

    private void setEvents() {
        languageViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
//                handleActions(action,languageViewModel.getPrivacyRepository().getMessage());
                assert action != null;
                Intent intent = null;
                if(action.equals(Constants.LANGUAGE)){
                    UserHelper.saveKey(Constants.LANGUAGE_HAVE,Constants.TRUE);
                    LanguagesHelper.setLanguage(languageViewModel.getLanguage());
                    LanguagesHelper.changeLanguage(context,languageViewModel.getLanguage());
                    LanguagesHelper.changeLanguage(MyApplication.getInstance(),languageViewModel.getLanguage());

                    if(UserHelper.getUserId() == -1) {
                        intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                        ((ParentActivity)context).finishAffinity();
                        startActivity(intent);
                    }else{
                        ((ParentActivity)context).finishAffinity();
                        startActivity(new Intent(context, MainActivity.class));
                    }
                }else if(action.equals(Constants.ERROR)){
                    showError(languageViewModel.error);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        languageViewModel.reset();
    }


}
