package grand.app.moondelegate.views.fragments.credit;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentCreditBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.credit.CreditViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreditFragment extends BaseFragment {

    private CreditViewModel creditViewModel;
    private FragmentCreditBinding fragmentCreditBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentCreditBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_credit, container, false);
        Bind();
        setEvent();
        return fragmentCreditBinding.getRoot();

    }

    private void Bind() {
        creditViewModel = new CreditViewModel();
        fragmentCreditBinding.setCreditViewModel(creditViewModel);
    }

    private void setEvent() {
        creditViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, creditViewModel.getCreditRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    creditViewModel.showPage(true);
                    creditViewModel.setCreditResponse(creditViewModel.getCreditRepository().getCreditResponse());
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        creditViewModel.reset();

    }


}
