package grand.app.moondelegate.views.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentProfileBinding;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.PopUp.PopUpMenuHelper;
import grand.app.moondelegate.utils.dialog.DialogChoosePhoto;
import grand.app.moondelegate.utils.images.ImageLoaderHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.utils.upload.FileOperations;
import grand.app.moondelegate.utils.upload.FilesUtilsFragment;
import grand.app.moondelegate.viewmodels.user.ProfileViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.activities.MapAddressActivity;
import grand.app.moondelegate.vollyutils.VolleyFileObject;
import timber.log.Timber;


public class ProfileFragment extends BaseFragment {
    View rootView;
    private FragmentProfileBinding fragmentProfileBinding;
    private ProfileViewModel profileViewModel;
    public String type = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        getData();
        bind();
        setEvent();
        moveCursor();
        fragmentProfileBinding.edtProfilePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
                    fragmentProfileBinding.edtProfilePhone.setText("+");
                    moveCursor();
                }
            }
        });

        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
    }

    private void bind() {
        profileViewModel = new ProfileViewModel();
        fragmentProfileBinding.setProfileViewModel(profileViewModel);
        rootView = fragmentProfileBinding.getRoot();
    }

    public void moveCursor(){
        fragmentProfileBinding.edtProfilePhone.post(new Runnable() {
            @Override
            public void run() {
                fragmentProfileBinding.edtProfilePhone.setSelection(fragmentProfileBinding.edtProfilePhone.length());
            }
        });
    }


    private void setEvent() {
        profileViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, profileViewModel.getLoginRepository().getMessage());
            Timber.e(action);
            if (action.equals(Constants.SUCCESS)) {//submitSearch register
                toastMessage(profileViewModel.getLoginRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
            } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                pickImageDialogSelect();
            }else if (action.equals(Constants.LOCATIONS)) {//select location
                Timber.e("Locations");
                Intent intent = new Intent(context, MapAddressActivity.class);
                startActivityForResult(intent, Constants.ADDRESS_RESULT);
            }else if(action.equals(Constants.CHANGE_PASSWORD)){
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.CHANGE_PASSWORD);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.change_password));
                startActivity(intent);
            }else if(action.equals(Constants.LANGUAGE)){
                Intent intent = new Intent(context,BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LANGUAGE);
                intent.putExtra(Constants.NAME_BAR, getString(R.string.label_language));
                startActivity(intent);
            }else if(action.equals(Constants.COUNTRIES)){
                Intent intent = new Intent(context,BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                intent.putExtra(Constants.NAME_BAR, getString(R.string.country));
                startActivity(intent);
            }
        });
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            profileViewModel.setImage(volleyFileObject);
            fragmentProfileBinding.imgProfileUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
//            fragmentProfileBinding.imgProfileUser.setImageBitmap(volleyFileObject.getBitmap());
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            profileViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        profileViewModel.reset();

    }



}
