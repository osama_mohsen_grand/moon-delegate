package grand.app.moondelegate.views.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.ActivityTrackingBinding;
import grand.app.moondelegate.map.GPSAllowListener;
import grand.app.moondelegate.map.GPSLocation;
import grand.app.moondelegate.models.driver.current.TripCurrent;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.animation.AnimationHelper;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.maputils.background.LocationBackground;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.maputils.direction.DirectionDistance;
import grand.app.moondelegate.utils.maputils.direction.DirectionHelper;
import grand.app.moondelegate.utils.maputils.direction.DrawRoute;
import grand.app.moondelegate.utils.maputils.location.LocationListenerEvent;
import grand.app.moondelegate.utils.maputils.location.MapActivityHelper;
import grand.app.moondelegate.utils.maputils.permission.MapPermissionActivity;
import grand.app.moondelegate.utils.maputils.tracking.FireTracking;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.location.LocationHelper;
import grand.app.moondelegate.viewmodels.track.TrackViewModel;
import timber.log.Timber;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.util.concurrent.ServiceManager;

public class TrackingActivity extends ParentActivity {

    ActivityTrackingBinding activityTrackingBinding;
    TrackViewModel trackViewModel;
    AnimationHelper animationHelper;
    GoogleApiClient mGoogleApiClient;
    TripCurrent tripCurrent;
    MapActivityHelper mapActivityHelper;
    MapPermissionActivity mapPermission;
    boolean async = false;
    private static final String TAG = "TrackingActivity";
    FireTracking fireTracking = null;
    public int trip_id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind();
        initMap();

    }

    public void checkServiceLocationUpdate(){
        if(tripCurrent.status >= 1)
            startTracking();
        if(tripCurrent.status >=3 ){
            startBackgroundService();
        }
    }

    //here from get Data
    public void handleDialog() {
        if (tripCurrent.status == 0) {
            trackViewModel.showDialogAcceptance();
        } else if (tripCurrent.status == 1) {
            showInfoDialog();
        } else if (tripCurrent.status == 3) {
            trackViewModel.showDialogStartTrip(false);
        } else if (tripCurrent.status == 4) {
            showMarkers();
            trackViewModel.showDialogFinishTrip(false);
        }
        checkServiceLocationUpdate();
    }

    public void showMarkers() {
        trackViewModel.mapConfig.clearMarkers();
        if (tripCurrent.status >= 1) {
            trackViewModel.mapConfig.addMarkerList(new LatLng(Double.parseDouble(tripCurrent.startLat), Double.parseDouble(tripCurrent.startLng)), R.drawable.ic_map_pin, getString(R.string.pickup_location));
        }
        if (tripCurrent.status >= 4) {
            trackViewModel.mapConfig.addMarkerList(new LatLng(Double.parseDouble(tripCurrent.endLat), Double.parseDouble(tripCurrent.endLng)), R.drawable.ic_marker_dest, getString(R.string.destination_location));
            drawRoute(new LatLng(Double.parseDouble(tripCurrent.startLat), Double.parseDouble(tripCurrent.startLng))
                    , new LatLng(Double.parseDouble(tripCurrent.endLat), Double.parseDouble(tripCurrent.endLng)));
        }
    }

    private void getData() {
        if (getIntent() != null && getIntent().hasExtra(Constants.TRIP)) {
            tripCurrent = (TripCurrent) getIntent().getSerializableExtra(Constants.TRIP);
            if (tripCurrent != null) {
                trackViewModel.setTrip(tripCurrent);
                showMarkers();
                handleDialog();
            }
        }else if(getIntent() != null && getIntent().hasExtra(Constants.TRIP_ID)){
            trackViewModel.setOrderId(getIntent().getIntExtra(Constants.TRIP_ID,-1));
        }
    }

    private void initMap() {
        mapPermission = new MapPermissionActivity(this);
        if (mapActivityHelper != null) {
            mapActivityHelper.buildGoogleApiClient();
        }
        initViews();
    }

    private void initViews() {
        if (!mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST)) {
            if (mapActivityHelper != null) {
                mapActivityHelper.initFused();
                mapActivityHelper.setUpLocation(Constants.LOCATION_REQUEST, Constants.LOCATION_PERMISSION);
            }
            allowGPS();
        }
        if (activityTrackingBinding.map != null) {
            activityTrackingBinding.map.onCreate(null);
            activityTrackingBinding.map.onResume();
            onMapReady();
        }

    }
    /*
    0 => no confirm
    1 => confirm
    2 => cancel
    3 => arrived
    4 => finished
    send status 1 if accept send status 2 if cancel send status 3 if arrive  else if finished trip send status 4

     */


    public void allowGPS() {
        GPSLocation.EnableGPSAutoMatically(this, new GPSAllowListener() {
            @Override
            public void GPSStatus(boolean isOpen) {
                if (isOpen) {
                    async = true;
                    onResume();
                } else
                    Toast.makeText(TrackingActivity.this, "" + ResourceManager.getString(R.string.please_open_gps), Toast.LENGTH_SHORT).show();
            }
        });
    }


    DirectionHelper directionHelper = null;

    //
    private void onMapReady() {
        activityTrackingBinding.map.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap googleMap) {
                setViewModel();
                setEvent();
                trackViewModel.mapConfig = new MapConfig(TrackingActivity.this, googleMap);
                mapActivityHelper = new MapActivityHelper(TrackingActivity.this, trackViewModel.mapConfig);
                directionHelper = new DirectionHelper(TrackingActivity.this, trackViewModel.mapConfig);
                trackViewModel.mapConfig.setMapStyle();
                trackViewModel.mapConfig.setSettings();
                trackViewModel.mapConfig.setLocationButtonListeners();
                trackViewModel.mapConfig.setPadding(0, 0, 20, 400);
//                trackViewModel.mapConfig.changeMyLocationButtonLocation(activityTrackingBinding.map);//change location button
//                homeViewModel.mapConfig.setLocationButtonListeners();//enable button location listeners
                getData();
                if (!mapPermission.validLocationPermission()) {
                    mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST);
                } else {
                    trackViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                    mapActivityHelper.startUpdateLocation(new LocationListenerEvent() {
                        @Override
                        public void update(LatLng latLng, Object object) {
                            async = false;
                            trackViewModel.mapConfig.saveLastLocation(latLng);
                        }
                    });
                    onResume();
                }
            }
        });
    }

    Intent backgroundService = null;

    public void startBackgroundService() {
        if (!AppUtils.isServiceRunning(this, LocationBackground.class)) {
            backgroundService = new Intent(this, LocationBackground.class);
            backgroundService.putExtra(Constants.TRIP_ID, +tripCurrent.id);
            stopService(backgroundService);
            startService(backgroundService);
        }
    }

    public void stopBackgroundService() {
        Timber.e("stop service");
        if (backgroundService != null) {
            Timber.e("stop service backgroundService not null");
            LocationHelper.clear();
            stopService(backgroundService);
        }
    }

    public void drawRoute(LatLng start, LatLng end) {
        if (start == null) Log.e(TAG, "drawRouteStart: null");
        else Log.e(TAG, "drawRouteStart: not null");
        if (end == null) Log.e(TAG, "drawRouteEnd: null");
        else Log.e(TAG, "drawRouteEnd: not null");
        directionHelper.json_route = directionHelper.getRouteUrl(start, end);
        Log.e(TAG, "onChanged: " + directionHelper.json_route);
        trackViewModel.mapConfig.removeRoute();
        new DrawRoute(this, directionHelper.json_route, trackViewModel.mapConfig, new DirectionDistance() {
            @Override
            public void receive(int distanceM) {
            }
        }).execute();
    }

    public void showInfoDialog() {
        LocationHelper.clear();
        showMarkers();
        trackViewModel.showDialogUserInfo(false);
    }

    //1=>accept , 2=>reject  , 3=>arrive  , 4=>start  , 5=> cancel after accept ,  6=>finished trip
    private void setEvent() {
        trackViewModel.mMutableLiveData.observe(this, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Log.e(TAG, "onChanged: " + action);
                handleActions(action, trackViewModel.getTripRepository().getMessage());
                if (action.equals(Constants.TRIP_ACTION)) {
                    toastMessage(trackViewModel.getTripRepository().getMessage());
                    tripCurrent.status = trackViewModel.submitAction;
                    checkServiceLocationUpdate();
                    if (trackViewModel.submitAction == 1) {
                        showInfoDialog();
                    } else if (trackViewModel.submitAction == 2 || trackViewModel.submitAction == 5 || trackViewModel.submitAction == 6) {
                        finishAffinity();
                        if (trackViewModel.submitAction == 6) {
                            stopBackgroundService();
                            if(fireTracking != null){
                                fireTracking.stopTracking();
                            }
                            Intent intent = new Intent(TrackingActivity.this, BaseActivity.class);
                            intent.putExtra(Constants.PAGE, Constants.RATE);
                            intent.putExtra(Constants.NAME_BAR, getString(R.string.rate));
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(Constants.TRIP, tripCurrent);
                            bundle.putString(Constants.TOTAL, trackViewModel.getTripRepository().getTripActionResponse().price);
                            intent.putExtra(Constants.BUNDLE, bundle);
                            startActivity(intent);
                        } else
                            startActivity(new Intent(TrackingActivity.this, MainActivity.class));
                    } else if (trackViewModel.submitAction == 3) {
                        trackViewModel.showDialogStartTrip(false);
                    } else if (trackViewModel.submitAction == 4) { // start Trip and draw route
                        showMarkers();
                        trackViewModel.showDialogFinishTrip(false);
                    }
                } else if (action.equals(Constants.HIDE_NEW_ORDER)) {
                    //hide view new order , show view user data info
                    animationHelper.animationDownUp(activityTrackingBinding.rlTrackUserData, activityTrackingBinding.rlTrackNewOrder);
                } else if (action.equals(Constants.CALL)) {
                    Uri call = Uri.parse("tel:" + tripCurrent.receiverPhone);
                    Intent surf = new Intent(Intent.ACTION_DIAL, call);
                    startActivity(surf);
                }else if(action.equals(Constants.TRIP_DETAILS)){
                    tripCurrent = trackViewModel.tripRepository.getTripCurrentResponse().tripCurrent;
                    trackViewModel.setTrip(tripCurrent);
                    showMarkers();
                    handleDialog();
                }
//                handleActions(action, trackViewModel.getTripActionRepository().getMessage());
//                if (trackViewModel.getTripActionRepository() != null && trackViewModel.getTripActionRepository().getStatusMsg() != null && trackViewModel.getTripActionRepository().getStatusMsg().status == Constants.RESPONSE_TRIP_FAILED) {
//                    toastMessage(getString(R.string.sorry_trip_timeout), R.drawable.ic_sorry, R.color.colorPrimary);
//                    finishAffinity();
//                    startActivity(new Intent(TrackingActivity.this, MainActivity.class));
//                }
//                if (action.equals(Constants.TRIP_DETAILS)) {
//                    Log.e(TAG, "onChanged: here");
//                    if (trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.size() == 0) {
//                        Log.e(TAG, "onChanged: Accept Or Reject Dialog");
//                        trackViewModel.showDialogAcceptance();
//                    } else if (trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.get(trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.size() - 1).status_id == 5) {
//                        trackViewModel.showDialogUserInfo(false);
//                        Log.e(TAG, "onChanged: Accept Or Reject Dialog");
////                        drawRoute(trackViewModel.markerPickup.getPosition(),trackViewModel.markerDestination.getPosition());
//                        trackViewModel.setInitMarker();
//                        startBackgroundService();
//                        startTracking();
//
//                    } else if (trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.get(trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.size() - 1).status_id == 7) {
//                        Log.e(TAG, "onChanged: here3");
////                        drawRoute(trackViewModel.markerPickup.getPosition(),trackViewModel.markerDestination.getPosition());
//                        trackViewModel.setInitMarker();
//                        trackViewModel.showDialogUserInfo(false);
//                        trackViewModel.showStartTrip();
//                        startBackgroundService();
//                        startTracking();
//                    } else {
//                        Log.e(TAG, "onChanged: here4");
//                        trackViewModel.showDialogAcceptance();
//                    }
//
//                } else if (action.equals(URLS.TRIP_ACCEPT)) {
//                    //navigate to location
//                    trackViewModel.setInitMarker();
//                    Log.e(TAG, "onChanged: here5");
//                    LocationHelper.clear(); // clear all locations saved
//                    trackViewModel.showDialogUserInfo(true);
//                    startTracking();
//                } else if (action.equals(URLS.TRIP_CANCEL) || action.equals(URLS.TRIP_CANCEL_AFTER_ACCEPT)) {
//                    //navigate to location
//                    Log.e(TAG, "onChanged: here6");
//                    finishAffinity();
//                    startActivity(new Intent(TrackingActivity.this, MainActivity.class));
//                } else if (action.equals(URLS.TRIP_ARRIVE)) {
//                    //start trip
//                    Log.e(TAG, "onChanged: here7");
//                    trackViewModel.showStartTrip();
//                } else if (action.equals(Constants.START_TRIP)) {
//                    //start trip
////                    trackViewModel.drawDestination();
//                    drawRoute(trackViewModel.markerPickup.getPosition(),trackViewModel.markerDestination.getPosition());
//                    trackViewModel.showEndTrip();
//                } else if (action.equals(Constants.END_TRIP)) {
//                    //show Feedback
//                    Intent intent = new Intent(TrackingActivity.this, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.RATE);
//                    Bundle bundle = new Bundle();
////                    bundle.putSerializable(Constants.ORDER, trackViewModel.getTripRepository().getTripDetailsResponse());
//                    bundle.putString(Constants.TRIP_ID,String.valueOf(trip_id));
//                    bundle.putString(Constants.TOTAL,trackViewModel.getTripActionRepository().getEndTripResponse().data);
//                    bundle.putString(Constants.NAME,trackViewModel.getTripRepository().getTripDetailsResponse().data.user.name);
//                    bundle.putString(Constants.IMAGE,trackViewModel.getTripRepository().getTripDetailsResponse().data.user.image);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivity(intent);
//                    stopBackgroundService();
//                } else if (action.equals(Constants.NAVIGATION)) {
//                    //navigate to location
//                    trackViewModel.mapConfig.navigation(trackViewModel.latLng.latitude, trackViewModel.latLng.longitude
//                    );
//                } else if (action.equals(Constants.HIDE_NEW_ORDER)) {
//                    //hide view new order , show view user data info
//                    animationHelper.animationDownUp(activityTrackingBinding.rlTrackUserData, activityTrackingBinding.rlTrackNewOrder);
//                } else if (action.equals(Constants.NEXT_TRIP)) {
//                    //hide view new order , show view user data info
//                    drawRoute(trackViewModel.markerPickup.getPosition(),trackViewModel.markerDestination.getPosition());
//                } else if (action.equals(Constants.NO_NEXT)) {
//                    //hide view new order , show view user data info
//                    toastMessage(ResourceManager.getString(R.string.no_next_trip), R.drawable.ic_check_white, R.color.colorPrimary);
//                }else if (action.equals(Constants.ARRIVE)) {
//                    /*
//                    String lat = SharedPreferenceHelper.getKey(Constants.LAT);
//                    String lng = SharedPreferenceHelper.getKey(Constants.LNG);
//                    if(!lat.equals("") && !lng.equals("")){
//
//                        String res = directionHelper.loadJSONFromAsset();
//                        directionHelper.drawRoute();
//                        trackViewModel.mapConfig.addMarkerList(new LatLng(Double.parseDouble(lat),Double.parseDouble(lng)),R.drawable.ic_map_pin,"pickup location");
//                        trackViewModel.mapConfig.addMarkerList(new LatLng(trackViewModel.lat,trackViewModel.lng),R.drawable.ic_marker_dest,"destination location");
//
////                        String route = directionHelper.getDirectionsUrl(new LatLng(Double.parseDouble(lat),Double.parseDouble(lng)),
////                                new LatLng(trackViewModel.lat,trackViewModel.lng));
////                        new DrawRoute(TrackingActivity.this,route,trackViewModel.mapConfig.getGoogleMap()).execute();
//                    }*/
//                } else if (action.equals(Constants.GOOGLE_MAP)) {
//                    String lat = SharedPreferenceHelper.getKey(Constants.LAT);
//                    String lng = SharedPreferenceHelper.getKey(Constants.LNG);
////                    trackViewModel.mapConfig.navigation(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)),
////                            new LatLng(trackViewModel.getTripRepository().getTripDetailsResponse().data.tripPath.get(0).lat, trackViewModel.getTripRepository().getTripDetailsResponse().data.tripPath.get(0).lng));
//
//                    trackViewModel.mapConfig.navigation(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)),
//                            new LatLng(trackViewModel.getTripRepository().getTripDetailsResponse().data.tripPath.get(trackViewModel.marker_destination).lat,
//                                    trackViewModel.getTripRepository().getTripDetailsResponse().data.tripPath.get(trackViewModel.marker_destination).lng));
//

            }
        });

    }

    public void startTracking() {
        if(fireTracking == null) {
            fireTracking = new FireTracking(TrackingActivity.this, trackViewModel.mapConfig, mapActivityHelper);
            fireTracking.startUpdateLocation();
        }
    }

    private void bind() {
        activityTrackingBinding = DataBindingUtil.setContentView(this, R.layout.activity_tracking);
        activityTrackingBinding.getRoot();
    }

    public void setViewModel() {
        trackViewModel = new TrackViewModel();
        animationHelper = new AnimationHelper(this);
        activityTrackingBinding.setTrackViewModel(trackViewModel);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.LOCATION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                allowGPS();
                onResume();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (async) {
            onMapReady();
            async = false;
        }
    }

    @Override
    protected void onDestroy() {
        if (fireTracking != null)
            fireTracking.stopTracking();
        super.onDestroy();
    }


}

