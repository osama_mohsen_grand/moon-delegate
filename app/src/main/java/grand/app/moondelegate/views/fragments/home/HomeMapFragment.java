package grand.app.moondelegate.views.fragments.home;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentHomeMapBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.maputils.location.GPSAllowListener;
import grand.app.moondelegate.utils.maputils.location.GPSLocation;
import grand.app.moondelegate.utils.maputils.location.LocationListenerEvent;
import grand.app.moondelegate.utils.maputils.location.MapActivityHelper;
import grand.app.moondelegate.utils.maputils.permission.MapPermissionFragment;
import grand.app.moondelegate.utils.maputils.tracking.FireTracking;
import grand.app.moondelegate.viewmodels.home.HomeMapViewModel;
import grand.app.moondelegate.vollyutils.MyApplication;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeMapFragment extends BaseFragment implements LocationListener {

    private View rootView;
    private FragmentHomeMapBinding fragmentHomeMapBinding;
    private HomeMapViewModel homeMapViewModel;
    public static double lat = 0, lng = 0;
    private static final String TAG = "HomeFragment";
    private boolean async = false;
    public FusedLocationProviderClient fusedLocationClient;
    MapPermissionFragment mapPermission;
    MapActivityHelper mapActivityHelper;
    FireTracking fireTracking;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentHomeMapBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_map, container, false);
        Timber.e("STATUS:Home Map");
        if (fragmentHomeMapBinding != null) {
            bind(savedInstanceState);
            setEvent();
        }
        return rootView;
    }

    private void bind(Bundle savedInstanceState) {
        homeMapViewModel = new HomeMapViewModel();
        mapPermission = new MapPermissionFragment(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

        fragmentHomeMapBinding.mapView.onCreate(savedInstanceState);

        fragmentHomeMapBinding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.e(TAG, "bind: ");
        fragmentHomeMapBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                homeMapViewModel.mapConfig = new MapConfig(context, mMap);
                mapActivityHelper = new MapActivityHelper(getActivity(), homeMapViewModel.mapConfig);
                homeMapViewModel.mapConfig.setMapStyle();//set style google map
                homeMapViewModel.mapConfig.setSettings();//add setting google map
                homeMapViewModel.mapConfig.checkLastLocation();
                homeMapViewModel.mapConfig.changeMyLocationButtonLocation(fragmentHomeMapBinding.mapView);//change location button
//                homeMapViewModel.mapConfig.setLocationButtonListeners();//enable button location listeners
                homeMapViewModel.mapConfig.getGoogleMap().setPadding(0, 10, 0, 10);
                homeMapViewModel.mapConfig.getGoogleMap().setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        lat = homeMapViewModel.mapConfig.getGoogleMap().getCameraPosition().target.latitude;
                        lng = homeMapViewModel.mapConfig.getGoogleMap().getCameraPosition().target.longitude;
                    }
                });//addMarkerOnMap_

                Log.e(TAG, "onMapReady: start");
                if (!mapPermission.validLocationPermission()) {
                    mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST);
                } else {
                    homeMapViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                    mapActivityHelper.startUpdateLocation(new LocationListenerEvent() {
                        @Override
                        public void update(LatLng latLng, Object object) {
                            async = false;
                            homeMapViewModel.mapConfig.saveLastLocation(latLng);
                        }
                    });
                    onResume();
                    enableMyLocationIfPermitted();
                }
            }
        });
        fragmentHomeMapBinding.setHomeViewModel(homeMapViewModel);
        rootView = fragmentHomeMapBinding.getRoot();
    }

    private void showAutoComplete() {
        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            Places.initialize(context, getString(R.string.google_direction_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(context);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    private void enableMyLocationIfPermitted() {
        if (!mapPermission.validLocationPermission()) {
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        } else if (homeMapViewModel.mapConfig != null && homeMapViewModel.mapConfig.getGoogleMap() != null) {
            allowGPS();
            onResume();
        }
    }


    @SuppressLint("MissingPermission")
    private void addCircleLocation(LatLng latLng) {
        if (!mapPermission.validLocationPermission()) {
            return;
        }
        homeMapViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
        homeMapViewModel.mapConfig.moveCamera(latLng);
        onResume();
    }

    private void setEvent() {
        homeMapViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
            }
        });
    }

    private LocationCallback locationCallback;

    @SuppressLint("MissingPermission")
    public void allowGPS() {
        if (!GPSLocation.checkGps(context)) {
            GPSLocation.EnableGPSAutoMatically((ParentActivity) context, new GPSAllowListener() {
                @Override
                public void GPSStatus(boolean isOpen) {
                    if (isOpen) {
                        homeMapViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                        updateLocation();
                    }
                }
            });

        } else {
            if (mapPermission.validLocationPermission()) {
                Location location = GPSLocation.getLocation((ParentActivity) context);
                if (location != null && !async) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    homeMapViewModel.mapConfig.moveCamera(latLng);
                    async = true;
                }
                updateLocation();
            }
        }
    }


    private LocationManager mLocationManager;


    @SuppressLint("MissingPermission")
    public void updateLocation() {
        Log.e(TAG, "updateLocation: ");
        if (!mapPermission.validLocationPermission()) {
            return;
        }
        homeMapViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        Task<Location> task = fusedLocationClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                Log.e(TAG, "onSuccess: ");
                homeMapViewModel.mapConfig.saveLastLocation(new LatLng(location.getLatitude(),location.getLongitude()));
                if (location != null && !async) {
                    //Write your implemenation here
                    Log.e(TAG, location.getLatitude() + " " + location.getLongitude());
                }
            }
        });
        onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mLocationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: Before super");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onRequestPermissionsResult: " + Constants.LOCATION_REQUEST);
        if (requestCode == Constants.LOCATION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "onRequestPermissionsResultDone: " + Constants.LOCATION_REQUEST);
                onResume();
                allowGPS();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onLocationChanged(Location location) {
        homeMapViewModel.mapConfig.saveLastLocation(new LatLng(location.getLatitude(),location.getLongitude()));
        if (!async && location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            homeMapViewModel.mapConfig.saveLastLocation(latLng);
            addCircleLocation(latLng);
            homeMapViewModel.updateLocation();
            async = true;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (fragmentHomeMapBinding != null && fragmentHomeMapBinding.mapView != null)
            fragmentHomeMapBinding.mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fragmentHomeMapBinding != null && fragmentHomeMapBinding.mapView != null)
            fragmentHomeMapBinding.mapView.onPause();
    }

    @Override
    public void onDestroy() {

        if (fragmentHomeMapBinding != null && fragmentHomeMapBinding.mapView != null)
            if (homeMapViewModel != null) {
                homeMapViewModel.reset();
                fragmentHomeMapBinding.mapView.onDestroy();
                fragmentHomeMapBinding = null;
            }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (fragmentHomeMapBinding.mapView != null) fragmentHomeMapBinding.mapView.onLowMemory();
    }
}
