package grand.app.moondelegate.views.fragments.auth;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hbb20.CountryCodePicker;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentPhoneVerificationBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.user.PhoneVerificationViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import timber.log.Timber;

public class PhoneVerificationFragment extends BaseFragment {
    private FragmentPhoneVerificationBinding fragmentPhoneVerificationBinding;
    private PhoneVerificationViewModel phoneVerificationViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentPhoneVerificationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_phone_verification, container, false);
        bind();
        setEvent();
        return fragmentPhoneVerificationBinding.getRoot();
    }

    private void bind() {
        phoneVerificationViewModel = new PhoneVerificationViewModel();
        phoneVerificationViewModel.cpp = "+"+fragmentPhoneVerificationBinding.ccp.getDefaultCountryCode();
        fragmentPhoneVerificationBinding.ccp.setCountryForNameCode(fragmentPhoneVerificationBinding.ccp.getDefaultCountryNameCode());
        fragmentPhoneVerificationBinding.ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                phoneVerificationViewModel.cpp = "+"+fragmentPhoneVerificationBinding.ccp.getSelectedCountryCode();
            }
        });
        fragmentPhoneVerificationBinding.setPhoneVerificationViewModel(phoneVerificationViewModel);
    }


    private void setEvent() {
        phoneVerificationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Timber.e(phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage());
                handleActions(action, phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.WRITE_CODE)){
                    toastMessage(phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.VERIFICATION);
                    Bundle bundle = getArguments();
                    if(bundle == null)
                        bundle = new Bundle();
                    bundle.putString(Constants.TYPE,Constants.REGISTRATION);
                    bundle.putString(Constants.PHONE, phoneVerificationViewModel.phoneCpp);
                    bundle.putString(Constants.VERIFY_ID,phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                    bundle.putString(Constants.NAME_BAR,"");
                    bundle.putString(Constants.TYPE,Constants.REGISTRATION);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    ((ParentActivity)context).finish();
                    startActivity(intent);
                }

            }
        });
        phoneVerificationViewModel.mMutableLiveDataLogin.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Timber.e(phoneVerificationViewModel.getLoginRepository().getMessage());
                handleActions(action, phoneVerificationViewModel.getLoginRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    phoneVerificationViewModel.sendCode();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(phoneVerificationViewModel != null) phoneVerificationViewModel.reset();
    }

}
