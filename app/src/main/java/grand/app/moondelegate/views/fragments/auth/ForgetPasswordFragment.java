package grand.app.moondelegate.views.fragments.auth;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentForgetPasswordBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.user.ForgetPasswordViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgetPasswordFragment extends BaseFragment {

    private FragmentForgetPasswordBinding fragmentForgetPasswordBinding;
    private ForgetPasswordViewModel forgetPasswordViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentForgetPasswordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_forget_password, container, false);
        getData();
        bind();
        setEvent();
        return fragmentForgetPasswordBinding.getRoot();
    }

    private void getData() {
    }

    private void bind() {
        forgetPasswordViewModel = new ForgetPasswordViewModel();
        fragmentForgetPasswordBinding.setForgetPasswordViewModel(forgetPasswordViewModel);
    }

//SUCCESS
    private void setEvent() {
        forgetPasswordViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, forgetPasswordViewModel.getVerificationFirebaseSMSRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.WRITE_CODE)){
                    toastMessage(forgetPasswordViewModel.getVerificationFirebaseSMSRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.VERIFICATION);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TYPE,Constants.FORGET_PASSWORD);
                    bundle.putString(Constants.PHONE, forgetPasswordViewModel.getLoginRepository().getForgetPasswordResponse().data);
                    bundle.putString(Constants.VERIFY_ID,forgetPasswordViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                    bundle.putString(Constants.NAME_BAR,"");
                    intent.putExtra(Constants.BUNDLE,bundle);
                    ((ParentActivity)context).finish();
                    startActivity(intent);
                }

            }
        });

        forgetPasswordViewModel.mMutableLiveDataForget.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, forgetPasswordViewModel.getLoginRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    forgetPasswordViewModel.getVerificationFirebaseSMSRepository().sendVerificationCode(forgetPasswordViewModel.getLoginRepository().getForgetPasswordResponse().data);
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(forgetPasswordViewModel != null) forgetPasswordViewModel.reset();
    }

}
