package grand.app.moondelegate.views.fragments.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentLoginBinding;
import grand.app.moondelegate.models.app.AccountTypeModel;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.PopUp.PopUpInterface;
import grand.app.moondelegate.utils.PopUp.PopUpMenuHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.viewmodels.user.LoginViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.activities.TrackingActivity;
import timber.log.Timber;

public class LoginFragment extends BaseFragment {
    View rootView;
    private FragmentLoginBinding fragmentLoginBinding;
    private LoginViewModel loginViewModel;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        loginViewModel = new LoginViewModel();
        initPopUp();
        fragmentLoginBinding.setLoginViewModel(loginViewModel);
        rootView = fragmentLoginBinding.getRoot();
    }

    private void initPopUp() {
        fragmentLoginBinding.edtLoginAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AccountTypeModel> accountTypeModels = AppMoon.getAccountType();
                ArrayList<String> popUpModels = new ArrayList<>();
                for (AccountTypeModel accountTypeModel : accountTypeModels)
                    popUpModels.add(accountTypeModel.type);
                popUpMenuHelper.openPopUp(getActivity(), fragmentLoginBinding.edtLoginAccountType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        fragmentLoginBinding.edtLoginAccountType.setText(accountTypeModels.get(position).type);
                        loginViewModel.loginRequest.setType(String.valueOf(accountTypeModels.get(position).id));
                    }
                });

            }
        });
    }

    private void setEvent() {
        loginViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Timber.e(loginViewModel.getLoginRepository().getMessage());
                handleActions(action, loginViewModel.getLoginRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.HOME)){
                    ((AppCompatActivity) context).finishAffinity();
                    if(UserHelper.getUserDetails().trip != null && UserHelper.getUserDetails().trip.status != null){
                        Intent intent = new Intent(context, TrackingActivity.class);
                        intent.putExtra(Constants.TRIP,UserHelper.getUserDetails().trip);
                        startActivity(intent);
                    }else {
                        startActivity(new Intent(context, MainActivity.class));
                    }
                }else if(action.equals(Constants.PACKAGES)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.PACKAGES);
                    ((AppCompatActivity)context).finishAffinity();
                    startActivity(intent);
                }else if(action.equals(Constants.REGISTRATION)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.PHONE_VERIFICATION);
                    startActivity(intent);
                }else if(action.equals(Constants.FORGET_PASSWORD)){
                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    Bundle bundle = new Bundle();
                    intent.putExtra(Constants.PAGE,Constants.FORGET_PASSWORD);
                    bundle.putString(Constants.NAME, ResourceManager.getString(R.string.label_forget_password));
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(loginViewModel != null) loginViewModel.reset();
    }

}
