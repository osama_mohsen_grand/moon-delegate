package grand.app.moondelegate.views.fragments.rate;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentRateBinding;
import grand.app.moondelegate.models.driver.current.TripCurrent;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.rate.RateViewModel;
import grand.app.moondelegate.views.activities.MainActivity;

public class RateFragment extends BaseFragment {
    View rootView;
    private FragmentRateBinding fragmentRateBinding;
    private RateViewModel rateViewModel;
    TripCurrent tripCurrent;
    String name="",image="";
    int tripId;
    public String total = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentRateBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_rate, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.TRIP))
            tripCurrent = (TripCurrent) getArguments().getSerializable(Constants.TRIP);
        if(getArguments() != null && getArguments().containsKey(Constants.TOTAL))
            total = getArguments().getString(Constants.TOTAL);
    }

    private void bind() {
        rateViewModel = new RateViewModel(tripCurrent,total);
        fragmentRateBinding.setRateViewModel(rateViewModel);
        rootView = fragmentRateBinding.getRoot();
    }

    private void setEvent() {
        rateViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, rateViewModel.getTripRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.RATE)){
                    toastMessage(rateViewModel.getTripRepository().getMessage());
                    ((AppCompatActivity)context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(rateViewModel != null) rateViewModel.reset();
    }

}
