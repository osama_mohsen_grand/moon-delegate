package grand.app.moondelegate.views.fragments.base;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.FragmentZoomBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.app.ZoomViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZoomFragment extends Fragment {


    FragmentZoomBinding fragmentZoomBinding;
    public String image = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentZoomBinding =   DataBindingUtil.inflate(inflater,R.layout.fragment_zoom, container, false);
        getData();
        fragmentZoomBinding.setZoomViewModel(new ZoomViewModel(image));
        return fragmentZoomBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.IMAGE)){
            image = getArguments().getString(Constants.IMAGE);
        }
    }

}
