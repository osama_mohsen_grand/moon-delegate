package grand.app.moondelegate.views.fragments.order;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.OrderHistoryAdapter;
import grand.app.moondelegate.base.BaseFragment;

import grand.app.moondelegate.databinding.FragmentOrderHistoryBinding;

import grand.app.moondelegate.models.order.OrderDelegate;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.viewmodels.home.HomeDelegateViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryFragment extends BaseFragment {


    private FragmentOrderHistoryBinding fragmentOrderHistoryBinding;
    private HomeDelegateViewModel homeDelegateViewModel;
    private OrderHistoryAdapter orderHistoryAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentOrderHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_history, container, false);
        bind();
        setEvent();
        return fragmentOrderHistoryBinding.getRoot();
    }

    private void bind() {
        homeDelegateViewModel = new HomeDelegateViewModel(2);//1 for home , 2 for previous orders
        AppUtils.initVerticalRV(fragmentOrderHistoryBinding.rvOrders, fragmentOrderHistoryBinding.rvOrders.getContext(), 1);
        fragmentOrderHistoryBinding.setHomeDelegateViewModel(homeDelegateViewModel);
    }


    private void setEvent() {
        homeDelegateViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, homeDelegateViewModel.getOrderDelegateRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    orderHistoryAdapter = new OrderHistoryAdapter(homeDelegateViewModel.getOrderDelegateRepository().getOrdersDelegateListResponse().orders);
                    fragmentOrderHistoryBinding.rvOrders.setAdapter(orderHistoryAdapter);
                    setEventAdapter();
                    if (homeDelegateViewModel.getOrderDelegateRepository().getOrdersDelegateListResponse().orders.size() == 0) {
                        homeDelegateViewModel.noData();
                    }
                }
            }
        });
    }

    private void setEventAdapter() {
        orderHistoryAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                OrderDelegate order = homeDelegateViewModel.getOrderDelegateRepository().getOrdersDelegateListResponse().orders.get(pos);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.ORDER_DETAILS);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ID, order.id);
                bundle.putInt(Constants.STATUS, order.statusId);
                intent.putExtra(Constants.BUNDLE, bundle);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.order_details));
                startActivity(intent);

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if(isReloadPage() && homeDelegateViewModel != null ){
            homeDelegateViewModel.getOrderDelegateRepository().getOrders(2);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        homeDelegateViewModel.reset();
    }

}
