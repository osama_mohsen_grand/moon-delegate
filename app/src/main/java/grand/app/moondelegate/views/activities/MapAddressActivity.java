package grand.app.moondelegate.views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.ActivityMapAddressBinding;
import grand.app.moondelegate.map.GPSAllowListener;
import grand.app.moondelegate.map.GPSLocation;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.MyContextWrapper;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.maputils.location.LocationLatLng;
import grand.app.moondelegate.viewmodels.map.MapAddressViewModel;
import timber.log.Timber;

public class MapAddressActivity extends FragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {


    public double lat = 0, lng = 0;
    MapAddressViewModel mapAddressViewModel = null;

    ActivityMapAddressBinding activityMapAddressBinding;
    MapView mapView;

    PlaceAutocompleteFragment place_location;
    //    AutocompleteSupportFragment autocompleteSupportFragment;
    boolean async = false;

    FusedLocationProviderClient fusedLocationProviderClient = null;
    GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static final int LOCATION_REQUEST = 500;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 7000;
    public double latOffer = 0, lngOffer = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMapAddressBinding = DataBindingUtil.setContentView(this, R.layout.activity_map_address);
        mapView = activityMapAddressBinding.mapView;
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        getData();
        mapAddressViewModel = new MapAddressViewModel(this);
        activityMapAddressBinding.setMapAddressViewModel(mapAddressViewModel);

        mGoogleApiClient.connect();
        place_location = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_location);
        place_location.getView().setVisibility(View.GONE);
        initViews();
        onMapReady();
//        setMapSearch();
        setClickEvent();
    }

    private void getData() {
        Intent intent = getIntent();
        Log.e("getData", "Done get ProductPaginate");
        if (intent.hasExtra(Constants.LAT)) {
            lat = intent.getDoubleExtra(Constants.LAT, 0);
            Timber.e("lat:" + lat);
        }
        if (intent.hasExtra(Constants.LNG)) {
            lng = intent.getDoubleExtra(Constants.LNG, 0);
            Timber.e("lat:" + lat);
        }
        Log.e("lat", String.valueOf(lat));
        Log.e("lng", String.valueOf(lng));
    }

    private void setClickEvent() {
        activityMapAddressBinding.imgMapSubmit.setOnClickListener(v -> {
            double lat = mapAddressViewModel.mMap.getCameraPosition().target.latitude;
            double lng = mapAddressViewModel.mMap.getCameraPosition().target.longitude;
            String address = AppUtils.getAddress(MapAddressActivity.this, mapAddressViewModel.mMap.getCameraPosition().target);
            Intent intent = new Intent();
            intent.putExtra("lat", lat);
            intent.putExtra("lng", lng);
            intent.putExtra("address", address);
            setResult(Activity.RESULT_OK, intent);
            finish();
        });
    }

    private void onMapReady() {
        Log.e("map", "map");
        activityMapAddressBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Log.e("map", "mapRedy");
                mapAddressViewModel.mMap = googleMap;
                MapConfig mapConfig = new MapConfig(MapAddressActivity.this, mapAddressViewModel.mMap);
                mapConfig.setMapStyle();
                mapConfig.setSettings();
                mapConfig.changeMyLocationButtonLocation(activityMapAddressBinding.mapView);
                mapAddressViewModel.mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
                mapAddressViewModel.mMap.setOnMyLocationClickListener(onMyLocationClickListener);

                if (lat != 0 && lng != 0) {
                    LatLng offerLocation = new LatLng(lat, lng);
                    googleMap.addMarker(new MarkerOptions().position(offerLocation)
                            .title(getString(R.string.location)));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(offerLocation, 15));
                    googleMap.animateCamera(CameraUpdateFactory.zoomIn());
                    // Zoom out to zoom level 10, animating with a duration of 2 seconds.
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                    activityMapAddressBinding.imgMarkerCenter.setVisibility(View.GONE);
                } else
                    activityMapAddressBinding.imgMarkerCenter.setVisibility(View.VISIBLE);

                Log.e("mapReady", "Done");
//        mMap.getUiSettings().setZoomControlsEnabled(true);
                enableMyLocationIfPermitted();
            }
        });
    }


    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            () -> {
//                    mMap.setMinZoomPreference(15);
                return false;
            };


    private void enableMyLocationIfPermitted() {
        Log.e("enableMyLocation", "permitted");

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e("enableMyLocation", "request not allow");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST);
        } else if (mapAddressViewModel.mMap != null) {
            Log.e("enableMyLocation", "request allow");
            mapAddressViewModel.mMap.setMyLocationEnabled(true);
            startUpdateLocation();
            onResume();
        }
    }

    private void initViews() {
        if (!runtime_permissions()) {
            allowGPS();
        }
        if (activityMapAddressBinding.mapView != null) {
            activityMapAddressBinding.mapView.onCreate(null);
            activityMapAddressBinding.mapView.onResume();
            onMapReady();
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        setUpLocation();
    }

    private void setUpLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //Request RunTime Permission
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
            }, MY_PERMISSIONS_REQUEST_CODE);

        }
    }


    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {

                }
            };


    private void startUpdateLocation() {
        Log.e("startUpdateLocation", "Called");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Log.e("startUpdateLocation", "startUpdateLocation");
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
            Log.e("startUpdateLocation", "location");
            if (location != null) {
                Log.e("startUpdateLocation", "location not null");
                if (lat == 0 && lng == 0)
                    mapAddressViewModel.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17.0f));
                async = false;
            }
        });
    }


    private void setMapSearch() {
        place_location.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                mapAddressViewModel.moveToPlace(place);
            }

            @Override
            public void onError(com.google.android.gms.common.api.Status status) {

            }
        });

        activityMapAddressBinding.edtOrderLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View root = place_location.getView();
                root.post(new Runnable() {
                    @Override
                    public void run() {
                        root.findViewById(R.id.place_autocomplete_search_input)
                                .performClick();
                    }
                });
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (async) {
            onMapReady();
            async = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapAddressViewModel.reset();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();

    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                allowGPS();
                Log.e("enable_gps_done", "start enable here");
                if (mGoogleApiClient == null) {
                    buildGoogleApiClient();
                }
            } else {
                Log.e("check_run_time", "runtime_permissions");
                runtime_permissions();
            }
        }
    }

    private static final int ACCESS_CODE = 102;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                Log.e("result_select", "start");
                final String action = android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS;
                startActivityForResult(new Intent(action), ACCESS_CODE);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("result_select", "no_thanks");
                allowGPS();
                //Write your code if there's no result
            }
        } else if (requestCode == ACCESS_CODE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("result_select", "no_thanks");
                allowGPS();
                //Write your code if there's no result
            }
        }
    }

    public void allowGPS() {
        GPSLocation.EnableGPSAutoMatically(MapAddressActivity.this, new GPSAllowListener() {
            @Override
            public void GPSStatus(boolean isOpen) {
                if (isOpen) {
                    async = true;
                    onResume();
                }
            }
        });
    }


    protected void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    protected boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST);
            Log.e("start_run_time", "start_runtime_permissions");

            return true;
        }
        return false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("failed", "faileder");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("location", "" + location.getLatitude());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(MyContextWrapper.wrap(newBase, LanguagesHelper.getCurrentLanguage()));
        } else {
            super.attachBaseContext(newBase);
        }
    }
}
