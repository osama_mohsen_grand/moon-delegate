package grand.app.moondelegate.views.fragments.notification;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.NotificationAdapter;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentNotificationsBinding;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.notification.NotificationViewModel;

public class NotificationFragment extends BaseFragment {
    View rootView;
    private FragmentNotificationsBinding fragmentNotificationsBinding;
    private NotificationViewModel notificationViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentNotificationsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        notificationViewModel = new NotificationViewModel();
        fragmentNotificationsBinding.setNotificationViewModel(notificationViewModel);
        rootView = fragmentNotificationsBinding.getRoot();
    }

    private void setEvent() {
        notificationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, notificationViewModel.getNotificationRepository().getMessage());
                if(action.equals(Constants.NOTIFICATION)){
                    if(notificationViewModel.getNotificationRepository().getData().data.size() != 0) {
                        AppUtils.initVerticalRV(fragmentNotificationsBinding.rvNotifications, fragmentNotificationsBinding.rvNotifications.getContext(), 1);
                        fragmentNotificationsBinding.rvNotifications.setAdapter(new NotificationAdapter(notificationViewModel.getNotificationRepository().getData().data));
                    }else
                        notificationViewModel.noData();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (notificationViewModel != null) notificationViewModel.reset();
    }

}
