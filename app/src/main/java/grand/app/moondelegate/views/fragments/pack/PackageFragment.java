package grand.app.moondelegate.views.fragments.pack;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.PackageAdapter;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentPackageBinding;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.packagePayment.PackageViewModel;
import grand.app.moondelegate.views.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PackageFragment extends BaseFragment {

    View rootView;
    private FragmentPackageBinding fragmentPackageBinding;
    private PackageViewModel packageViewModel;
    private PackageAdapter packageAdapter;




    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentPackageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_package, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        packageViewModel = new PackageViewModel();
        AppUtils.initVerticalRV(fragmentPackageBinding.rvPackages, fragmentPackageBinding.rvPackages.getContext(), 1);
        fragmentPackageBinding.setPackageViewModel(packageViewModel);
        rootView = fragmentPackageBinding.getRoot();
    }

    private static final String TAG = "SplashFragment";

    private void setEvent() {
        packageViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, packageViewModel.getPackageRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.PACKAGES)){
                    packageAdapter = new PackageAdapter(packageViewModel.getPackageRepository().getPackageResponse().mData);
                    fragmentPackageBinding.rvPackages.setAdapter(packageAdapter);
                    setEventAdapter();
                }else if(action.equals(Constants.ADD)){
                    ((ParentActivity)context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });
    }

    private void setEventAdapter() {
        packageAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                packageViewModel.addPackage(packageViewModel.getPackageRepository().getPackageResponse().mData.get(position).mId);
            }
        });
    }

}
