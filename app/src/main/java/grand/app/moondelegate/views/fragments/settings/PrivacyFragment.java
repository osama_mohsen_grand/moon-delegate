package grand.app.moondelegate.views.fragments.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentPrivacyBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.app.PrivacyViewModel;


public class PrivacyFragment extends BaseFragment {
    View rootView;
    private FragmentPrivacyBinding fragmentPrivacyBinding;
    private PrivacyViewModel privacyViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentPrivacyBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_privacy, container, false);
        bind();
        rootView = fragmentPrivacyBinding.getRoot();
        return rootView;
    }

    private void bind() {
        privacyViewModel = new PrivacyViewModel();
        setEvents();
        fragmentPrivacyBinding.setPrivacyViewModel(privacyViewModel);
    }

    private void setEvents() {
        privacyViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,privacyViewModel.getPrivacyRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.PRIVACY_POLICY)){
                    privacyViewModel.setText();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        privacyViewModel.reset();
    }
}
