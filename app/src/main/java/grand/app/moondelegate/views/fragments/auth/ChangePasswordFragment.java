package grand.app.moondelegate.views.fragments.auth;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentChangePasswordBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.viewmodels.user.ChangePasswordViewModel;
import grand.app.moondelegate.views.activities.MainActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends BaseFragment {

    View rootView;
    private FragmentChangePasswordBinding fragmentChangePasswordBinding;
    private ChangePasswordViewModel changePasswordViewModel;
    public String type = "",phone="";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentChangePasswordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false);
        getData();
        bind();
        setEvent();

        return fragmentChangePasswordBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {
            phone = getArguments().getString(Constants.PHONE);
        }
    }

    private void bind() {
        changePasswordViewModel = new ChangePasswordViewModel(phone);
        fragmentChangePasswordBinding.setChangePasswordViewModel(changePasswordViewModel);
    }


    private void setEvent() {
        changePasswordViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, changePasswordViewModel.getLoginRepository().getMessage());
                Timber.e(action);
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(changePasswordViewModel.getLoginRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    if(UserHelper.getUserId() != -1) {
                        ((ParentActivity) context).finish();
                    }else{
                        getActivityBase().finishAffinity();
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra(Constants.PAGE,Constants.LOGIN);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        changePasswordViewModel.reset();

    }

}
