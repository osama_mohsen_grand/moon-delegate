package grand.app.moondelegate.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.libraries.places.internal.b;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.customviews.actionbar.BackActionBarView;
import grand.app.moondelegate.databinding.ActivityBaseBinding;
import grand.app.moondelegate.notification.NotificationGCMModel;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.MovementHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.viewmodels.common.BaseViewModel;
import grand.app.moondelegate.views.fragments.OpenLocationFragment;
import grand.app.moondelegate.views.fragments.auth.ChangePasswordFragment;
import grand.app.moondelegate.views.fragments.auth.ForgetPasswordFragment;
import grand.app.moondelegate.views.fragments.auth.LoginFragment;
import grand.app.moondelegate.views.fragments.auth.PhoneVerificationFragment;
import grand.app.moondelegate.views.fragments.auth.ProfileFragment;
import grand.app.moondelegate.views.fragments.auth.RegisterFragment;
import grand.app.moondelegate.views.fragments.auth.VerificationCodeFragment;
import grand.app.moondelegate.views.fragments.base.NoConnectionFragment;
import grand.app.moondelegate.views.fragments.chat.ChatFragment;
import grand.app.moondelegate.views.fragments.home.OrderDetailsDelegateFragment;
import grand.app.moondelegate.views.fragments.pack.PackageFragment;
import grand.app.moondelegate.views.fragments.rate.RateFragment;
import grand.app.moondelegate.views.fragments.settings.CountryFragment;
import grand.app.moondelegate.views.fragments.settings.LanguageFragment;
import grand.app.moondelegate.views.fragments.settings.SettingsFragment;
import grand.app.moondelegate.views.fragments.settings.SplashFragment;
import grand.app.moondelegate.vollyutils.MyApplication;
import timber.log.Timber;

public class BaseActivity extends ParentActivity {

    ActivityBaseBinding activityBaseBinding;
    ParentViewModel baseViewModel;

    BackActionBarView backActionBarView;
    boolean notification_checked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        LanguagesHelper.setLanguage(this,"ar");
        LanguagesHelper.changeLanguage(this,LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(),LanguagesHelper.getCurrentLanguage());
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        getNotification();
        baseViewModel = new BaseViewModel();
        activityBaseBinding.setBaseViewModel(baseViewModel);

        Log.e("page", "Done");
        if (!notification_checked) {

            if (getIntent().getExtras() != null) {
                Log.e("page", "getIntent()");
                if (getIntent().getExtras() != null) {
                    if (getIntent().hasExtra(Constants.PAGE)) {
                        if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LOGIN)) {
                            MovementHelper.addFragment(this, new LoginFragment(), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LANGUAGE)) {
                            MovementHelper.addFragment(this, getBundle(new LanguageFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CHANGE_PASSWORD)) {
                            MovementHelper.addFragment(this, getBundle(new ChangePasswordFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LOCATION)) {
                            MovementHelper.addFragment(this, getBundle(new OpenLocationFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.COUNTRIES)) {
                            MovementHelper.addFragment(this, getBundle(new CountryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.REGISTRATION)) {
                            MovementHelper.addFragment(this, getBundle(new RegisterFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PHONE_VERIFICATION)) {
                            MovementHelper.addFragment(this, getBundle(new PhoneVerificationFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.VERIFICATION)) {
                            MovementHelper.addFragment(this, getBundle(new VerificationCodeFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PROFILE)) {
                            MovementHelper.addFragment(this, getBundle(new ProfileFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PACKAGES)) {
                            MovementHelper.addFragment(this, getBundle(new PackageFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ORDER_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new OrderDetailsDelegateFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAILURE_CONNECTION)) {
                            MovementHelper.addFragment(this, getBundle(new NoConnectionFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CHAT_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new ChatFragment()), "");
                        }else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FORGET_PASSWORD)) {
                            MovementHelper.replaceFragment(this, getBundle(new ForgetPasswordFragment()), "");
                        }else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.RATE)) {
                            MovementHelper.addFragment(this, getBundle(new RateFragment()), "");
                        }else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SPLASH)) {
                            MovementHelper.addFragment(this, getBundle(new SplashFragment()), "");
                        }else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SETTINGS)) {
                            MovementHelper.addFragment(this, getBundle(new SettingsFragment()), "");
                        }
                    } else
                        MovementHelper.addFragment(this, new SplashFragment(), "");
                } else {
                    MovementHelper.addFragment(this, new SplashFragment(), "");
                }
            } else {
                MovementHelper.addFragment(this, new SplashFragment(), "");
            }
        }

    }


    private void setTitleName() {
        Timber.e("Title Name");
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            Timber.e("Title Name Done");
            backActionBarView = new BackActionBarView(this);
            backActionBarView.setTitle(getIntent().getStringExtra(Constants.NAME_BAR));
            activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);
        }
    }

    private void setTitleName(String name) {
        backActionBarView = new BackActionBarView(this);
        backActionBarView.setTitle(name);
        activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);
    }


    private Fragment getBundle(Fragment fragment) {
        Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE);
        fragment.setArguments(bundle);
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            setTitleName();
        }
        return fragment;
    }

    private static final String TAG = "BaseActivity";

    private void getNotification() {
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constants.BUNDLE_NOTIFICATION)) {
            Intent intent = new Intent(this,BaseActivity.class);
            Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE_NOTIFICATION);
            NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
            if(notificationGCMModel.notification_type == 1) {//new order
//                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.order_details));
//                intent.putExtra(Constants.PAGE, Constants.ORDER_DETAILS);
                bundle.putInt(Constants.ID, notificationGCMModel.order_id);
                bundle.putInt(Constants.STATUS, 1);
//                intent.putExtra(Constants.BUNDLE, bundle);
//                notification_checked = true;
//                startActivity(intent);

                setTitleName(ResourceManager.getString(R.string.order_details));
                OrderDetailsDelegateFragment orderDetailsFragment = new OrderDetailsDelegateFragment();
                orderDetailsFragment.setArguments(bundle);
                notification_checked = true;
                MovementHelper.replaceFragment(this, orderDetailsFragment, "");
                return;
            }else if(notificationGCMModel.notification_type == 2){
//                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
//                intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
//                bundle.putInt(Constants.ID, notificationGCMModel.order_id);
//                bundle.putInt(Constants.TYPE, notificationGCMModel.sender_type);
//                intent.putExtra(Constants.BUNDLE, bundle);
//                notification_checked = true;
//                startActivity(intent);



                //                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                setTitleName(ResourceManager.getString(R.string.chat));
//                intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
                bundle.putInt(Constants.ID, notificationGCMModel.order_id);
                bundle.putInt(Constants.TYPE, notificationGCMModel.sender_type);
//                intent.putExtra(Constants.BUNDLE, bundle);
                notification_checked = true;
//                startActivity(intent);
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setArguments(bundle);
                MovementHelper.replaceFragment(this,chatFragment , "");
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}