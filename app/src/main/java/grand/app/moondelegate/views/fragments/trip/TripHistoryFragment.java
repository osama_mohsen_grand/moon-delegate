package grand.app.moondelegate.views.fragments.trip;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.TripAdapter;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentTripHistoryBinding;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.trip.TripHistoryViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;


public class TripHistoryFragment extends BaseFragment {
    View rootView;
    private FragmentTripHistoryBinding fragmentTripHistoryBinding;
    private TripHistoryViewModel tripHistoryViewModel;
    private TripAdapter tripAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTripHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trip_history, container, false);
        bind();
        rootView = fragmentTripHistoryBinding.getRoot();
        return rootView;
    }


    private void bind() {
        tripHistoryViewModel = new TripHistoryViewModel();
        setEvents();
        fragmentTripHistoryBinding.setTripHistoryViewModel(tripHistoryViewModel);
    }

    private void setEvents() {
        tripHistoryViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,tripHistoryViewModel.getTripRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.HISTORY)){
                    if(tripHistoryViewModel.getTripRepository().getTripHistory().data.size() == 0)
                        tripHistoryViewModel.noData();
                    else{
                        AppUtils.initVerticalRV(fragmentTripHistoryBinding.rvTrips, fragmentTripHistoryBinding.rvTrips.getContext(), 1);
                        tripAdapter = new TripAdapter(tripHistoryViewModel.getTripRepository().getTripHistory().data);
                        fragmentTripHistoryBinding.rvTrips.setAdapter(tripAdapter);
                        setEventAdapter();
                    }
                }
            }
        });
    }

    private void setEventAdapter() {
        tripAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
//                int pos = (int) o;
//                Intent intent = new Intent(context, BaseActivity.class);
//                intent.putExtra(Constants.PAGE,Constants.TRIP_DETAILS);
//                intent.putExtra(Constants.NAME_BAR,getString(R.string.trip_details));
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(Constants.TRIP,tripHistoryViewModel.getTripRepository().getTripHistory().data.get(pos));
//                intent.putExtra(Constants.BUNDLE,bundle);
//                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tripHistoryViewModel.reset();
    }
}
