package grand.app.moondelegate.views.fragments.trip;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;

import grand.app.moondelegate.databinding.FragmentTripDetailsBinding;
import grand.app.moondelegate.models.trip.TripHistoryResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.trip.TripDetailsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class TripDetailsFragment extends BaseFragment {
    View rootView;
    private FragmentTripDetailsBinding fragmentTripDetailsBinding;
    private TripDetailsViewModel tripDetailsViewModel;
    private TripHistoryResponse.Datum trip;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentTripDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trip_details, container, false);
        getData();
        bind();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.TRIP)){
            trip = (TripHistoryResponse.Datum) getArguments().getSerializable(Constants.TRIP);
        }
    }

    private void bind() {
        tripDetailsViewModel = new TripDetailsViewModel(trip);
        fragmentTripDetailsBinding.setTripDetailsViewModel(tripDetailsViewModel);
        rootView = fragmentTripDetailsBinding.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tripDetailsViewModel != null) tripDetailsViewModel.reset();
    }
}
