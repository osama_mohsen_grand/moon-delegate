package grand.app.moondelegate.views.fragments.auth;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moondelegate.MultipleImageSelect.models.Image;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.databinding.FragmentRegisterBinding;
import grand.app.moondelegate.models.app.AccountTypeModel;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.models.base.IdName;
import grand.app.moondelegate.models.country.City;
import grand.app.moondelegate.models.country.Datum;
import grand.app.moondelegate.models.country.Region;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.PopUp.PopUpInterface;
import grand.app.moondelegate.utils.PopUp.PopUpMenuHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.utils.upload.FileOperations;
import grand.app.moondelegate.viewmodels.user.RegisterViewModel;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.activities.MapAddressActivity;
import grand.app.moondelegate.vollyutils.VolleyFileObject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

public class RegisterFragment extends BaseFragment {
    private View rootView;

    private FragmentRegisterBinding fragmentRegisterBinding;
    private RegisterViewModel registerViewModel;
    public String type = "", phone = "";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        Timber.e("register page");
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {
            phone = getArguments().getString(Constants.PHONE);
        }
    }

    private void bind() {
        registerViewModel = new RegisterViewModel(phone);
        fragmentRegisterBinding.setRegisterViewModel(registerViewModel);
        initPopUp();
        rootView = fragmentRegisterBinding.getRoot();
    }

    private void initPopUp() {
        fragmentRegisterBinding.edtRegisterAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AccountTypeModel> accountTypeModels = AppMoon.getAccountType();
                ArrayList<String> popUpModels = new ArrayList<>();
                for (AccountTypeModel accountTypeModel : accountTypeModels)
                    popUpModels.add(accountTypeModel.type);
                popUpMenuHelper.openPopUp(getActivity(), fragmentRegisterBinding.edtRegisterAccountType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        fragmentRegisterBinding.edtRegisterAccountType.setText(accountTypeModels.get(position).type);
                        registerViewModel.registerShopRequest.setTypeText(accountTypeModels.get(position).type);
                        registerViewModel.registerShopRequest.setType(String.valueOf(accountTypeModels.get(position).id));
                        registerViewModel.updateType();

                    }
                });

            }
        });


        fragmentRegisterBinding.edtRegisterAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AccountTypeModel> accountTypeModels = AppMoon.getAccountType();
                ArrayList<String> popUpModels = new ArrayList<>();
                for (AccountTypeModel accountTypeModel : accountTypeModels)
                    popUpModels.add(accountTypeModel.type);
                popUpMenuHelper.openPopUp(getActivity(), fragmentRegisterBinding.edtRegisterAccountType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        fragmentRegisterBinding.edtRegisterAccountType.setText(accountTypeModels.get(position).type);
                        registerViewModel.registerShopRequest.setTypeText(accountTypeModels.get(position).type);
                        registerViewModel.registerShopRequest.setType(String.valueOf(accountTypeModels.get(position).id));
                        registerViewModel.updateType();

                    }
                });

            }
        });
    }

    //
    public List<Region> regions = new ArrayList<>();
    public List<IdName> trucks = new ArrayList<>();
    Datum country = null;

    private void setEvent() {
        registerViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, registerViewModel.getRegisterRepository().getMessage());
                Timber.e(action);
                if (action.equals(Constants.REGISTRATION)) {//submitSearch register
                    toastMessage(registerViewModel.getRegisterRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                    ((ParentActivity) context).finishAffinity();
                    startActivity(intent);
                } else if (action.equals(Constants.SERVICE_SUCCESS)) {//get service type
                    //Log services
                } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                    pickImageDialogSelect();
                } else if (action.equals(Constants.SELECT_IMAGE_MULTIPLE)) {//select image profile
                    Intent intent = new Intent(context, AlbumSelectActivity.class);
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 2);
                    startActivityForResult(intent, Constants.REQUEST_CODE);
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(registerViewModel.baseError);
                } else if (action.equals(Constants.LOCATIONS)) {//select location
                    Timber.e("Locations");
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                } else if (action.equals(Constants.COLOR)) {
                    ColorPickerDialogBuilder
                            .with(context)
                            .setTitle("Choose color")
                            .initialColor(R.color.colorAccent)
                            .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                            .density(12)
                            .setOnColorSelectedListener(new OnColorSelectedListener() {
                                @Override
                                public void onColorSelected(int selectedColor) {
                                    Timber.e("color:" + selectedColor);
                                }
                            })
                            .setPositiveButton("ok", new ColorPickerClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
//                                    Timber.e("color:"+selectedColor);
                                    String hexColor = String.format("#%06X", (0xFFFFFF & selectedColor));
                                    Timber.e("color_update:" + hexColor);
                                    registerViewModel.registerShopRequest.setCar_color(hexColor);
                                    registerViewModel.notifyChange();
                                }
                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .build()
                            .show();
                } else if (action.equals(Constants.TERMS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, Constants.TYPE_TERMS);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.PAGE, Constants.SETTINGS);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.terms_and_privacy_policy));
                    startActivity(intent);
                }
            }
        });
        registerViewModel.mMutableLiveDataCountry.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, registerViewModel.getCountryRepository().getMessage());
                if (action.equals(Constants.COUNTRIES)) {//load countries
                    country = AppMoon.getCities(UserHelper.retrieveKey(Constants.COUNTRY_ID), registerViewModel.getCountryRepository().getCountriesResponse().data);
                    registerViewModel.showPage(true);
                    getTrucks();
                    fragmentRegisterBinding.edtRegisterCity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ArrayList<String> popUpCities = new ArrayList<>();
                            if (country != null) {
                                Timber.e("country:" + country.countryName);
                                for (City city : country.cities)
                                    popUpCities.add(city.cityName);
                                popUpMenuHelper.openPopUp(getActivity(), fragmentRegisterBinding.edtRegisterCity, popUpCities, new PopUpInterface() {
                                    @Override
                                    public void submitPopUp(int position) {
                                        registerViewModel.registerShopRequest.setCity_id(String.valueOf(country.cities.get(position).id));
                                        fragmentRegisterBinding.edtRegisterCity.setText(country.cities.get(position).cityName);
                                        fragmentRegisterBinding.edtRegisterRegion.setText("");
                                        regions = new ArrayList<>(country.cities.get(position).regions);
                                        getRegions();
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    private void getRegions() {
        fragmentRegisterBinding.edtRegisterRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> popUpRegions = new ArrayList<>();
                for (Region region : regions)
                    popUpRegions.add(region.regionName);
                Timber.e("size:" + popUpRegions.size());
                popUpMenuHelper.openPopUp(getActivity(), fragmentRegisterBinding.edtRegisterRegion, popUpRegions, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        registerViewModel.registerShopRequest.setRegion_id(String.valueOf(regions.get(position).id));
                        fragmentRegisterBinding.edtRegisterRegion.setText(regions.get(position).regionName);
                    }
                });
            }
        });

    }


    private void getTrucks() {
        Timber.e("getTrucks:Start");
        trucks = new ArrayList<>(country.trucks);
        fragmentRegisterBinding.edtRegisterTruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> popUpTrucks = new ArrayList<>();
                for (IdName truck : trucks)
                    popUpTrucks.add(truck.name);
                Timber.e("size:" + trucks.size());
                popUpMenuHelper.openPopUp(getActivity(), fragmentRegisterBinding.edtRegisterTruck, popUpTrucks, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        registerViewModel.registerShopRequest.setCarId(String.valueOf(trucks.get(position).id));
                        fragmentRegisterBinding.edtRegisterTruck.setText(trucks.get(position).name);
                    }
                });
            }
        });
    }


    int counter = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        Timber.e("imageName:" + registerViewModel.image_select);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, registerViewModel.image_select, Constants.FILE_TYPE_IMAGE);
            registerViewModel.setImage(volleyFileObject);
            if (registerViewModel.image_select.equals("image")) {
                fragmentRegisterBinding.imgRegisterUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
            } else if (registerViewModel.image_select.equals("car_image")) {
                registerViewModel.registerShopRequest.setCarImage(getString(R.string.car_image_had_been_selected));
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            registerViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0), data.getStringExtra("address"));
        }
        if (requestCode == Constants.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            try {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                counter = images.size();
                if (registerViewModel.image_select.equals(Constants.ID_IMAGE)) {
                    if (counter == 2) {
                        registerViewModel.registerShopRequest.setIdImage(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    } else
                        toastInfo(fragmentRegisterBinding.tilRegisterIdImage.getHelperText().toString());
                } else if (registerViewModel.image_select.equals("license_image")) {
                    if (counter == 2)
                        registerViewModel.registerShopRequest.setDrivingLicence(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    else
                        toastInfo(fragmentRegisterBinding.tilRegisterLicence.getHelperText().toString());
                }
                if (counter == 2) {
                    for (int i = 0; i < images.size(); i++) {
                        registerViewModel.setImage(new VolleyFileObject(registerViewModel.image_select + "[" + i + "]", images.get(i).path, Constants.FILE_TYPE_IMAGE));
                    }
                }
                registerViewModel.notifyChange();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        registerViewModel.reset();

    }
}
