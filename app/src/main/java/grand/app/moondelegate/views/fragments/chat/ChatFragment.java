package grand.app.moondelegate.views.fragments.chat;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devlomi.record_view.OnRecordClickListener;
import com.devlomi.record_view.OnRecordListener;
import com.rygelouv.audiosensei.player.AudioSenseiListObserver;

import java.io.File;
import java.io.IOException;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.adapter.ChatAdapter;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentChatBinding;
import grand.app.moondelegate.models.chat.ChatDetailsModel;
import grand.app.moondelegate.notification.NotificationGCMModel;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.utils.upload.FileOperations;
import grand.app.moondelegate.viewmodels.chat.ChatViewModel;
import grand.app.moondelegate.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static grand.app.moondelegate.utils.Constants.AUDIO_REQUEST_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment {


    private View rootView;
    private FragmentChatBinding fragmentChatBinding;
    private ChatViewModel chatViewModel;
    private ChatAdapter chatAdapter;
    int id = -1, type = -1; int receiver_id = 1;
    String senderName = "", senderImage = "";
    public boolean allowChat = true;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentChatBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
        if (getArguments() != null && getArguments().containsKey(Constants.ALLOW_CHAT))
            allowChat = getArguments().getBoolean(Constants.ALLOW_CHAT,true);
//        if (getArguments() != null && getArguments().containsKey(Constants.RECEIVER_ID))
//            receiver_id = getArguments().getInt(Constants.RECEIVER_ID);
        Log.d(TAG, "getData: "+receiver_id);
    }


    private void bind() {
        chatViewModel = new ChatViewModel(id,type,allowChat);
        AppUtils.initVerticalRV(fragmentChatBinding.rvChat, fragmentChatBinding.rvChat.getContext(), 1);
        fragmentChatBinding.setChatViewModel(chatViewModel);

        fragmentChatBinding.recordButton.setRecordView(fragmentChatBinding.recordView);
        fragmentChatBinding.recordButton.setSoundEffectsEnabled(false);

//        fragmentChatBinding.recordView.setOnRecordListener(this);
        fragmentChatBinding.recordView.setOnRecordListener(new OnRecordListener() {
            @Override
            public void onStart() {
                Log.e(TAG, "onStart: Done" );
                if(checkPermissions())
                    record();
            }

            @Override
            public void onCancel() {
                stopMedia();
            }

            @Override
            public void onFinish(long recordTime) {
                finishAudio(recordTime);
            }

            @Override
            public void onLessThanSecond() {
                stopMedia();
            }
        });


        fragmentChatBinding.recordButton.setListenForRecord(true);

        //ListenForRecord must be false ,otherwise onClick will not be called
        fragmentChatBinding.recordButton.setOnRecordClickListener(new OnRecordClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: Start Click" );
                if(checkPermissions()){
                    Log.e(TAG, "checkPermissions:done " );
                    record();
                }else {
                    Log.e(TAG, "checkPermissions:notDone " );
                    RequestPermissions();
                }
            }
        });
        AudioSenseiListObserver.getInstance().registerLifecycle(getLifecycle());
        rootView = fragmentChatBinding.getRoot();
    }

    public boolean checkPermissions() {
        int result = ContextCompat.checkSelfPermission(ChatFragment.this.getActivity(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(ChatFragment.this.getActivity(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void setEvent() {
        chatViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, chatViewModel.getChatRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.CHAT)) {
                    setData();
                    chatAdapter = new ChatAdapter(senderName, senderImage, chatViewModel.getChatRepository().getChatDetailsResponse().chats);
                    chatViewModel.receiver_id = chatViewModel.getChatRepository().getChatDetailsResponse().delegate.id;
                    fragmentChatBinding.rvChat.setAdapter(chatAdapter);
                    fragmentChatBinding.rvChat.scrollToPosition(chatAdapter.chats.size() - 1);

//                    fragmentChatBinding.rvChat.setVisibility(View.GONE);
//
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            // Hide your View after 3 seconds
//                            fragmentChatBinding.rvChat.setVisibility(View.VISIBLE);
//                        }
//                    }, 1000);


                    setEventAdapter();
                } else if (action.equals(Constants.SELECT_IMAGE)) {
                    pickImageDialogSelect();
                } else if (action.equals(Constants.CHAT_SEND)) {
                    chatAdapter.add(chatViewModel.getChatRepository().getChatSendResponse().data);
                    chatViewModel.text = "";
                    chatViewModel.notifyChange();
                    fragmentChatBinding.rvChat.scrollToPosition(chatAdapter.chats.size() - 1);
                    AppUtils.hideKeyboard(getActivityBase(),rootView);
                }
//
//                } else if (action.equals(Constants.DELETE)) {
//                    toastMessage(chatViewModel.getCategoryRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
//                    homeAdapter.remove(chatViewModel.category_delete_position);
//                }
            }
        });
    }

    private void setData() {
        if(UserHelper.getUserId() != -1) {
            if (chatViewModel.getChatRepository().getChatDetailsResponse().delegate.type != Integer.parseInt(UserHelper.getUserDetails().type)) {
                senderName = chatViewModel.getChatRepository().getChatDetailsResponse().delegate.name;
                senderImage = chatViewModel.getChatRepository().getChatDetailsResponse().delegate.image;
            }else{
                senderName = chatViewModel.getChatRepository().getChatDetailsResponse().user.name;
                senderImage = chatViewModel.getChatRepository().getChatDetailsResponse().user.image;
            }
        }
    }


    private void setEventAdapter() {
        chatAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
            }
        });
    }

    private BroadcastReceiver chat = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(chatViewModel != null && chatAdapter != null){
                Bundle bundle = intent.getBundleExtra(Constants.BUNDLE_NOTIFICATION);
                NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
                ChatDetailsModel chatDetailsModel = new ChatDetailsModel();
                chatDetailsModel.senderName = senderName;
                chatDetailsModel.senderImage = senderImage;
                if(notificationGCMModel.type_data == 1)
                    chatDetailsModel.message = notificationGCMModel.body;
                else if(notificationGCMModel.type_data == 2)
                    chatDetailsModel.image = notificationGCMModel.image;
                else
                    chatDetailsModel.audio = notificationGCMModel.audio;
                chatDetailsModel.type = notificationGCMModel.type+"";
                chatAdapter.update(chatDetailsModel);
                fragmentChatBinding.rvChat.scrollToPosition(chatAdapter.chats.size() - 1);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(chat, new IntentFilter(Constants.CHAT));
    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(chat);
    }


    private static final String TAG = "ChatFragment";
    private void RequestPermissions() {
        Log.e(TAG, "RequestPermissions: True" );
        ActivityCompat.requestPermissions(ChatFragment.this.getActivity(), new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, AUDIO_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: before start result" );
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        Log.e(TAG, "onRequestPermissionsResult: start result" );
        switch (requestCode) {
            case AUDIO_REQUEST_CODE:
                if (grantResults.length> 0) {
                    Log.e(TAG, "onRequestPermissionsResult: permission" );
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] ==  PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) {
                        onResume();
                    }
                }
                break;
        }
    }

    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private static String mFileName = null;
    public void record(){
        Log.e(TAG, "record: " );
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/chat.mp3";
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(mFileName);
        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            Log.e(TAG, "IOException: "+e.getMessage() );
        }
        fragmentChatBinding.rlChatTextContainer.setVisibility(View.GONE);

    }


    public void finishAudio(long time){
        stopMedia();
        File file = new File(mFileName);
        if(file.exists()){

            VolleyFileObject volleyFileObject = FileOperations.getFileVolleyFileObject(mFileName, "audio", AUDIO_REQUEST_CODE);

            chatViewModel.setFile(volleyFileObject);
        }
    }


    public void stopMedia(){
        Log.e(TAG, "stopMedia: stop" );
        if(mRecorder != null) {
            try {
                mRecorder.stop();
//                mRecorder.reset();
//                mRecorder.release();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        fragmentChatBinding.rlChatTextContainer.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            chatViewModel.setImage(volleyFileObject);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        chatViewModel.reset();

    }

}
