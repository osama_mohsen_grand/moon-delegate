package grand.app.moondelegate.views.fragments.contactAndSupport;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.BaseFragment;
import grand.app.moondelegate.databinding.FragmentContactUsBinding;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.viewmodels.contact.ContactUsViewModel;

public class ContactUsFragment extends BaseFragment {

    private FragmentContactUsBinding binding;
    private ContactUsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false);
        bind();
        setEvent();
        return binding.getRoot();
    }

    private void bind() {
        viewModel = new ContactUsViewModel();
        binding.setContactUsViewModel(viewModel);

    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getRepository().getMessage());
                if(action.equals(Constants.SUCCESS)){
                    toastMessage(viewModel.getRepository().getMessage());
                    viewModel.request.setEmail("");
                    viewModel.request.setMessage("");
                    viewModel.request.setName("");
                    viewModel.request.setSubject("");
                    viewModel.notifyChange();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }
}
