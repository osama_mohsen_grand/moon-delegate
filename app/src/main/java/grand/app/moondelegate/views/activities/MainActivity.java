package grand.app.moondelegate.views.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.customviews.actionbar.HomeActionBarView;
import grand.app.moondelegate.customviews.menu.NavigationDrawerView;
import grand.app.moondelegate.databinding.ActivityMainBinding;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.user.UpdateTokenRequest;
import grand.app.moondelegate.models.user.profile.User;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.MovementHelper;
import grand.app.moondelegate.utils.dialog.DialogHelper;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.views.fragments.home.HomeDelegateFragment;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.MyApplication;
import grand.app.moondelegate.vollyutils.URLS;
import timber.log.Timber;

public class MainActivity extends ParentActivity {
    public HomeActionBarView homeActionBarView = null;
    public NavigationDrawerView navigationDrawerView;
    public String message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagesHelper.changeLanguage(this,LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(),LanguagesHelper.getCurrentLanguage());
        initializeToken();
        bind();
        navigationDrawerView.homePage();
        getData();
        setEvents();
        changeStatus();

    }


    private void getData() {
        if (getIntent() != null && getIntent().hasExtra(Constants.MESSAGE)) {
            message = getIntent().getStringExtra(Constants.MESSAGE);
//            new OrderCodeViewModel(this,message);
        }
    }


    ActivityMainBinding activityMainBinding;

    private View bind() {
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        homeActionBarView = new HomeActionBarView(this);
        navigationDrawerView = new NavigationDrawerView(this);
        activityMainBinding.llBaseContainer.addView(navigationDrawerView);
        navigationDrawerView.layoutNavigationDrawerBinding.llBaseActionBarContainer.addView(homeActionBarView, 0);
        homeActionBarView.setNavigation(navigationDrawerView);
        homeActionBarView.connectDrawer(navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu, true);
        navigationDrawerView.setActionBar(homeActionBarView);
        Timber.e("main start check");
        if (getIntent() != null && getIntent().hasExtra(Constants.TYPE)) {
            Timber.e("main start TYPE");
            if (getIntent().getStringExtra(Constants.TYPE).equals(Constants.ORDERS)) {
                Timber.e("main have TYPE");
                navigationDrawerView.previousOrders();
            } else {
                Timber.e("main not have TYPE");
                navigationDrawerView.homePage();
            }
        } else
            navigationDrawerView.homePage();
        return activityMainBinding.getRoot();
    }


    private void setEvents() {
        navigationDrawerView.mutableLiveData.observe(this, new Observer<Object>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onChanged(@Nullable Object object) {
                if (object instanceof String) {
                    String text = (String) object;
                    if (text.equals(Constants.LOGOUT)) {
                        logoutApp();
                    } else {
                        homeActionBarView.setTitle(text);
                        navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawer(Gravity.START);
                    }
                } else if (object instanceof Boolean) {
                    //profile
                    Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.PROFILE);
                    startActivityForResult(intent, Constants.RESULT_PROFILE_RESPONSE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_PROFILE_RESPONSE) {
            if (resultCode == Activity.RESULT_OK) {
                navigationDrawerView.setHeader();
            }
        }
    }//onActivityResult


    @Override
    public void onBackPressed() {

        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                super.onBackPressed();
                return;
            }
            HomeDelegateFragment homeFragment = (HomeDelegateFragment) getSupportFragmentManager().findFragmentByTag(Constants.HOME);
            if (homeFragment != null) {
                int[] ids = {R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no};
                DialogHelper.showDialogHelper(MainActivity.this, R.layout.dialog_close_app, ids, (dialog, view) -> {
                    switch (view.getId()) {
                        case R.id.tv_dialog_close_app_yes:
                            dialog.dismiss();
                            finish();
                            break;
                        case R.id.tv_dialog_close_app_no:
                            dialog.dismiss();
                            break;

                    }
                });
            } else {
                if (navigationDrawerView != null)
                    navigationDrawerView.homePage();
            }
            return;
        } else
            finish();

    }

    private void changeStatus() {
        homeActionBarView.layoutActionBarHomeBinding.switchAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = (UserHelper.getUserDetails().status == 1 ? "2" : "1");
                showProgress();
                new ConnectionHelper(new ConnectionListener() {
                    @Override
                    public void onRequestSuccess(Object response) {
                        hideProgress();
                        StatusMsg statusMsg = (StatusMsg) response;
                        if (statusMsg != null) {
                            User user = UserHelper.getUserDetails();
                            user.status = Integer.parseInt(status);
                            UserHelper.saveUserDetails(user);
                            homeActionBarView.switchStatus();
                        }
                    }

                    @Override
                    public void onRequestError(Object error) {
                        super.onRequestError(error);
                        hideProgress();
                    }
                }).requestJsonObject(Request.Method.POST, URLS.FIREBASE, new UpdateTokenRequest(UserHelper.retrieveKey(Constants.TOKEN), status), StatusMsg.class);
            }
        });
    }


    public void logoutApp() {
        int[] ids = {R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no};
        DialogHelper.showDialogHelper(MainActivity.this, R.layout.dialog_logout_app, ids, (dialog, view) -> {
            switch (view.getId()) {
                case R.id.tv_dialog_close_app_yes:
                    dialog.dismiss();
                    finishAffinity();
                    UserHelper.clearUserDetails();
                    Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                    startActivity(intent);
                    break;
                case R.id.tv_dialog_close_app_no:
                    dialog.dismiss();
                    break;

            }
        });
    }
}
