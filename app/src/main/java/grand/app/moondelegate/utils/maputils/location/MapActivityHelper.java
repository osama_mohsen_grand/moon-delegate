package grand.app.moondelegate.utils.maputils.location;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import grand.app.moondelegate.utils.maputils.base.MapConfig;

public class MapActivityHelper implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    private static final String TAG = "MapActivityHelper";
    Activity activity;
    Fragment fragment;
    MapConfig mapConfig;
    GoogleApiClient mGoogleApiClient;
    FusedLocationProviderClient fusedLocationProviderClient = null;
    private LocationRequest mLocationRequest;

    public MapActivityHelper(Activity activity, MapConfig mapConfig) {
        this.activity = activity;
        this.mapConfig = mapConfig;
    }

    public void initFused(){
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
    }

    public void startUpdateLocation(LocationListenerEvent locationListener) {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(fusedLocationProviderClient == null)
            initFused();
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {
                Log.e(TAG, "startUpdateLocation: "+location.getLongitude() );
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                mapConfig.getGoogleMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
                locationListener.update(latLng,false);
            }
        });
    }

    public void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public GoogleApiClient getmGoogleApiClient() {
        if(mGoogleApiClient == null){
            buildGoogleApiClient();
        }
        return mGoogleApiClient;
    }

    public void updateLocationRequestInterval(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        updateLocationRequestInterval();
        mapConfig.getmLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(activity);
        settingsClient.checkLocationSettings(locationSettingsRequest);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void setUpLocation(int locationRequest, int locationPermission) {

    }
}
