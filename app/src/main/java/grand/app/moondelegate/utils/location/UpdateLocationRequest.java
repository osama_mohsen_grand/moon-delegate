package grand.app.moondelegate.utils.location;

public class UpdateLocationRequest {
    public String tech_id;
    public String jwt;
    public double lat;
    public double lng;

    public UpdateLocationRequest(String tech_id, String jwt, double lat, double lng) {
        this.tech_id = tech_id;
        this.jwt = jwt;
        this.lat = lat;
        this.lng = lng;
    }
}
