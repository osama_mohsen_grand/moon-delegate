package grand.app.moondelegate.utils.storage.location;

import java.io.Serializable;


public class LocationPosition implements Serializable {


    private double lat;
    private double lng;

    public LocationPosition(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }


    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}