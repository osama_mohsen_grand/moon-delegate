package grand.app.moondelegate.utils.maputils.direction;

public interface DirectionDistance {
    public void receive(int distanceM);
}
