package grand.app.moondelegate.utils.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableInt;
import grand.app.moondelegate.R;


/**
 * Created by osama on 12/16/2017.
 */

public class ImageLoaderHelper {

    // compile 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
    public ImageLoaderHelper(Context context) {
        DisplayImageOptions imageOptions = new DisplayImageOptions.Builder()
//                .cacheInMemory(true)
//                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(imageOptions).build();


        ImageLoader.getInstance().init(config);
    }

    public static void ImageLoaderLoad(final Context context, String url, final ImageView image, final ObservableInt avi){
        ImageLoader.getInstance().displayImage(
                url,image,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        if(avi != null) avi.set(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        if(avi != null) avi.set(View.GONE);
                        image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if(avi != null) avi.set(View.GONE);
                    }
                }
        );
    }

    public static void ImageLoaderLoad(final Context context, String url, final ImageView image, final ProgressBar progressBar){
        ImageLoader.getInstance().displayImage(
                url,image,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        if(context != null && progressBar != null)
                           progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        if(context != null && progressBar != null)
                            progressBar.setVisibility(View.GONE);
                        image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if(context != null && progressBar != null)
                            progressBar.setVisibility(View.GONE);
                    }
                }
        );
    }


    public static void ImageLoaderLoad(final Context context, String url, final ImageView image){
        ImageLoader.getInstance().displayImage(
                url,image,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    }
                }
        );
    }

}
