package grand.app.moondelegate.utils.upload;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class CompressObject implements Serializable {
    private Bitmap image = null;
    private ByteArrayOutputStream byteStream = null;
    private byte[] bytes;

    public Bitmap getImage() {
        return image;
    }


    public void setImage(Bitmap image) {
        this.image = image;
    }

    public ByteArrayOutputStream getByteStream() {
        return byteStream;
    }

    public void setByteStream(ByteArrayOutputStream byteStream) {
        this.byteStream = byteStream;
    }


    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }

}
