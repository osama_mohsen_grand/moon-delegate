package grand.app.moondelegate.utils.maputils.background;

import java.util.ArrayList;

import grand.app.moondelegate.models.location.UpdateLocation;
import grand.app.moondelegate.repository.TripRepository;
import grand.app.moondelegate.utils.storage.location.LocationPosition;

public class UpdateLocationBackgroundService {

    TripRepository tripRepository = new TripRepository(null);
    ArrayList<LocationPosition> arrayList = null;
    ArrayList<String> locations = null;
    public void updateLocations(int trip_id, ArrayList<LocationPosition> locations) {
        arrayList =  new ArrayList<>(locations);
        this.locations = new ArrayList<>();
        for(LocationPosition locationPosition : arrayList) {
            this.locations.add(locationPosition.getLat()+"");
            this.locations.add(locationPosition.getLng()+"");
        }
        UpdateLocation updateLocation = new UpdateLocation(trip_id, this.locations);
        tripRepository.updateLocations(updateLocation);
    }
}
