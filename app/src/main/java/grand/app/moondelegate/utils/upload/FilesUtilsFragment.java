package grand.app.moondelegate.utils.upload;


import android.Manifest;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import okhttp3.MultipartBody;


public class FilesUtilsFragment {

    public static final int FILES_BUFFER_SIZE = 70000000;
    public static final int PERMISSION_LOAD_IMAGE = 4000;
    public static final int PERMISSION_LOAD_VIDEO = 4001;
    public static final int RESULT_LOAD_IMAGE = 2000;
    public static final int RESULT_LOAD_VIDEO = 2001;
    public static final int PICK_IMAGE_CAMERA = 2002;
    public static final int PICK_IMAGE_REQUEST = 2003;

    public static FileOperations fileOperations;
    public static ImageCompression imageCompression;


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }




    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getFilePath(final Fragment fragment, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(fragment.getContext(), uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(fragment, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(fragment, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(fragment, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getFilePath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    private static String getDataColumn(Fragment fragment, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = fragment.getActivity().getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int columnIndex = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(columnIndex);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int columnIndex = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(columnIndex);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public static File getFile(String path) {
        return new File(path);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static File getFile(final Fragment fragment, final Uri uri) {
        return new File(getFilePath(fragment, uri));
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static File getFile(final Context fragment, final Uri uri) {
        return new File(getFilePath(fragment, uri));
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getFileAsString(final Fragment fragment, final Uri uri) {
        return pathToString(getFilePath(fragment, uri));
    }

    private static String pathToString(String selectedPath) {
        return fileToString(new File(selectedPath));
        //        return fileToString(getCompresedImage(fragment, new File(selectedPath),imageConfigurations));

    }


    private static String fileToString(File selectedFile) {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        try {
            FileInputStream inputStream = new FileInputStream(selectedFile);
            int bufferSize = FILES_BUFFER_SIZE;
            byte[] buffer = new byte[bufferSize];
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Base64.encodeToString(byteBuffer.toByteArray(), 0);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return null;
    }


    public static void selectImage(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(fragment.getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                fragment.requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_LOAD_IMAGE);
            } else {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                fragment.startActivityForResult(intent, RESULT_LOAD_IMAGE);
            }
        }
    }


    public static void selectVideo(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(fragment.getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                fragment.requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_LOAD_VIDEO);
            } else {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("video/*");
                fragment.startActivityForResult(intent, RESULT_LOAD_VIDEO);
            }
        }
    }

    public static void onRequestPermissionsResult(Fragment fragment, int requestCode, String permissions[], int[] grantResults) {
        Log.e("catch", "done");
        if (requestCode == PERMISSION_LOAD_IMAGE) {
            if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                fragment.startActivityForResult(intent, RESULT_LOAD_IMAGE);
            }
        } else if (requestCode == PERMISSION_LOAD_VIDEO) {
            if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("video/*");
                fragment.startActivityForResult(intent, RESULT_LOAD_VIDEO);
            }
        }
    }

    public static String path = "";
    public static MultipartBody.Part onActivityResult(Fragment fragment, String fileName, int requestCode, Intent data) {
        MultipartBody.Part file = null;
        File fileRequest = null;
//        if (data != null && (requestCode == FilesUtilsFragment.RESULT_LOAD_IMAGE || requestCode == FilesUtilsFragment.RESULT_LOAD_VIDEO)) {
//            Log.e("result",""+requestCode);
//            fileOperations = new FileOperations();
//            imageCompression = new ImageCompression();
//            Uri returnUri = data.getData();
//            path = fileOperations.getPath(MyApplication.getInstance(), returnUri);
//            Log.e("path",path);
//            if (requestCode == FilesUtilsFragment.RESULT_LOAD_VIDEO) {
//                Log.e("result","video");
//                fileRequest = new File(path);
//            } else if (requestCode == FilesUtilsFragment.RESULT_LOAD_IMAGE) {
//                Log.e("result","image");
//                Log.e("path", path);
//                Bitmap bitmap = imageCompression.compressImageBitmap(path, "user");
//                fileRequest = imageCompression.bitmapToFile(bitmap);
//                Log.e("bitmap", String.valueOf(bitmap));
//            }
//            if (fileRequest != null && fileRequest.exists()) {
//                Log.e("request","not Null");
//                RequestBody mFile = null;
//                if (requestCode == FilesUtilsFragment.RESULT_LOAD_IMAGE)
//                    mFile = RequestBody.create(MediaType.parse("image/*"), fileRequest);
//                else
//                    mFile = RequestBody.create(MediaType.parse("video/*"), fileRequest);
//                file = MultipartBody.Part.createFormData(fileName, fileRequest.getName(), mFile);
//            }
//        }
        return file;
    }

    public static MultipartBody.Part extractImageFromVideo(String fileName , String file_path){
        MultipartBody.Part file = null;
//        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(file_path,
//                MediaStore.Images.Thumbnails.MINI_KIND);
//        File fileRequest = imageCompression.bitmapToFile(bitmap);
//        Log.e("bitmap", String.valueOf(bitmap));
//        if (fileRequest != null && fileRequest.exists()) {
//            Log.e("request","not Null");
//            RequestBody mFile = null;
//            mFile = RequestBody.create(MediaType.parse("image/*"), fileRequest);
//            file = MultipartBody.Part.createFormData(fileName, fileRequest.getName(), mFile);
//        }
        return file;
    }
}