package grand.app.moondelegate.utils.location;
import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import grand.app.moondelegate.BuildConfig;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentActivity;


/**
 * Created by Kintan on 16/8/18.
 */

public class LocationServiceDemo extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static LocationServiceDemo locationService;
    private static GoogleApiClient mGoogleApiClient;
    private static LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedProviderClient;
    private MyLocationCallback mMyLocationCallback;
    private Location curLocation;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            showNotificationAndStartForegroundService();

        locationService = this;
        init();
    }

    //Google location Api build
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mMyLocationCallback = new MyLocationCallback();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(5.0f);

        requestUpdate();

    }

    //Start Foreground ProductList and Show Notification to user for Android O and higher Version
    private void showNotificationAndStartForegroundService() {

        final String CHANNEL_ID = BuildConfig.APPLICATION_ID.concat("_notification_id");
        final int REQUEST_CODE = 1;

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                REQUEST_CODE, new Intent(this, ParentActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,
                CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(false)
                .setContentIntent(pendingIntent);

        startForeground(12025, notificationBuilder.build());
    }

    public void requestUpdate() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedProviderClient.requestLocationUpdates(mLocationRequest, mMyLocationCallback,
                Looper.myLooper());
    }

    public void removeUpdate() {
        mFusedProviderClient.removeLocationUpdates(mMyLocationCallback);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        buildGoogleApiClient();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        buildGoogleApiClient();
    }

    private void init() {
        buildGoogleApiClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mFusedProviderClient = LocationServices.getFusedLocationProviderClient(LocationServiceDemo.this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return START_STICKY;
        }
        mFusedProviderClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {

                curLocation = location;

            }
        });
        if (mGoogleApiClient.isConnected()) {
            createLocationRequest();
        } else {
            buildGoogleApiClient();
        }
        return START_STICKY;
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        startService();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        startService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        startService();
    }

    public void startService() {
        startService(new Intent(LocationServiceDemo.this, LocationServiceDemo.class));
    }

    public class MyLocationCallback extends LocationCallback {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            //get your location here
            if (locationResult.getLastLocation() != null) {
                for (Location location : locationResult.getLocations()) {
                    curLocation = location;
                }
            }
        }

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            super.onLocationAvailability(locationAvailability);
        }
    }

    private class MyBinder extends Binder {
        LocationServiceDemo getService() {
            return LocationServiceDemo.this;
        }
    }
}