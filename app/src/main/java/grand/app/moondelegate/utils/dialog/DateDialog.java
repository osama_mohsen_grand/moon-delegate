package grand.app.moondelegate.utils.dialog;

import android.app.DatePickerDialog;
import android.content.Context;

import java.util.Calendar;

import grand.app.moondelegate.R;


public class DateDialog {

    public static DatePickerDialog initCalender(Context context, DatePickerDialog.OnDateSetListener datePickerDialog){
        Calendar calendar = Calendar.getInstance();

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog dPickerDialog =  new DatePickerDialog(context, R.style.datepicker,datePickerDialog, year, month, day);
        dPickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        return dPickerDialog;
    }

    public static String dateConvert(Context context, int year , int month , int day){
        int month_select = month+1;
        String month_final = String.valueOf(month_select),day_final = String.valueOf(day);
        if(month_select < 10){

            month_final = "0" + month_final;
        }
        if(day < 10){
            day_final  = "0" + day_final ;
        }
        String date_show = String.valueOf(year) + "-" + String.valueOf(month_final) + "-" + String.valueOf(day_final);
        return date_show;
    }

}
