package grand.app.moondelegate.utils.maputils.base;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import grand.app.moondelegate.R;
import grand.app.moondelegate.models.helper.MarkerHelper;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.SharedPreferenceHelper;
import grand.app.moondelegate.vollyutils.MyApplication;


public class MapConfig {
    public List<LatLng> markers_service = new ArrayList<>();
    ArrayList<Marker> markers = new ArrayList<>();
    private static final String TAG = "MapConfig";
    Context context;
    GoogleMap mMap;
    Marker driverMarker;
    public Polyline direction;
    public double distanceKm;

    public MapConfig(Context context, GoogleMap map) {
        this.context = context;
        this.mMap = map;
    }

    public void setMapStyle() {
        try {
            boolean isSuccess = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map));
            if (!isSuccess)
                Log.e("errorMapStyle", "errorMap");
        } catch (Resources.NotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public void setSettings() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        mMap.setTrafficEnabled(true);
//        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
//        mMap.getUiSettings().setZoomControlsEnabled(true);
//        mMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    public void setPadding(int left, int top, int right, int bottom) {
        if (mMap != null) mMap.setPadding(left, top, right, bottom);
    }

    public void setPaddingRunTime(int left, int top, int right, int bottom) {
        if (mMap != null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    getGoogleMap().setPadding(left, top, right, bottom);
                }
            });

        }
    }

    public void setDriverMarker(LatLng driverLatLng) {
        SharedPreferenceHelper.saveKey(Constants.DRIVER_LAT, "" + driverLatLng.latitude);
        SharedPreferenceHelper.saveKey(Constants.DRIVER_LNG, "" + driverLatLng.longitude);
//        if(driverMarker == null || !driverMarker.isVisible()) {
        if (driverMarker != null) driverMarker.remove();
        driverMarker = addMarker(driverLatLng, R.drawable.ic_pin_car3, "");
//        }
    }

    public void enableLocationButton() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.setOnMyLocationButtonClickListener(() -> true);
    }

    public void saveLastLocation(LatLng location) {
        SharedPreferenceHelper.saveKey(Constants.LAT, String.valueOf(location.latitude));
        SharedPreferenceHelper.saveKey(Constants.LNG, String.valueOf(location.longitude));
    }

    public void checkLastLocation() {
        if (!SharedPreferenceHelper.getKey(Constants.LAT).equals("") && !SharedPreferenceHelper.getKey(Constants.LAT).equals(""))
            moveCamera(new LatLng(Double.parseDouble(SharedPreferenceHelper.getKey(Constants.LAT)), Double.parseDouble(SharedPreferenceHelper.getKey(Constants.LNG))));
    }

    public void moveCamera(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
    }

    public void moveCamera(ArrayList<LatLng> latLngs) {
        if (latLngs.size() > 0) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng latLng : latLngs) {
                builder.include(latLng);
            }
            int padding = 250; // offset from edges of the map in pixels
            LatLngBounds bounds = builder.build();
            final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            /**call the map call back to know map is loaded or not*/
            getGoogleMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    /**set animated zoom camera into map*/
                    getGoogleMap().animateCamera(cu);
                }
            });
        }
    }

    public void setRoute(Polyline direction) {
        this.direction = direction;
    }

    public void removeRoute() {
        if (direction != null)
            direction.remove();
    }
    public Marker getDriverMarker() {
        return driverMarker;
    }


    public void moveCamera(List<MarkerHelper> points) {
        markers.clear();
        int counter = 0;
        for (MarkerHelper point : points) {
            if (point != null && point.latLng != null && point.latLng.latitude != 0 && point.latLng.longitude != 0) {
                int resource = (counter == 0 ? R.drawable.ic_map_pin : R.drawable.ic_marker_dest);
                Marker marker = mMap.addMarker(new MarkerOptions().position(point.latLng)
                        .icon(BitmapDescriptorFactory.fromResource(resource))
                        .title(point.text_select));
                ;
                markers.add(marker);
            }
            counter++;
        }
        int padding = 50; // offset from edges of the map in pixels
        if (markers.size() > 0) {
            Log.e(TAG, "moveCamera: " + markers.size());
            LatLngBounds latLngBounds = zoomCamera(markers);
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(latLngBounds, padding);
            mMap.animateCamera(cu);
        }
    }

    public Marker addMarkerList(LatLng latLng, int icon, String title) {
        Log.e(TAG, "addMarkerList: " + latLng.latitude + "," + latLng.longitude + "," + title);
        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(icon))
                .title(title));
        markers.add(marker);
        return marker;
    }

    public Marker addMarker(LatLng latLng, int icon, String title) {
        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(icon))
                .title(title));
        return marker;
    }

    public ArrayList<Marker> getMarkers() {
        return markers;
    }

    public void clearMarkers() {
        markers.clear();
    }

    public void changeMyLocationButtonLocation(MapView mapView) {
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);

            layoutParams.setMargins(0, 0, 0, 0);
        }
    }

    public LatLngBounds zoomCamera(ArrayList<Marker> markers) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        return bounds;
    }

    public void navigation(double lat, double lng) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lng));
        context.startActivity(intent);
    }

    public void navigation(LatLng latLngPicker, LatLng latLngDestination) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + latLngPicker.latitude + "," + latLngPicker.longitude + "&daddr=" + latLngDestination.latitude + "," + latLngDestination.longitude + ""));
        context.startActivity(intent);
    }

    public void setLocationButtonListeners() {
        mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyLocationClickListener);
    }

    private LocationRequest mLocationRequest;
    private static int UPDATE_INTERVAL = 5000 * 60;
    private static int FATEST_INTERVAL = 3000;
    private static int DISPLACEMENT = 10;


    public void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    public LocationRequest getmLocationRequest() {
        if (mLocationRequest == null) {
            createLocationRequest();
        }
        return mLocationRequest;
    }

    public void setmLocationRequest(LocationRequest mLocationRequest) {
        this.mLocationRequest = mLocationRequest;
    }

    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {
                    Log.e(TAG, "onMyLocationClick: " + location.getLatitude() + "," + location.getLongitude());
                }
            };

    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            () -> {
//                    mMap.setMinZoomPreference(15);
                return false;
            };


    public GoogleMap getGoogleMap() {
        return mMap;
    }

    ArrayList<LatLng> latLngPointsDirections = new ArrayList();

    public void setLatLngPointsDirections(ArrayList<LatLng> pointsDirections) {
        latLngPointsDirections = new ArrayList(pointsDirections);
    }

    public ArrayList<LatLng> getLatLngPointsDirections() {
        return latLngPointsDirections;
    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses = null;
        String address = "";
        geocoder = new Geocoder(MyApplication.getInstance(), Locale.ENGLISH);
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.size() > 0 && addresses.get(0).getAddressLine(0) != null) {
                address = addresses.get(0).getAddressLine(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (address.equals("")) address = ResourceManager.getString(R.string.your_location);
        return address;
    }

    public Double setDistanceWithKilo(int distance_meters) {
        distanceKm = (distance_meters / 1000); //by kilo
        return distanceKm;
    }

}
