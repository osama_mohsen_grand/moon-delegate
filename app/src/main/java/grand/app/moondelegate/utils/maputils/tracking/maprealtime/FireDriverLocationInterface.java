package grand.app.moondelegate.utils.maputils.tracking.maprealtime;

import com.google.android.gms.maps.model.LatLng;

public interface FireDriverLocationInterface {
    void getDriverLocation(LatLng driverLatLng);
}
