package grand.app.moondelegate.utils.maputils.tracking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.maputils.location.MapActivityHelper;
import grand.app.moondelegate.utils.maputils.permission.MapPermissionActivity;
import grand.app.moondelegate.utils.maputils.tracking.maprealtime.FireDriverLocationInterface;
import grand.app.moondelegate.utils.maputils.tracking.maprealtime.FireUpdateLocationInterface;
import grand.app.moondelegate.utils.maputils.tracking.maprealtime.MapPrescience;
import io.reactivex.annotations.NonNull;

public class FireTracking implements
        GoogleMap.OnMyLocationChangeListener,
        LocationListener {
    private static final String TAG = "FireTracking";
    public com.google.android.gms.location.LocationCallback locationCallback;
    public Location driverLocation;
    Activity activity;
    MapPermissionActivity mapPermission;
    MapConfig mapConfig;
    public String id = "";
    boolean validUpdateFirebaseLocation = false;

    public FusedLocationProviderClient fusedLocationProviderClient;
    private MapActivityHelper mapActivityHelper;
    MapPrescience mapPrescience;

    public FireTracking(Activity activity, MapConfig mapConfig, MapActivityHelper mapActivityHelper) {
        this.activity = activity;
        id = "10";//TODO here change no to user_id
        this.mapConfig = mapConfig;
        this.mapPermission = new MapPermissionActivity(activity);
        this.mapActivityHelper = (mapActivityHelper != null ? mapActivityHelper : new MapActivityHelper(activity, mapConfig));
        new FireRealTime();//update fire-token in Database (real-time & grand)
        mapPrescience = new MapPrescience(mapConfig);
        init();
    }

    @SuppressLint("MissingPermission")
    private void init() {
        if (mapPermission.validLocationPermission()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);//install fused track location
            buildLocationCallBack();
            mapConfig.createLocationRequest();
            mapActivityHelper.getmGoogleApiClient();
            setUpLocation();
            fusedLocationProviderClient.requestLocationUpdates(mapConfig.getmLocationRequest(), locationCallback, Looper.myLooper());
            mapPrescience.connect();
        } else {
            Log.e(TAG, "init: invalid enable location permission");
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        }
    }


    public void startUpdateLocation(){
        if(mapPermission.validLocationPermission()) {
            validUpdateFirebaseLocation = true;
            displayLocation();
        }
    }


    private void buildLocationCallBack() {
        LocationServices.getFusedLocationProviderClient(activity);
        locationCallback = new com.google.android.gms.location.LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                driverLocation = locationResult.getLocations().get(locationResult.getLocations().size() - 1);
                displayLocation();
            }
        };
    }

    private void setUpLocation() {
        if (!mapPermission.validLocationPermission()) {
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        } else {
            displayLocation();
        }
    }

    @SuppressLint("MissingPermission")
    private void displayLocation() {
        if(validUpdateFirebaseLocation) {
            Log.e(TAG, "displayLocation: location");
            if (mapPermission.validLocationPermission()) {
                Log.e(TAG, "displayLocation: valid");
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Log.e(TAG, "displayLocation_onSuccess: " + location);
                        if (location != null) {
                            driverLocation = location;
                            LatLng latLng = new LatLng(driverLocation.getLatitude(), driverLocation.getLongitude());
                            mapConfig.setDriverMarker(latLng);
                            mapPrescience.updateDriverLocation(driverLocation, new FireUpdateLocationInterface() {
                                @Override
                                public void update() {
                                    //update location firebase success
                                }
                            });

                        }
                    }
                });
            }
        }
    }

    @SuppressLint("MissingPermission")
    public void getDriver(String driver_id, FireDriverLocationInterface fireDriverLocationInterface){
        if(!mapPermission.validLocationPermission()){
            Log.e(TAG, "getDrivers: check permission access location" );
           return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                Log.e("driver_location", "locatioh wo");
                mapPrescience.loadDriver(driver_id,fireDriverLocationInterface);
                mapPrescience.getDrivers().addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //remove value when disconnected
                        mapPrescience.loadDriver(driver_id,fireDriverLocationInterface);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled: DataBase Error "+databaseError.getMessage() );
                    }
                });
            }
        });
    }


    @Override
    public void onLocationChanged(Location location) {
        this.driverLocation = location;
        displayLocation();
    }

    @Override
    public void onMyLocationChange(Location location) {
        this.driverLocation = location;
        displayLocation();
    }

    public void stopTracking(){
        if (fusedLocationProviderClient != null && locationCallback != null)
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        if(mapPrescience != null) {
            mapPrescience.setRemoveConnection(true);
            mapPrescience.getCurrentUserRef().onDisconnect().removeValue();
        }
    }
}
