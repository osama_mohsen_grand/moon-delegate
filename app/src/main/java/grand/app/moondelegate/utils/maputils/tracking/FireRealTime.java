package grand.app.moondelegate.utils.maputils.tracking;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;


public class FireRealTime {

    public FireRealTime(){
        updateFireToken();
    }

    public void updateFireToken() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(Constants.TOKENS_TBL);

        FireToken token = new FireToken(FirebaseInstanceId.getInstance().getToken());
        tokens.child(UserHelper.retrieveKey(Constants.TOKEN))
                .setValue(token);

    }

}
