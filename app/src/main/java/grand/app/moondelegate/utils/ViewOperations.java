package grand.app.moondelegate.utils;


import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class ViewOperations {




   public static void setRVHorzontial(Context context, RecyclerView recyclerView) {
       recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
   }


   public static void setRVHVertical(Context context, RecyclerView recyclerView) {
       LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
       recyclerView.setLayoutManager(linearLayoutManager);
   }

   public static LinearLayoutManager setRVHVertical2(Context context, RecyclerView recyclerView) {
       LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true);
       linearLayoutManager.setReverseLayout(true);
       linearLayoutManager.setStackFromEnd(true);
       recyclerView.setLayoutManager(linearLayoutManager);
       return linearLayoutManager;
   }


   public static void setRVHVerticalGrid(Context context, RecyclerView recyclerView, int numOfColumns) {
       recyclerView.setLayoutManager(new GridLayoutManager(context, numOfColumns));
   }


   public static void setRelativeViewSize(View view, int width, int height) {
       RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
       view.setLayoutParams(layoutParams);
   }

   public static void setLinearViewSize(View view, int width, int height) {
       LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
       view.setLayoutParams(layoutParams);
   }


   public static float dpFromPx(final Context context, final float px) {
       return px / context.getResources().getDisplayMetrics().density;
   }

   public static float pxFromDp(final Context context, final float dp) {
       return dp * context.getResources().getDisplayMetrics().density;
   }


   public static int getScreenWidth(Context context) {
       DisplayMetrics metrics = new DisplayMetrics();
       ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
       return metrics.widthPixels;

   }

   public static int getScreenHeight(Context context) {
       DisplayMetrics metrics = new DisplayMetrics();
       ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
       return metrics.heightPixels;
   }


}
