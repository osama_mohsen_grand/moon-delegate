package grand.app.moondelegate.utils.dialog;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.MediaStore;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.upload.FilesUtilsFragment;


public class DialogChoosePhoto {
    public static void choosePhoto(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            if (checkPermission(fragment)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getActivity());
                builder.setTitle(ResourceManager.getString(R.string.select_image));
                builder.setItems(new CharSequence[]{ResourceManager.getString(R.string.gallery),
                                ResourceManager.getString(R.string.camera)},
                        (dialog, which) -> {
                            switch (which) {
                                case 0:
                                    Intent galleryIntent = new Intent();
                                    galleryIntent.setType("image/*");
                                    galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                    fragment.startActivityForResult(galleryIntent, FilesUtilsFragment.PICK_IMAGE_REQUEST);
                                    break;
                                case 1:
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    fragment.startActivityForResult(intent, FilesUtilsFragment.PICK_IMAGE_CAMERA);
                                    break;

                                default:
                                    break;
                            }
                        });

                builder.show();
            } else {
                ActivityCompat.requestPermissions(fragment.getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, FilesUtilsFragment.PERMISSION_LOAD_IMAGE);
            }
        }
    }


    public static boolean checkPermission(Fragment fragment){
        if (fragment != null && fragment.getActivity() != null) {

            if (ActivityCompat.checkSelfPermission(fragment.getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(fragment.getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(fragment.getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }
        return false;
    }
}
