package grand.app.moondelegate.utils.maputils.direction;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.resources.ResourceManager;

import static com.google.android.gms.maps.model.JointType.ROUND;


public class DrawRoute extends AsyncTask<String, Void, String> {
    private static final String TAG = "DrawRoute";
    private Context context;
    private String route_url;
    MapConfig mapConfig;
    public Polyline direction;
    DirectionDistance directionDistance;
    ProgressDialog mDialog;
    public DrawRoute(Context context, String route_url, MapConfig mapConfig, DirectionDistance directionDistance) {
        this.context = context;
        mDialog = new ProgressDialog(context);
        mDialog.setMessage(ResourceManager.getString(R.string.please_wait));
        mDialog.setCancelable(false);
        this.route_url = route_url;
        this.mapConfig = mapConfig;
        this.directionDistance = directionDistance;
        mDialog.show();
    }

    public void draw(String route_url, MapConfig mapConfig, DirectionDistance directionDistance){
        this.route_url = route_url;
        this.mapConfig = mapConfig;
        this.directionDistance = directionDistance;
    }


    @Override
    protected String doInBackground(String... url) {
        String data = "";

        try {
            data = downloadUrl(route_url);
//            data = route_url;
        } catch (Exception e) {
            Log.d("Background Task", e.toString());
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.e(TAG, "DrawRoute_result: " + result);
        mDialog.hide();
        mDialog.dismiss();
        ParserTask parserTask = new ParserTask();
        parserTask.execute(result);

    }


    public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        ProgressDialog mDialog = new ProgressDialog(context);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("starting", "preExecute");
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            Log.e("doInBackground", "doInBackground");
            JSONObject jobject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jobject = new JSONObject(strings[0]);
                JSONArray routess = jobject.getJSONArray("routes");
                JSONObject object = routess.getJSONObject(0);
                JSONArray legs = object.getJSONArray("legs");
                JSONObject legsObjects = legs.getJSONObject(0);

                //get the distance
                JSONObject distance = legsObjects.getJSONObject("distance");
                int distanceValue = distance.getInt("value");
                mapConfig.setDistanceWithKilo(distanceValue);
                directionDistance.receive(distanceValue);

                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jobject);
            } catch (JSONException e) {
                Log.e("error_list", e.getMessage().toString());
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            mDialog.dismiss();
            if (lists != null) {
                Log.e(TAG, "onPostExecute: ");
                ArrayList<LatLng> points = null;
                PolylineOptions polylineOptions = null;
                for (int i = 0; i < lists.size(); i++) {
                    points = new ArrayList();
                    polylineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = lists.get(i);
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }
                    Log.e("all_points", String.valueOf(points.size()));

                    polylineOptions.addAll(points);
                    polylineOptions.width(5);
                    polylineOptions.color(Color.DKGRAY);
                    polylineOptions.geodesic(true);
                    polylineOptions.startCap(new SquareCap());
                    polylineOptions.endCap(new SquareCap());
                    polylineOptions.jointType(ROUND);
                }
                if (polylineOptions != null) {
                    direction = mapConfig.getGoogleMap().addPolyline(polylineOptions);
                    mapConfig.setRoute(direction);
                    mapConfig.moveCamera(points);
                }
            }
        }
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        Log.e(TAG, "downloadUrl: " + data);
        return data;
    }

}