package grand.app.moondelegate.utils.storage.location;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import grand.app.moondelegate.utils.storage.SharedPreferenceHelper;


/**
 * Created by mohamedatef on 1/12/19.
 */

public class LocationHelper {


    public static void saveLocation(LocationPosition locationHelper) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        ArrayList<LocationPosition> locationHelpers = getLocations();
        locationHelpers.add(locationHelper);
        Gson gson = new Gson();
        String json = gson.toJson(locationHelpers);
        prefsEditor.putString("locations",json);
        prefsEditor.commit();
    }


    public static ArrayList<LocationPosition> getLocations(){
        Gson gson = new Gson();
        Type type = new TypeToken<List<LocationPosition>>(){}.getType();
        String response =  SharedPreferenceHelper.getSharedPreferenceInstance().getString("locations", "");
        ArrayList<LocationPosition> lstArrayList = gson.fromJson(response,
                type);
        if(lstArrayList == null)
            lstArrayList = new ArrayList<>();
        return lstArrayList;
    }

    public static void clear(){
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString("locations","");
        prefsEditor.commit();
    }
}
