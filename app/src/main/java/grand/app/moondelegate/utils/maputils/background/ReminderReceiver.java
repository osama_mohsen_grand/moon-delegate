package grand.app.moondelegate.utils.maputils.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReminderReceiver extends BroadcastReceiver {



    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent("grand.app.moondelegate.utils.maputils.background.LocationBackground");
        i.setClass(context, LocationBackground.class);
        context.startService(i);
    }
}
