package grand.app.moondelegate.utils.maputils.tracking.maprealtime;

import android.location.Location;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import io.reactivex.annotations.NonNull;

public class MapPrescience {

    private static final String TAG = "MapPrescience";
    public static String driver_tbl = "Drivers";
    public static String user_driver_tbl = "DriversInformation";
    DatabaseReference onlineRef , currentUserRef;
    DatabaseReference drivers, userDrivers;
    GeoFire geoFire;
    MapConfig mapConfig;
    boolean removeConnection = false;

    public MapPrescience(MapConfig mapConfig) {
        this.mapConfig = mapConfig;
    }

    public void connect(){
        initDriverLocation();
        onlineRef = FirebaseDatabase.getInstance().getReference().child(".info/connected");
        currentUserRef = FirebaseDatabase.getInstance().getReference(driver_tbl)
                .child(String.valueOf(UserHelper.getUserId()));
        onlineRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //remove value when disconnected
                if(removeConnection)
                    currentUserRef.onDisconnect().removeValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: DataBase Error "+databaseError.getMessage() );
            }
        });
    }

    private void initDriverLocation(){
        drivers = FirebaseDatabase.getInstance().getReference(driver_tbl);
        userDrivers = FirebaseDatabase.getInstance().getReference(user_driver_tbl);
        geoFire = new GeoFire(drivers);
    }

    public DatabaseReference getDrivers() {
        return drivers;
    }

    public void updateDriverLocation(Location location, FireUpdateLocationInterface mapPrescienceInterface){
        geoFire.setLocation(String.valueOf(UserHelper.getUserId()),
                new GeoLocation(location.getLatitude(), location.getLongitude()), new GeoFire.CompletionListener() {
                    @Override
                    public void onComplete(String key, DatabaseError error) {
                        mapPrescienceInterface.update();
                    }
                });
    }

    public void setRemoveConnection(boolean removeConnection) {
        this.removeConnection = removeConnection;
    }

    public void loadDriver(String driver_id, FireDriverLocationInterface fireDriverLocationInterface) {
        if (mapConfig.getGoogleMap() != null) {

            mapConfig.getGoogleMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    DatabaseReference driverLocation = FirebaseDatabase.getInstance().getReference(driver_tbl);
                    driverLocation.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            if (snapshot.hasChild(driver_id)) {
                                String driver_lat = snapshot.child(driver_id).child("l").child("0").getValue().toString();
                                String driver_lng = snapshot.child(driver_id).child("l").child("1").getValue().toString();
                                fireDriverLocationInterface.getDriverLocation(new LatLng(Double.parseDouble(driver_lat), Double.parseDouble(driver_lng)));
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.e(TAG, "onCancelled: DataBase Error "+databaseError.getMessage() );
                        }
                    });
                }
            });
        }
    }

    public DatabaseReference getCurrentUserRef() {
        return currentUserRef;
    }

}
