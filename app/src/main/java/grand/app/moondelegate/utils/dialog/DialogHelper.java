package grand.app.moondelegate.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;
import grand.app.moondelegate.R;


public class DialogHelper {
    public Activity context;
    public static Dialog showDialog;

    public DialogHelper(Activity context) {
        this.context = context;
    }


    public View SetViewLayout(int dialog){
        try{
            Rect displayRectangle = new Rect();
            Window window = context.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

            // inflate and adjust layout
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(dialog,null);
            view.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
            return view;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public static void setView(Context context, View view) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.customDialog);
            builder.setView(view);
            showDialog = builder.create();
            showDialog.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    // Best Array Dialog
    public static void showDialogHelper(Context context, int layout , int [] ids, final DialogHelperInterface dialogHelperInterface){
        try {
            DialogHelper dialogHelper = new DialogHelper((Activity) context);
            View view = dialogHelper.SetViewLayout(layout);

            for (int id : ids) {
                final View view_id = view.findViewById(id);
                view_id.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogHelperInterface.OnClickListenerContinue(showDialog, view_id);
                    }
                });
            }
            setView(context, view);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    //Best Of Best
    public static void showDialogHelper(Context context, int layout , int [] ids, final DialogHelperViewInterface dialogHelperInterface){
        try {
            DialogHelper dialogHelper = new DialogHelper((Activity) context);
            final View view_layout = dialogHelper.SetViewLayout(layout);

            for (int id : ids) {
                final View view_id = view_layout.findViewById(id);
                view_id.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogHelperInterface.OnClickListenerContinue(showDialog,view_layout, view_id);
                    }
                });
            }
            setView(context, view_layout);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }




    public static void showDialogHelperTimer(Context context, int layout , int timer){
        try {
            DialogHelper dialogHelper = new DialogHelper((Activity) context);
            View view = dialogHelper.SetViewLayout(layout);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    showDialog.dismiss();
                }
            }, timer);
            setView(context, view);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }




}
