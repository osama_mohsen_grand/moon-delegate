package grand.app.moondelegate.utils.maputils.location;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.maputils.permission.MapPermissionFragment;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import timber.log.Timber;

import static android.content.Context.LOCATION_SERVICE;
public class LocationLatLng implements LocationListener {

    Activity activity;
    Fragment fragment;
    LocationChangeListener locationChangeListner;
    FusedLocationProviderClient fusedLocationProviderClient;
    public MapPermissionFragment mapPermission;
    SettingsClient settingsClient = null;
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;


    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1000; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 100000 * 60 * 1; // 5 minute


    public LocationLatLng(Activity activity, Fragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
        mapPermission = new MapPermissionFragment(fragment);
    }


    LocationRequest mLocationRequest;
    LocationSettingsRequest locationSettingsRequest = null;
    private long UPDATE_INTERVAL = 5000;  /* 10 secs */
    private long FASTEST_INTERVAL = 1000; /* 2 sec */
    protected LocationManager locationManager;

    public void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        settingsClient = LocationServices.getSettingsClient(activity);
    }

    public void getLocationLooper(LocationChangeListener locationChangeListner) {
        this.locationChangeListner = locationChangeListner;
        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());
    }

    LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            // do work here
            UserHelper.saveKey(Constants.LAT, locationResult.getLastLocation().getLatitude() + "");
            UserHelper.saveKey(Constants.LNG, locationResult.getLastLocation().getLongitude() + "");
            String address = getAddress(locationResult.getLastLocation());
            locationChangeListner.select(address, new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude()));
        }
    };

    public void removeLocationUpdate() {
        if (fusedLocationProviderClient != null && locationCallback != null)
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    @SuppressLint("MissingPermission")
    public void getLocation(LocationChangeListener locationChangeListner) {
        this.locationChangeListner = locationChangeListner;
        Timber.e("locationListenerStart");
        if (!mapPermission.validLocationPermission()) {
            mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST);
        } else {
            Timber.e("locationListener enable all permissions");
            startLocationUpdates();
            settingsClient.checkLocationSettings(locationSettingsRequest)
                    .addOnSuccessListener(activity, new OnSuccessListener<LocationSettingsResponse>() {
                        @SuppressLint("MissingPermission")
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                                Timber.e("locationListener fetch location");
                                if (location != null) {
                                    Timber.e("Location Normal");
                                    locationFetch(location);
                                } else {
                                    location = getLocation();
                                    if (location != null)
                                        locationFetch(location);
                                    else{
                                        Timber.e("unable to fetch location");
                                        locationChangeListner.error();
                                    }
                                }

                            });
                        }
                    })
                    .addOnFailureListener(activity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            int statusCode = ((ApiException) e).getStatusCode();
                            Timber.e("failure:" + statusCode);
                        }
                    });
        }


    }

    @SuppressLint("MissingPermission")
    public Location getLocation() {
        Location location = null;
        try {
            locationManager = (LocationManager) activity.getApplicationContext()
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            //isGPSEnabled = locationManager
            //.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                //showSettingsAlert();

            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                else {
                    if (isGPSEnabled) {

                        if (location == null) {
                            locationManager.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            Log.d("GPS Enabled", "GPS Enabled");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }


//    private LocationCallback locationCallback;
//
//    private void getLocations() {
//        locationCallback = new LocationCallback() {
//            @Override
//            public void onLocationResult(LocationResult locationResult) {
//                if (locationResult == null) {
//                    return;
//                }
//                for (Location location : locationResult.getLocations()) {
//                    Timber.e("location:" + location.getLatitude() + "," + location.getLongitude());
//                }
//            }
//
//            ;
//        };
//    }

    public String getAddress(Location location) {
        List<Address> addresses;
        String address = "";
        try {
            Timber.e("start here location gecoder");
            Geocoder geocoder = new Geocoder(activity, new Locale(LanguagesHelper.getCurrentLanguage()));
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
            address = ResourceManager.getString(R.string.failed_to_fetch_your_address);
        }
        return address;
    }

    public String getAddress(double lat , double lng) {
        List<Address> addresses;
        String address = "";
        try {
            Timber.e("start here location gecoder");
            Geocoder geocoder = new Geocoder(activity, new Locale(LanguagesHelper.getCurrentLanguage()));
            addresses = geocoder.getFromLocation(lat,lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
            address = ResourceManager.getString(R.string.failed_to_fetch_your_address);
        }
        return address;
    }

    public void locationFetch(Location location) {
        String address = getAddress(location);
        locationChangeListner.select(address, new LatLng(location.getLatitude(), location.getLongitude()));
    }

    @Override
    public void onLocationChanged(Location location) {
//        getLocations();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


}
