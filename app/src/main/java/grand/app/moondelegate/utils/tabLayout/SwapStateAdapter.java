package grand.app.moondelegate.utils.tabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import grand.app.moondelegate.models.app.TabModel;

public class SwapStateAdapter extends FragmentStatePagerAdapter {
    private ArrayList<TabModel> tabs;

    public SwapStateAdapter(FragmentManager fragmentManager, ArrayList<TabModel> tabs) {
        super(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.tabs = tabs;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        if(position >= 0)
            return tabs.get(position).fragment;
        return null;
    }

    @Override
    public int getCount() {
        return tabs.size();
    }




    @Override
    public CharSequence getPageTitle(int position) {
        if(position >=  0)
            return tabs.get(position).name;
        return null;
    }


    public void refresh() {

    }
}