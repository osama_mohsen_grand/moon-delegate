package grand.app.moondelegate.utils.maputils.location;

import com.google.android.gms.maps.model.LatLng;

public interface LocationChangeListener {
    public void select(String address, LatLng latLng);
    public void error();
}
