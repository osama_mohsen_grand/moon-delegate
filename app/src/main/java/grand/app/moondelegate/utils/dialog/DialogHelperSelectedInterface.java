package grand.app.moondelegate.utils.dialog;

import android.app.Dialog;

/**
 * Created by osama on 1/7/2018.
 */

public interface DialogHelperSelectedInterface {
    void onClickListener(Dialog dialog, String type, Object object);
}
