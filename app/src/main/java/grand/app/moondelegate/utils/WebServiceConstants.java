package grand.app.moondelegate.utils;

/**
 * Created by sameh on 4/2/18.
 */

public class WebServiceConstants {

    public static String EMPLOYEE ="employee";


    public static final String LOGIN = "login";
    public static final String SEND_CODE = "code_send";
    public static final String REGISTER = "registerUser";
    public static final String JOB_APPLIED = "job_applied";
    public static final String JOB_FAVOURITE = "job_favorites";
    public static final String SEARCH_JOBS = "search";
    public static final String JOB_SUGGESTED = "job_suggested";
    public static final String NOTIFICATIONS = "my_notifications";
    public static final String COURSES = "my_courses";
    public static final String ALL_COURSES = "all_courses";
    public static final String SINGLE_COURSE = "course";
    public static final String SHOW = "show";
    public static final String NOTIFY_SWITCH = "notify_switch";
    public static final String HOME = "language";
    public static final String ADD_SUITABLE = "add_suitables";
    public static final String JOB_DETAILS = "job_show";
    public static final String CHANGE_PASSWORD = "change_password";
    public static final String MESSAGES = "get_support_messages";
    public static final String ABOUT_US = "about_us";
    public static final String CONTACT_US="contact_us";
    public static final String UPDATE_EMAIL="user/change_email";
    public static final String UPDATE_PASSWORD="user/change_password";
    public static final String NOTIFY_SETTINGS="notify_setting";
    public static final String ADD_JOB="job_add";
    public static final String UPDATE_JOB="job_update";
    public static final String ADD_COURSE = "course/store";
    public static final String UPDATE_COURSE = "course/update";
    public static final String COUNTRIES="get_countries";
    public static final String CITIES="get_cities";
    public static final String LEVELS="levels";
    public static final String DELETE_COURSE="course/delete";
    public static final String GET_BUNDLES="get_bundles";
    public static final String JOB_APPLICATIONS = "job/applicants";
    public static final String DECLINE_JOB = "job/application/decline";
    public static final String ACCEPT_JOB = "job/application/approve";
    public static final String DELETE_JOB = "job/delete";
    public static final String SPLASH = "splash";
    public static final String PROFILE = "user/profile";
    public static final String EXPERINECE_UPDATE = "user/profile/experience_update";
    public static final String EDUCATION_UPDATE = "user/profile/education_update";
    public static final String LANGUAGE_UPDATE = "user/profile/languages_update";
    public static final String SKILLS_UPDATE = "user/profile/skills_update";
    //languages_update

    public static final String MY_JOBS = "get_my_jobs";
    public static final String BE_SPECIAL = "be_special/store";
    public static final String APPLY_JOB = "job_apply";
    public static final String ADD_FAVOURITE = "job_favorite";
    public static final String ADD_EXPERIENCE = "add_experience";
    public static final String SEND_MESSAGE= "send_message";
    public static final String GET_COUNTRIES= "get_countries";
    public static final String UPDATE_COUNTRIES= "user/update_country";
    public static final String RECOMMENDATIONS= "job/get_recommendation";
    public static final String RECOMMEND= "job/recommend";
    public static final String COUNTRIES_JOBS = "jobs_by_country";
    public static final String JOB_ANSWERS = "job_apply_answers";
    public static final String QUESTIONS = "get_questions";
    public static final String UPLOAD_JOB_FILE = "job_apply_file";

    public static final String BASIC_INFO_UPDATE = "user/profile/basic_update";

    public static final String PERSONAL_INFO_UPDATE = "user/profile/personal_update";

}
