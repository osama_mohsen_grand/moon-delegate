package grand.app.moondelegate.utils.maputils.tracking.token;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.SharedPreferenceHelper;


public class FireRealTime {

    public FireRealTime(){
        updateFireToken();
    }

    public void updateFireToken() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(Constants.TOKENS_TBL);

        FireToken token = new FireToken(FirebaseInstanceId.getInstance().getToken());
        tokens.child(SharedPreferenceHelper.getKey(Constants.TOKEN))
                .setValue(token);

        //TODO un comment this line
//            int user_id = UserHelper.getUserId();
//            TokenAsyncTask tokenAsyncTask = new TokenAsyncTask(context, user_id, token.getToken(), AppHelper.getDeviceIdWithoutPermission(context));
//            tokenAsyncTask.execute();
    }

}
