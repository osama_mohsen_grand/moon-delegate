package grand.app.moondelegate.utils.maputils.permission;

import android.Manifest;
import android.content.pm.PackageManager;

import java.util.Objects;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

public class MapPermissionFragment {

    private static final String TAG = "MapPermissionFragment";
    private Fragment fragment;

    public MapPermissionFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    /*

     */
    public boolean validLocationPermission(){
        try{
            if(fragment != null) {
                if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(fragment.getContext()),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(fragment.getContext(),
                                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return true;
    }


    public void runtimePermissionLocation(int request_permission) {
        try{
            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(fragment.getContext()),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(fragment.getContext(),
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)  {
                fragment.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION},
                        request_permission);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public void makeRequestPermission(int request_permission) {
        try{
            fragment.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    request_permission);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
