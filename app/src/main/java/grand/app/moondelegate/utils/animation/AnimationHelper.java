package grand.app.moondelegate.utils.animation;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import grand.app.moondelegate.R;

public class AnimationHelper {
    Animation slideUpAnimation, slideDownAnimation;
    private Context context;

    public AnimationHelper(Context context) {
        this.context = context;
        slideUpAnimation = AnimationUtils.loadAnimation(context,
                R.anim.slide_up_animation);

        slideDownAnimation = AnimationUtils.loadAnimation(context,
                R.anim.slide_down_animation);
    }

    public void animationDownUp(View show, View hide) {
//        startSlideDownAnimation(hide);//down animation
        hide.setVisibility(View.GONE);//hide
//        startSlideUpAnimation(show);//show animation
        show.setVisibility(View.VISIBLE);//show view user data info
    }

    public void startSlideUpAnimation(View view) {
        view.startAnimation(slideUpAnimation);
    }

    public void startSlideDownAnimation(View view) {
        view.startAnimation(slideDownAnimation);
    }
}
