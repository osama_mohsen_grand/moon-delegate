package grand.app.moondelegate.utils.maputils.tracking.caranimation;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.jakewharton.rxrelay2.PublishRelay;

import java.util.Timer;

import grand.app.moondelegate.utils.maputils.base.MapConfig;

public class CarAnimation {
    private static final String TAG = "CarAnimation";
    private Activity activity;
    public int emission = 0;
    public MapConfig mapConfig;
    public Marker marker;
    private int timeMileSecond;
    private LatLng prevLocation;
    private PublishRelay<LatLng> latLngPublishRelay = PublishRelay.create();

    private float v;
    Timer timer = null;

    public CarAnimation(Activity activity, MapConfig mapConfig) {
        this.activity = activity;
        this.mapConfig = mapConfig;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public boolean mStopHandler = false;
    Handler handler = new Handler();
    Handler mHandler = new Handler();

    public void setDuration(int timeMileSecond) {
        this.timeMileSecond = timeMileSecond;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // do your stuff - don't create a new runnable here!
                if (!mStopHandler) {
//                    String lat = SharedPreferenceHelper.getKey(Constants.DRIVER_LAT);
//                    String lng = SharedPreferenceHelper.getKey(Constants.DRIVER_LNG);
//                    if (!lat.equals("") && !lng.equals(""))
//                        prevLocation = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                    if (mapConfig.getDriverMarker() != null) {
                        setPrevLocation(new LatLng(mapConfig.getDriverMarker().getPosition().latitude, mapConfig.getDriverMarker().getPosition().longitude));
                    }
                    mHandler.postDelayed(this, timeMileSecond);
                }
                setPrevLocation(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
            }
        };

        mHandler.post(runnable);

    }
    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    public void setConfig(Marker marker, int timeSecond) {
        setMarker(marker);
        setDuration(timeSecond);
        animate();
    }


    public void setPrevLocation(LatLng prevLocation) {
        this.prevLocation = prevLocation;
    }

    boolean isStart = false;


    int index, next;
    double lat, lng;

    LatLng latLngPrev = null;
    LatLng latLngNew = null;

    public void animate() {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(500);//500 TODO
        valueAnimator.setInterpolator(new LinearInterpolator());

        mapConfig.setDriverMarker(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
        marker = mapConfig.getDriverMarker();
        handler = new Handler();
        index = -1;
        next = 1;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "run: " + timeMileSecond);
                if (latLngPrev == null || latLngNew == null || (latLngPrev.latitude != prevLocation.latitude && latLngPrev.longitude != prevLocation.longitude &&
                        latLngNew.latitude != marker.getPosition().latitude && latLngNew.longitude != marker.getPosition().longitude)) {

                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                            v = valueAnimator.getAnimatedFraction();
                            lng = v * marker.getPosition().longitude + (1 - v)
                                    * prevLocation.longitude;
                            lat = v * marker.getPosition().latitude + (1 - v)
                                    * prevLocation.latitude;
                            LatLng newPos = new LatLng(lat, lng);
                            marker.setPosition(newPos);
                            marker.setAnchor(0.5f, 0.5f);
//                        float bear = (float) bearingBetweenLocations(startPosition,newPos);

                            float bear = getBearing(prevLocation, newPos);
                            if (!Float.isNaN(bear)) {
                                marker.setRotation(bear);
                            }

                            mapConfig.getGoogleMap().moveCamera(CameraUpdateFactory
                                    .newCameraPosition
                                            (new CameraPosition.Builder()
                                                    .target(newPos)
                                                    .zoom(15.5f)
                                                    .build()));

                            latLngPrev = prevLocation;
                            latLngNew = marker.getPosition();
                        }
                    });
                    valueAnimator.start();
                    handler.postDelayed(this, timeMileSecond);

                }
            }
        }, timeMileSecond);
    }
}
