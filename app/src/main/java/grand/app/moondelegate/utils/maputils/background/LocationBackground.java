package grand.app.moondelegate.utils.maputils.background;


import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.SharedPreferenceHelper;
import grand.app.moondelegate.utils.storage.location.LocationHelper;
import grand.app.moondelegate.utils.storage.location.LocationPosition;
import timber.log.Timber;

@SuppressLint({"ClickableViewAccessibility", "InflateParams", "RtlHardcoded"})
public class LocationBackground extends Service {
    private static final String TAG = "LocationBackground";
    public UpdateLocationBackgroundService updateLocationService = new UpdateLocationBackgroundService();
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;

    public int trip_id;

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };


    Context context;
    View view;
    double lat = 0,lng =0;
    Thread thread  = null;
    @SuppressLint("LongLogTag")
    public void onCreate() {
        super.onCreate();
        initializeLocationManager();
        this.context = this;
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (SecurityException ex) {
            Log.e(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (SecurityException ex) {
            Log.e(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, "gps provider does not exist " + ex.getMessage());
        }


        thread = new Thread(new Runnable() {
            public void run() {
                try {
                    while (true) {

                        if(thread != null) {
                            Thread.sleep(5000);
                            Log.e(TAG, "location_run: start");
                            if (!SharedPreferenceHelper.getKey(Constants.LAT).equals("") && !SharedPreferenceHelper.getKey(Constants.LNG).equals("")) {
                                lat = Double.parseDouble(SharedPreferenceHelper.getKey(Constants.LAT));
                                lng = Double.parseDouble(SharedPreferenceHelper.getKey(Constants.LNG));
                                Log.e(TAG, "location_run2: " + lat + "," + lng);
                                LocationHelper.saveLocation(new LocationPosition(lat, lng));
                                int size = LocationHelper.getLocations().size();
                                if (size >= 60) {
                                    updateLocationService.updateLocations(trip_id, LocationHelper.getLocations());
                                    LocationHelper.clear();
                                }
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();
    }

    public void onDestroy() {
        super.onDestroy();
        if(thread != null) {
            thread.interrupt();
            thread = null;
            stopSelf();
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null && intent.hasExtra(Constants.TRIP_ID)) {
            this.trip_id = intent.getIntExtra(Constants.TRIP_ID,0);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }


    @SuppressLint("LongLogTag")
    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {

            Log.e(TAG, "LocationChangeListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            SharedPreferenceHelper.saveKey(Constants.LAT, String.valueOf(location.getLatitude()));
            SharedPreferenceHelper.saveKey(Constants.LNG, String.valueOf(location.getLongitude()));
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    @SuppressLint("LongLogTag")
    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }
}
