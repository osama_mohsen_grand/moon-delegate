package grand.app.moondelegate.utils.dialog;

import android.app.Dialog;
import android.view.View;

/**
 * Created by osama on 1/7/2018.
 */

public interface DialogHelperInterface {
    void OnClickListenerContinue(Dialog dialog, View view);
}
