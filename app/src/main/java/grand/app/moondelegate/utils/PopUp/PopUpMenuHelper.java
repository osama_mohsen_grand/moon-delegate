package grand.app.moondelegate.utils.PopUp;

import android.app.Activity;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.PopupMenu;

public class PopUpMenuHelper {
    PopupMenu popUpMenu = null;

    public void openPopUp(Activity context, View viewAction, ArrayList<String> objects, PopUpInterface popUpInterface) {
        List<String> list = new ArrayList<>();
        list.clear();
        list.addAll(objects);
        popUpMenu = new PopupMenu(context, viewAction);
        for (int i = 0; i < list.size(); i++) {
            popUpMenu.getMenu().add(i, i, i, list.get(i));
        }
        popUpMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                popUpInterface.submitPopUp(item.getItemId());
                return false;
            }
        });
        popUpMenu.show();
    }
}
