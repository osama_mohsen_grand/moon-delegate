package grand.app.moondelegate.utils;

/**
 * Created by mohamedatef on 1/8/19.
 */

public class Constants {

    public static final String CHANNEL_ID = "di";
    public static final String CHANNEL_NAME = "di Application";
    public static final String CHANNEL_DESCRIPTION = "di Application Desc";

    public static final int REQUEST_CODE = 2000;
    public final static int AUDIO_REQUEST_CODE = 381;

    public static final String INTENT_EXTRA_IMAGES = "images";
    public static final int PAGINATION_SIZE = 20;
    public static final String CATEGORY_ID = "category_id";
    public static final String SEARCH = "search";
    public static final String ALBUM_DETAILS = "album_details";
    public static final String DONE = "done";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String SHOP = "shop";
    public static final String ARRAY = "array";
    public static final String LANG = "lang";
    public static final String COLOR = "color";
    public static final String SIZE = "size";
    public static final String LOCATION_ENABLE = "location_enable";
    public static final String SPLASH = "splash";
    public static final String REGISTRATION_CAR = "register_car";
    public final static String HIDE_NEW_ORDER = "hide_new_order";

    public static final String NEWEST = "newest";
    public static final String OFFER = "offer";
    public static final String VOUCHER = "voucher";
    public static final String DISCOVER = "discover" ;
    public static final String SEARCH_ALBUM = "search_album";
    public static final String SEARCH_FAMOUS = "search_famous";
    public static final String PHONE_VERIFICATION = "phone_verification";
    public static final String SIZE_COLOR = "size_color";
    public static final String NUMBER = "number";
    public static final String USER_ADDRESS = "address";
    public static final String FOLLOWING = "following";
    public static final String MOON_MAP = "moon_map";
    public static final String LOCATION_UPDATED = "location_updated";
    public static final String SERVICE = "service";
    public static final String SELECTED = "selected";

    public static final String DEFAULT_TAXI = "7";
    public static final String DEFAULT_TRANSPORTATION = "12";
    public static final String DEFAULT_USER = "13";
    public static final String DEFAULT_DELEGATE = "14";

    public static final String DATA_LIST = "data_list";
    public static final String INTENT_EXTRA_LIMIT = "limit";
    public static final String CREDIT = "credit";
    public static final String SUPPORT = "support";
    public static final String SHOP_LOCATION = "shop_location";
    public static final String USER_LOCATION = "user_location";
    public static final String CONTACT_US = "contact_us";
    public static final String TECHNICAL_SUPPORT = "technical_support";
    public static final String CURRENCY = "currency";



    //pass data parameters
    final String PREF_USER_DETAILS = "user_details";//shared preference container
    final String PREF_USER_ID = "user_id";//shared preference key
    final String ARGS_USER_ITEM = "user_item"; //bundle key
    public final static String STORY = "stories";
    public final static String GALLERY = "gallery";


    public final static String TRIP = "trip";
    public final static String PAGE = "page";
    public final static String ID = "id";
    public final static String NAME = "name";
    public final static String TAB = "tab";
    public final static String NAME_BAR = "name";
    public final static String LOGIN = "login";
    public final static String INTRO = "intro";
    public final static String MESSAGE = "message";
    public static final String ID_IMAGE = "ID_image";
    public static final String LICENEC = "license_image";


    public final static String FORGET_PASSWORD = "forget_password";
    public final static String CHANGE_PASSWORD = "change_password";
    public final static String NEW_PASSWORD = "new_password";
    public final static String NEW_PASSWORD_PROFILE = "new_password_profile";
    public final static String VIDEO = "video";

    public final static String REGISTRATION = "registration";
    public final static String TERMS = "terms";
    public final static String URL = "url";
    public final static String WRITE_CODE = "write_code";
    public final static String ZOOM = "zoom";
    public final static String SHARE = "share";
    public final static String PROFILE = "profile";
    public final static String RESERVATION = "reservation";

    public final static String HISTORY = "history";
    public final static String GO_HOME = "go_home";
    public final static String HOME = "language";
    public final static String CANCELED = "canceled";
    public final static String SELECT_IMAGE = "select_image";
    public final static String SELECT_IMAGE_MULTIPLE = "select_image_multiple";

    public final static String RELOAD = "reload";
    public final static String PACKAGES = "packages";
    public final static String PRODUCTS = "products";
    public final static String PRODUCT = "product";

    public final static String LANGUAGE_OK = "LANGUAGE_OK";
    public final static String LANGUAGE_CANCEL = "LANGUAGE_CANCEL";
    public final static String CREATED_AT = "created_at";
    public final static String NEW_ORDER = "new_order";
    public final static String NEW_ORDER_TRACKING = "new_order_tracking";




    public final static String SHOP_DETAILS = "shop_details";


    public final static String BE_SHOP = "be_shop";
    public final static String BE_DELEGATE = "be_delegate";
    public final static String BE_DRIVER = "be_driver";
    public final static String TOTAL = "total";
    public final static String SUB_TOTAL = "sub_total";
    public final static String DELIVERY = "delivery";


    public final static String ORDER = "order";
    public final static String ORDER_ID = "ORDER_ID";
    public final static String ORDER_DETAILS = "order_details";
    public final static String ORDER_SUBMIT = "order_submit";



    public final static String TRIP_ACTION = "trip_action";
    public static final String FAMOUS = "famous";
    public static final String FAMOUS_MAIN_DETAILS = "famous_main_details";
    public static final String FAMOUS_DETAILS = "famous_details";

    public static final String FAMOUS_ADVERTISE = "famous_advertise";
    public final static String FAMOUS_FOLLOW = "famous_follow";
    public static final String ADD_ALBUM = "add_album";
    public static final String ADD_ADS = "add_adds";
    public static final String EDIT_ALBUM = "edit_album";

    public final static String PRODUCT_DETAILS = "product_details";
    public static final String ADD_PRODUCT_CONSUMER_SECTORAL = "add_product_consumer_sectoral";


    public final static String CHAT = "CHAT";
    public final static String ALLOW_CHAT = "allow_chat";
    public final static String CHAT_SEND = "chat_send";
    public final static String CHAT_DETAILS = "chat_details";


    public final static String USER = "user";
    public final static String ORDERS = "orders";
    public final static String VERIFY_ID = "verify_id";
    public final static String REVIEW = "review";
    public final static String ADD_REVIEW = "add_review";
    public final static String IMAGES_KEY = "images_key";

    public final static String ADD = "add";
    public final static String MINUS = "minus";
    public final static String ADS = "ads";
    public final static String FILTER = "filterFamousShopAds";

    /*
    if type = 1 link terms and  if type = 2 link about us , 3 = support , 4 = privacy
     */
    public final static int TYPE_TERMS = 1;
    public final static int TYPE_ABOUT_US = 2;
    public final static int TYPE_SUPPORT = 3;
    public final static int TYPE_PRIVACY = 4;
    public static final String TYPE_TAXI = "7";





    public final static String FOLLOW = "follow";
    public final static String NOTIFICATION = "notification";
    public final static String TAXI = "taxi";
    public final static String TRUCKS = "trucks";

    public final static String NOTIFICATION_RECEIVER = "notification_receiver";

    //on activity result codes
    public static final int RESULT_PROFILE_RESPONSE = 1888;
    public static final int ADDRESS = 1889;
    public static final int ADDRESS_RESULT = 1890;
    public static final int VIDEO_REQUEST = 1891;
    public static final int RELOAD_RESULT = 1892;
    public static final int TAGS_RESULT = 1893 ;
    public static final int FILTER_RESULT = 1894 ;

    public static final int DISCOVER_RESULT = 1895 ;

    public final static String TYPE = "type";
    public final static String HAS_DELEGATE = "1";
    public final static String TYPE_CONSUMER = "2";
    public final static String TYPE_CONSUMER_SECTORAL = "1,2";
    public final static String EMAIL = "email";
    public final static String PHONE = "phone";
    public final static String COMMERCIAL = "commercial";
    public final static String LICENCE = "licence";

    public final static String PLACES = "places";
    public final static String LOADER = "loader";


    public final static String IMAGE = "image";
    public final static String USER_IMAGE = "user_image";
    public final static String DRIVER_IMAGE = "driver_image";

    public final static String UPDATE_PROFILE = "updateProfile";

    public final static String DATE_DIALOG = "dateDialog";
    public final static String TIME_DIALOG = "timeDialog";

    public final static String LOGOUT = "logout";

    public final static String SUCCESS = "success";
    public final static String ERROR = "error";
    public final static String SHOW_PROGRESS = "showProgress";
    public final static String HIDE_PROGRESS = "hideProgress";
    public final static String SERVER_ERROR = "serverError";
    public final static String ERROR_RESPONSE = "error_response";
    public final static String ERROR_LOGIN_RESPONSE = "error_login_response";
    public final static String FAILURE_CONNECTION = "failure_connection";

    public final static String SOCIAL_NOT_EXIST_RESPONSE = "social_not_exist_response";

    //app
    public final static String DRIVER_ID = "driver_id";
    public final static String USER_ID = "user_id";
    public final static String TRIP_ID = "trip_id";
    public final static String RECEIVER_ID = "receiver_id";


    public final static String TITLE = "title";
    public final static String EDIT = "edit";
    public final static String DELETE = "delete";
    public final static String SERVICE_SUCCESS = "service_success";
    public final static String DELETED = "deleted";


    public static final String BODY = "body";


    public final static String CART = "cart";
    public final static String SHIPPING = "shipping";
    public final static String SCROLL_TOP = "scroll_top";
    public final static String DELEGATE_LOCATION = "delegate_location";
    public final static String TRIP_CANCEL = "trip_cancel";
    public final static String ABOUT = "about";
    public final static String DESCRIPTION_NEXT = "description_next";
    public final static String GAINER = "Gainer";
    public final static String HARD_GAINER = "Hard Gainer";
    public final static String SKINNY = "Skinny";
    public final static String BACK = "Back";
    public final static String SETTINGS = "settings";



    public final static String PRIVACY_POLICY = "privacy_policy";
    public final static String CALL = "call";
    public final static String PHOTOGRAPHER = "photographer";

    public final static String STATUS = "status";


    public final static String TRIP_DETAILS = "trip_details";
    public final static String UPDATE_CAR = "update_car";
    public final static String LOCATIONS = "locations";



    public final static String PAYMENT = "payment";

    public final static String LANGUAGE_EN = "en";
    public final static String LANGUAGE_AR = "ar";
    public final static String LANGUAGE_FR = "fr";
    public final static String LANGUAGE_GR = "de";

    public final static String LANGUAGE_IT = "it";
    public final static String LANGUAGE_HY = "hy";
    public final static String LANGUAGE_CS = "cs";
    public final static String LANGUAGE_HI = "hi";
    public final static String LANGUAGE_KO = "ko";
    public final static String LANGUAGE_PL = "pl";
    public final static String LANGUAGE_TR = "tr";
    public final static String LANGUAGE_RU = "ru";
    public final static String LANGUAGE_IND = "ind";
    public final static String LANGUAGE_BN = "bn";
    public final static String LANGUAGE_TH = "th";
    public final static String LANGUAGE_FIL = "fil";

    //keys
    public final static String LANGUAGE = "language";
    public final static String TOKEN = "token";
    public final static String CHECKOUT = "checkout";
    public final static String FILE_NOT_VALID = "fileNotValid";

    //default
    public static final String LANGUAGE_DATA = "languageData";
    public static final String LANGUAGE_HAVE = "haveLanguage";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String LOCATION = "location";


    //pages
    public static final String VERIFICATION = "verification";



    public final static String SUBMIT = "submitSearch";
    public final static String PICKUP_SUBMIT = "pickup_submit";
    public final static String DEST_SUBMIT = "dest_submit";
    public final static String CAR_TYPE = "car_type";
    public final static String SCHEDULE = "schedule";
    public final static String RATE = "rate";
    public final static String LAT = "lat";
    public final static String LNG = "lng";

    public final static String DRIVER_LAT = "driver_lat";
    public final static String DRIVER_LNG = "driver_lng";



    public final static String image_dummy = "https://media.gettyimages.com/photos/arch-bridge-in-kromlau-picture-id539121576?s=612x612";


    //CONSTANTS
    public static final String BUNDLE = "bundle";
    //CONSTANTS
    public static final String BUNDLE_NOTIFICATION = "bundle_notification";


    public final static String COUNTRIES = "countries";
    public final static String COUNTRY_CODE = "country_code";
    public final static String COUNTRY_ID = "country_id";
    public final static int DEFAULT_COUNTRY = 20;
    public final static int DEFAULT_COUNTRY_ID = 2;




    //RESPONSES
    public static final int RESPONSE_SUCCESS = 200;
    public final static int RESPONSE_ERROR = 203;
    public static final int RESPONSE_401 = 401;
    public static final int RESPONSE_FAILED = 402;
    public static final int RESPONSE_JWT_EXPIRE = 403;
    public static final int RESPONSE_405 = 405;
    public static final int LOCATION_PERMISSION = 6000;
    public final static int FILE_TYPE_VIDEO = 377;
    public final static int FILE_TYPE_IMAGE = 378;
    public final static int VIDEO_REQUEST_CODE = 380;
    public final static int MEDIA_REQUEST_CODE = 381;


    //permissions codes
    public static final int LOCATION_REQUEST = 7000;
    public static final int GPS_REQUEST = 7001;




    public static final String TYPE_CONSUMER_MARKET = "1";
    public static final String TYPE_MARKET_SERVICE = "2";
    public static final String TYPE_INDUSTRIES = "3";
    public static final String TYPE_FAMOUS_PEOPLE = "4";
    public static final String TYPE_PHOTOGRAPHER = "5";

    public static final String TYPE_INSTITUTIONS = "1";
    public static final String TYPE_COMMERCIALS = "1";
    public static final String TYPE_RESERVATION = "1";
    public static final String TYPE_ADVERTISING = "1";
    public static final String TYPE_COMPANIES = "1";


    // fire tracking
    public static String DRIVERS_TBL = "Drivers";
    public static String USER_DRIVER_TBL = "DriversInformation";
    public static String TOKENS_TBL = "Tokens";
    public static String FIRE_BASE_URL = "https://maps.googleapis.com";



}
