package grand.app.moondelegate.utils.maputils.direction;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.resources.ResourceManager;

import static com.google.android.gms.maps.model.JointType.ROUND;

public class DirectionHelper {
    private static final String TAG = "DirectionHelper";
    private Context context;
    private Polyline polyline;
    MapConfig mapConfig;
    public boolean isMarkerRotating = false;

    public DirectionHelper(Context context, MapConfig mapConfig) {
        this.context = context;
        this.mapConfig = mapConfig;
    }

    public String json_route = "";

    private int getRealSize(List<LatLng> markers) {
        List<LatLng> markers_service = new ArrayList<>();
        Log.e(TAG, "getRealSize: " + markers_service.size());
        int size = 0;
        for (LatLng latLng : markers) {
            if (latLng != null) {
                Log.e(TAG, "getRealSize: start");
                size++;
                markers_service.add(latLng);
            }
        }
//        Log.e(TAG, "getRealSize: "+markers_service.size() );
//        mapConfig.markers_service = new ArrayList<>(markers_service);
//        Log.e(TAG, "getRealSize: "+mapConfig.markers_service.size() );
        return size;
    }


    public String getRouteUrl(List<LatLng> markers) {
        Log.e(TAG, "getRouteUrl: start");
        int size = getRealSize(markers);
        Log.e(TAG, "getRouteUrl: " + size);
        String url = "";
        if (size >= 2) {
            Log.e(TAG, "getRouteUrl: >=2");
            String destination = "destination=";
            if (size == 2) {
                destination += markers.get(1).latitude + "," + markers.get(1).longitude;
            } else
                destination += markers.get(size - 1).latitude + "," + markers.get(size - 1).longitude;

            Log.e(TAG, "getRouteUrl: " + destination);

            String origin = "origin=" + markers.get(0).latitude + "," + markers.get(0).longitude;
            String waypoints = "";
            if (size > 2) {
                boolean check = false;
                waypoints = "&waypoints=optimize:true";
                for (int i = 1; i < size - 1; i++) {
                    if (markers.get(i) != null) {
                        waypoints += "|" + markers.get(i).latitude + "," + markers.get(i).longitude;
                        check = true;
                    }
                }
                waypoints += "|";
                if (!check) waypoints = "";
            }
            String key = "key=" + ResourceManager.getString(R.string.google_direction_api);
            String sensor = "sensor=false";
            String mode = "mode=driving";
            String parameters = origin + waypoints + "&" + destination + "&" + sensor + "&" + mode + "&" + key;
            ;
            String output = "json";
            url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        }
        return url;
    }

    public String getRouteUrl(LatLng start, LatLng end) {
        String url = "";
        Log.e(TAG, "getRouteUrl: >=2");
        String destination = "destination=";
        destination += end.latitude + "," + end.longitude;

        Log.e(TAG, "getRouteUrl: " + destination);

        String origin = "origin=" + start.latitude + "," + start.longitude;

        String key = "key=" + ResourceManager.getString(R.string.google_direction_api);
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String parameters = origin + "&" + destination + "&" + sensor + "&" + mode + "&" + key;
        ;
        String output = "json";
        url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }


    public String loadJSONFromAsset() {
        InputStream is = context.getResources().openRawResource(R.raw.direction);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        json_route = writer.toString();
        return json_route;
    }

    ArrayList<LatLng> points = null;


    int index = -1;
    int next = 1;
    Handler handler;
    Runnable pol = null;
    private LatLng startPosition;
    private LatLng endPosition;
    String htmlins = null;
    List<String> ins;
    private float v;
    double lat = 0, lng = 0;

    public void drawRoute() {
        JSONObject jobject = null;
        List<List<HashMap<String, String>>> routes = null;
        try {
            jobject = new JSONObject(json_route);
            DirectionsJSONParser parser = new DirectionsJSONParser();
            routes = parser.parse(jobject);
        } catch (JSONException e) {
            Log.e("error_list", e.getMessage().toString());
            e.printStackTrace();
        }

        PolylineOptions polylineOptions = null;
        LatLngBounds.Builder builder = null;
        for (int i = 0; i < routes.size(); i++) {
            points = new ArrayList();
            polylineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = routes.get(i);
            builder = new LatLngBounds.Builder();
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);
                builder.include(position);
            }
            Log.e("all_points", String.valueOf(points.size()));

            polylineOptions.addAll(points);
            polylineOptions.width(5);
            polylineOptions.color(Color.DKGRAY);
            polylineOptions.geodesic(true);
            polylineOptions.startCap(new SquareCap());
            polylineOptions.endCap(new SquareCap());
            polylineOptions.jointType(ROUND);
        }
        if (points.size() > 0) mapConfig.setLatLngPointsDirections(points);
        if (polylineOptions != null) {

            polyline = mapConfig.getGoogleMap().addPolyline(polylineOptions);
            int padding = 50;
            /**create the bounds from latlngBuilder to set into map camera*/
            LatLngBounds bounds = builder.build();
            /**create the camera with bounds and padding to set into map*/
            final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            /**call the map call back to know map is loaded or not*/
            mapConfig.getGoogleMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    /**set animated zoom camera into map*/
                    mapConfig.getGoogleMap().animateCamera(cu);
                }
            });
        }
//        startCarAnimation();
    }

    private void startCarAnimation() {
        //start here
        Marker marker = mapConfig.addMarker(points.get(0), R.drawable.ic_pin_car3, "Your LocationPosition");


        ValueAnimator polylineAnimator = ValueAnimator.ofInt(0, 100);
        polylineAnimator.setDuration(2000);
        polylineAnimator.setInterpolator(new LinearInterpolator());
        polylineAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int percentValue = (int) valueAnimator.getAnimatedValue();
                int size = points.size();
                int newPoints = (int) (size * (percentValue / 100.0f));
                List<LatLng> p = points.subList(0, newPoints);
                polyline.setPoints(p);
            }
        });
        polylineAnimator.start();
        handler = new Handler();
        index = -1;
        next = 1;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (index < points.size() - 1) {
                    index++;
                    next = index + 1;
                }
                if (index < points.size() - 1) {
                    startPosition = points.get(index);
                    endPosition = points.get(next);
                }
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(3000);//500 TODO
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        v = valueAnimator.getAnimatedFraction();
                        lng = v * endPosition.longitude + (1 - v)
                                * startPosition.longitude;
                        lat = v * endPosition.latitude + (1 - v)
                                * startPosition.latitude;
                        LatLng newPos = new LatLng(lat, lng);
                        marker.setPosition(newPos);
                        marker.setAnchor(0.5f, 0.5f);
//                        float bear = (float) bearingBetweenLocations(startPosition,newPos);

                        float bear = getBearing(startPosition, newPos);
                        Log.e(TAG, "newPos.lat: " + newPos.latitude);
                        Log.e(TAG, "newPos.lng: " + newPos.longitude);
                        Log.e(TAG, "onAnimationUpdate: " + bear);
                        if (!Float.isNaN(bear)) {
                            marker.setRotation(bear);
                        }

                        mapConfig.getGoogleMap().moveCamera(CameraUpdateFactory
                                .newCameraPosition
                                        (new CameraPosition.Builder()
                                                .target(newPos)
                                                .zoom(15.5f)
                                                .build()));
                    }
                });
                valueAnimator.start();
                handler.postDelayed(this, 3000);
            }
        }, 3000);
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;


    }

//    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {
//
//        double PI = 3.14159;
//        double lat1 = latLng1.latitude * PI / 180;
//        double long1 = latLng1.longitude * PI / 180;
//        double lat2 = latLng2.latitude * PI / 180;
//        double long2 = latLng2.longitude * PI / 180;
//
//        double dLon = (long2 - long1);
//
//        double y = Math.sin(dLon) * Math.cos(lat2);
//        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
//                * Math.cos(lat2) * Math.cos(dLon);
//
//        double brng = Math.atan2(y, x);
//
//        brng = Math.toDegrees(brng);
//        brng = (brng + 360) % 360;
//
//        return brng;
//    }
//
//
//    private void stopRepeatingTask() {
//        if (pol != null) {
//            handler.removeCallbacks(pol);
//        }
//    }
//
//    public ArrayList getPoints() {
//        return points;
//    }
//
//
//    private static final LatLng LOWER_MANHATTAN = new LatLng(40.722543, -73.998585);
//    private static final LatLng BROOKLYN_BRIDGE = new LatLng(40.7057, -73.9964);
//    private static final LatLng WALL_STREET = new LatLng(40.7064, -74.0094);
//
//    private int getRealSize(List<LatLng> markers){
//        List<LatLng> markers_service = new ArrayList<>();
//        int size = 0;
//        for(LatLng latLng : markers) {
//            if(latLng != null){
//                size++;
//                markers_service.add(latLng);
//            }
//        }
//        mapConfig.markers_service = markers_service;
//        return size;
//    }

//    public String getMapsApiDirectionsUrl(List<LatLng> markers) {
//        int size = getRealSize(markers);
//        String url = "";
//        if (size >= 2) {
//            String destination = "destination=";
//            if (size == 2) {
//                destination += markers.get(1).latitude + "," + markers.get(1).longitude;
//            }else
//                destination += markers.get(size - 1).latitude + "," + markers.get(size - 1).longitude;
//
//
//            String origin = "origin=" + markers.get(0).latitude + "," + markers.get(0).longitude;
//            String waypoints = "";
//            if (size > 2) {
//                boolean check = false;
//                waypoints = "&waypoints=optimize:true";
//                for (int i = 1; i < size - 1; i++) {
//                    if (markers.get(i) != null) {
//                        waypoints += "|" + markers.get(i).latitude + "," + markers.get(i).longitude;
//                        check = true;
//                    }
//                }
//                waypoints += "|";
//                if (!check) waypoints = "";
//            }
//            String key = "key=" + ResourceManager.getString(R.string.google_direction_api);
//            String sensor = "sensor=false";
//            String mode = "mode=driving";
//            String parameters = origin + waypoints + "&" + destination + "&" + sensor + "&" + mode + "&" + key;
//            ;
//            String output = "json";
//            url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
//        }
//        return url;
//    }


//    private List<LatLng> decodePoly(String encoded) {
//
//        List<LatLng> poly = new ArrayList<LatLng>();
//        int index = 0, len = encoded.length();
//        int lat = 0, lng = 0;
//
//        while (index < len) {
//            int b, shift = 0, result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lat += dlat;
//
//            shift = 0;
//            result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lng += dlng;
//
//            LatLng p = new LatLng((((double) lat / 1E5)),
//                    (((double) lng / 1E5)));
//            poly.add(p);
//        }
//
//        return poly;
//    }
}
