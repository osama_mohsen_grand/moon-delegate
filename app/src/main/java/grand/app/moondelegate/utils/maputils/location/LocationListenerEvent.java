package grand.app.moondelegate.utils.maputils.location;

import com.google.android.gms.maps.model.LatLng;

public interface LocationListenerEvent {
    public void update(LatLng latLng, Object object);
}
