package grand.app.moondelegate.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import grand.app.moondelegate.R;


/**
 * Created by mohamedatef on 1/12/19.
 */

public class MessagesManager {

    public static void fastDialog(final Activity activity, String title, String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int id) {
            }
        });
        builder.create().show();
    }


    public static void lToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void sToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void dToast(Context context, String text, int duration) {
        Toast.makeText(context, text, duration).show();
    }

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void makeCall(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
        context.startActivity(intent);
    }

    public static void startWebPage(Context context, String page) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(page)));
    }

    public static String getValueFromBundleFragment(Fragment fragment, String key) {
        String value = "";
        Bundle bundle = fragment.getArguments();
        if (bundle != null) {
            value = bundle.getString(key);
        }
        return value;
    }

    public static void shareUrl(String url, Context context){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("tools/plain");
        String shareBody = url;
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public static void setBottomeHeaderColor(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = ((Activity) context).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setNavigationBarColor(context.getResources().getColor(R.color.colorPrimaryDark));
            window.setStatusBarColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }
    }





}
