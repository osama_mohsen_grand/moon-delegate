package grand.app.moondelegate.retrofitutils;


import java.io.IOException;

import grand.app.moondelegate.utils.AppUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

public class ConnectivityInterceptor  implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!AppUtils.isNetworkAvailable()) {
            Timber.e("error no network");
            throw new NoConnectivityException();
        }else
            Timber.e("error have network");
        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }

}