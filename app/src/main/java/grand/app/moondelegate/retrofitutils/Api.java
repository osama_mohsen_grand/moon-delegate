package grand.app.moondelegate.retrofitutils;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface Api<T> {

    @Multipart
    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint", encoded = true) String url, @QueryMap Map<String, String> map, @Part MultipartBody.Part file);

    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint", encoded = true) String url, @QueryMap Map<String, String> map, @Body RequestBody file);

    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint", encoded = true) String url, @QueryMap Map<String, String> map);

}


