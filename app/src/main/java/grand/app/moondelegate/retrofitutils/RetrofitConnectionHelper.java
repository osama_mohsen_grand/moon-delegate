package grand.app.moondelegate.retrofitutils;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.URLS;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConnectionHelper {
    public static Retrofit retrofit = null;
    public static int bufferSize = 256 * 1024;

    public static Api webService() {
//        if (retrofit == null) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                //.addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .socketFactory(new RestrictedSocketFactory(bufferSize));
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClientBuilder.addInterceptor(new ConnectivityInterceptor());
        okHttpClientBuilder.addInterceptor(logging);
        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder newRequest = request.newBuilder().header("Authorization", "secret-key");
                if (UserHelper.getUserId() != -1) {
                    newRequest.addHeader("jwt", UserHelper.getUserDetails().jwtToken);
                }
                newRequest.addHeader("lang", LanguagesHelper.getCurrentLanguage());
                return chain.proceed(newRequest.build());
            }
        });
        retrofit = new Retrofit.Builder()
                .baseUrl(URLS.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build();
//        }
        return retrofit.create(Api.class);
    }
}
