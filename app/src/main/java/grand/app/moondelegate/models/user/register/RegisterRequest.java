package grand.app.moondelegate.models.user.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.Validate;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import timber.log.Timber;

public class RegisterRequest {
    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";

    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("password")
    @Expose
    private String password = "";

    private String country = "";

    @SerializedName("address")
    @Expose
    private String address = "";


    @SerializedName("country_id")
    @Expose
    private String country_id = UserHelper.retrieveKey(Constants.COUNTRY_ID);

    private String city = "";

    @SerializedName("city_id")
    @Expose
    private String city_id = "";


    private String region = "";

    @SerializedName("region_id")
    @Expose
    private String region_id = "";

    @SerializedName("ID_number")
    @Expose
    private String idNumber = "";

    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("lng")
    @Expose
    private double lng = 0;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("car_model")
    @Expose
    private String car_model = "";

    @SerializedName("car_version")
    @Expose
    private String car_version = "";

    @SerializedName("car_color")
    @Expose
    private String car_color = "";

    @SerializedName("car_number")
    @Expose
    private String car_number = "";

    @SerializedName("car_id")
    @Expose
    private String car_id = "";

    private String typeText = "";


    private transient String serviceText = "";
    private transient String truck = "";

    private transient String idImage = "";
    private transient String drivingLicence = "";
    private transient String carImage = "";

    public transient ObservableField nameError;
    public  transient ObservableField phoneError;
    public transient ObservableField emailError;
    public transient ObservableField passwordError;
    public  transient ObservableField countryError;
    public transient ObservableField cityError;
    public transient ObservableField regionError;
    public transient ObservableField addressError;
    public transient ObservableField idImageError;
    public transient ObservableField drivingLicenceError;
    public transient ObservableField carImageError;
    public transient ObservableField typeTextError;
    public transient ObservableField idNumberError;
    public transient ObservableField truckError;


    public transient ObservableField carModelError;
    public transient ObservableField carVersionError;
    public transient ObservableField carColorError;
    public transient ObservableField carNumberError;

    public RegisterRequest() {
        country_id = UserHelper.retrieveKey(Constants.COUNTRY_ID);
        nameError = new ObservableField();
        phoneError = new ObservableField();
        emailError = new ObservableField();
        passwordError = new ObservableField();
        addressError = new ObservableField();
        countryError = new ObservableField();
        cityError = new ObservableField();
        regionError = new ObservableField();
        typeTextError = new ObservableField();
        idImageError = new ObservableField();
        drivingLicenceError = new ObservableField();
        carImageError  = new ObservableField();
        idNumberError = new ObservableField();
        truckError = new ObservableField();
        carModelError = new ObservableField();
        carVersionError = new ObservableField();
        carColorError = new ObservableField();
        carNumberError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;
        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(phone, Constants.PHONE)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("PHONE:error");
        }
        if(!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
            Timber.e("EMAIL:error");
        }
        if(!Validate.isValid(password)) {
            passwordError.set(Validate.error);
            valid = false;
            Timber.e("password:error");
        }
        if(!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        if(!Validate.isValid(city)) {
            cityError.set(Validate.error);
            valid = false;
            Timber.e("city:error");
        }
        if(!Validate.isValid(region) ) {
            regionError.set(Validate.error);
            valid = false;
            Timber.e("region:error");
        }if(!Validate.isValid(idImage)){
            idImageError.set(Validate.error);
            Timber.e("idImage:error");
            valid = false;
        }if(!Validate.isValid(drivingLicence)){
            drivingLicenceError.set(Validate.error);
            Timber.e("drivingLicence:error");
            valid = false;
        }if(!Validate.isValid(typeText)){
            typeTextError.set(Validate.error);
            Timber.e("typeText:error");
            valid = false;
        }if(!Validate.isValid(idNumber)){
            idNumberError.set(Validate.error);
            Timber.e("idNumber:error");
            valid = false;
        }
        if(!type.equals(Constants.DEFAULT_DELEGATE)){

            if(!Validate.isValid(car_model)) {
                carModelError.set(Validate.error);
                valid = false;
                Timber.e("password:error");
            }

            if(!Validate.isValid(car_version)) {
                carVersionError.set(Validate.error);
                valid = false;
                Timber.e("password:error");
            }

            if(!Validate.isValid(car_color)) {
                carColorError.set(Validate.error);
                valid = false;
                Timber.e("password:error");
            }

            if(!Validate.isValid(car_number)) {
                carNumberError.set(Validate.error);
                valid = false;
                Timber.e("password:error");
            }
            if(!Validate.isValid(carImage)) {
                carImageError.set(Validate.error);
                valid = false;
                Timber.e("carImage:error");
            }

            if(type != null && ( type.equals(Constants.DEFAULT_TRANSPORTATION) || type.equals(Constants.DEFAULT_TAXI)) && !Validate.isValid(truck)){
                valid = false;
                Timber.e("truck:error");
                truckError.set(Validate.error);
            }
        }else{
            carModelError.set(null);
            carVersionError.set(null);
            carColorError.set(null);
            carNumberError.set(null);
            carImageError.set(null);
            truckError.set(null);
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }
    public String getServiceText() {
        return serviceText;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        passwordError.set(null);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
        countryError.set(null);
    }

    public String getCountry_id() {
        return country_id;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        cityError.set(null);
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
        cityError.set(null);
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
        regionError.set(null);
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
        regionError.set(null);
    }

    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
        idImageError.set(null);
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
        idNumberError.set(null);
    }

    public String getDrivingLicence() {
        return drivingLicence;
    }

    public void setDrivingLicence(String drivingLicence) {
        this.drivingLicence = drivingLicence;
        drivingLicenceError.set(null);
    }

    public String getTypeText() {
        return typeText;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
        typeTextError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }


    public String getType() {
        return type;
    }

    public String getCar_model() {
        return car_model;
    }

    public void setCar_model(String car_model) {
        this.car_model = car_model;
        carModelError.set(null);
    }

    public String getCar_version() {
        return car_version;
    }

    public void setCar_version(String car_version) {
        this.car_version = car_version;
        carVersionError.set(null);
    }

    public String getCar_color() {
        return car_color;
    }

    public void setCar_color(String car_color) {
        this.car_color = car_color;
        carColorError.set(null);
    }

    public String getCar_number() {
        return car_number;
    }

    public void setCar_number(String car_number) {
        this.car_number = car_number;
        carNumberError.set(null);
    }

    public String getCarImage() {
        return carImage;
    }

    public void setCarImage(String carImage) {
        this.carImage = carImage;
        carImageError.set(null);
    }

    private RegisterRequest(String name, String phone, String email, String password) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.password = password;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
        truckError.set(null);
    }

    public void setCarId(String car_id) {
        this.car_id = car_id;
    }
}
