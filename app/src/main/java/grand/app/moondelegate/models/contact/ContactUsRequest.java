package grand.app.moondelegate.models.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.Validate;
import timber.log.Timber;

public class ContactUsRequest {
    @SerializedName("name")
    @Expose
    private String name = "";
    
    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("message")
    @Expose
    private String message = "";
    @SerializedName("title")
    @Expose
    private String subject = "";

    @SerializedName("type")
    @Expose
    private String type = Constants.DEFAULT_DELEGATE;


    public transient ObservableField nameError;
    public transient ObservableField subjectError;
    public transient ObservableField emailError;
    public transient ObservableField messageError;


    public ContactUsRequest() {
        nameError = new ObservableField();
        subjectError = new ObservableField();
        emailError = new ObservableField();
        messageError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;
        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
            Timber.e("EMAIL:error");
        }
        if(!Validate.isValid(subject)) {
            subjectError.set(Validate.error);
            valid = false;
            Timber.e("EMAIL:error");
        }
        if(!Validate.isValid(message)) {
            messageError.set(Validate.error);
            valid = false;
            Timber.e("EMAIL:error");
        }
        return valid;
    }


    public boolean isValidSupport() {
        boolean valid = true;
        if(!Validate.isValid(message)) {
            messageError.set(Validate.error);
            valid = false;
            Timber.e("EMAIL:error");
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        messageError.set(null);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
        subjectError.set(null);
    }
}
