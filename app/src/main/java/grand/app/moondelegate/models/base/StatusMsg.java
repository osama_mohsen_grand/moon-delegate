package grand.app.moondelegate.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusMsg {
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("message")
    @Expose
    public String msg;
}
