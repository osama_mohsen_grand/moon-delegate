package grand.app.moondelegate.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.app.AppMoon;

public class ChatRequest {
    @SerializedName("order_id")
    @Expose
    public int order_id;

    @SerializedName("receiver_id")
    @Expose
    public int receiver_id;

    
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("receiver_type")
    @Expose
    public int receiver_type;

    @SerializedName("type")
    @Expose
    public String type = AppMoon.getUserType();

    public ChatRequest(int order_id ,int receiver_id, String message, int receiver_type) {
        this.order_id = order_id;
        this.receiver_id = receiver_id;
        this.message = message;
        this.receiver_type = receiver_type;
    }
}
