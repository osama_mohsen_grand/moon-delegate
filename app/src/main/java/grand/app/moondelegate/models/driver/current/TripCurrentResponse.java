package grand.app.moondelegate.models.driver.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moondelegate.models.base.StatusMsg;

public class TripCurrentResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public TripCurrent tripCurrent;

}
