package grand.app.moondelegate.models.user.profile.paymenttype;

public class PaymentTypeRequest {
    private int user_id;
    //1 cash, 2 online
    private int payment_type;

    public PaymentTypeRequest(int user_id, int payment_type) {
        this.user_id = user_id;
        this.payment_type = payment_type;
    }

    public void setPayment_type(int payment_type) {
        this.payment_type = payment_type;
    }
}
