package grand.app.moondelegate.models.country;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Region {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("region_name")
    @Expose
    public String regionName;
}
