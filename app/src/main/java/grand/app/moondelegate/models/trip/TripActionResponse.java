package grand.app.moondelegate.models.trip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.base.StatusMsg;

public class TripActionResponse extends StatusMsg {
    @SerializedName("price")
    @Expose
    public String price;
}
