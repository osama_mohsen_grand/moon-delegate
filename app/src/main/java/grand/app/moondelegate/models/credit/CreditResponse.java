package grand.app.moondelegate.models.credit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.base.StatusMsg;

public class CreditResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public Credit data;
}
