package grand.app.moondelegate.models.user.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trip {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("rate")
    @Expose
    public Integer rate;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("trip_id")
    @Expose
    public Integer tripId;
}
