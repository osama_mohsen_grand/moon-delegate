package grand.app.moondelegate.models.order.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.order.OrderDelegate;

public class OrdersDelegateListResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<OrderDelegate> orders;
}
