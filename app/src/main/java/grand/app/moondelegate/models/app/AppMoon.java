package grand.app.moondelegate.models.app;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import grand.app.moondelegate.R;
import grand.app.moondelegate.models.country.Datum;
import grand.app.moondelegate.notification.NotificationGCMModel;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import timber.log.Timber;

public class AppMoon {
    public static ArrayList<AccountTypeModel> accountType = new ArrayList<>();

    public static String getDefaultLocationCountry() {
        return "country_id=" + UserHelper.retrieveKey(Constants.COUNTRY_ID) + "&lat=" + UserHelper.retrieveKey(Constants.LAT) +
                "&lng=" + UserHelper.retrieveKey(Constants.LNG);
    }


    public static Datum getCities(String countryId, List<Datum> countries) {
        for (Datum country : countries) {
            if (String.valueOf(country.id).equals(countryId)) {
                return country;
            }
        }
        return null;
    }


    public static boolean searchVisibility(String id) {
        return false;
    }

    public static ArrayList<AccountTypeModel> getAccountType() {
        if (accountType.size() == 0) {
            accountType.add(new AccountTypeModel(Constants.DEFAULT_DELEGATE, ResourceManager.getString(R.string.delegate)));
//            accountType.add(new AccountTypeModel(Constants.DEFAULT_TAXI, ResourceManager.getString(R.string.taxi)));
//            accountType.add(new AccountTypeModel(Constants.DEFAULT_TRANSPORTATION, ResourceManager.getString(R.string.transporation)));
        }
        return accountType;
    }


    public static NotificationGCMModel getNotification(String mJsonString) {
        JsonParser parser = new JsonParser();
        JsonElement mJson = parser.parse(mJsonString);
        Gson gson = new Gson();
        return gson.fromJson(mJson, NotificationGCMModel.class);
    }

    public static String getUserType(){
        Timber.e("type:"+UserHelper.getUserDetails().type);
        return UserHelper.getUserDetails().type;
    }

    /*
    1: new order
2: confirm
3: on way
4: delivered
     */

    public static ArrayList<AccountTypeModel> getStatus(int status_id) {
        ArrayList<AccountTypeModel> status = new ArrayList<>();
        if (status_id == 2) {
            status.add(new AccountTypeModel("3", ResourceManager.getString(R.string.on_the_way)));
            status.add(new AccountTypeModel("4", ResourceManager.getString(R.string.delivered)));
        } else if (status_id == 3) {
            status.add(new AccountTypeModel("4", ResourceManager.getString(R.string.delivered)));
        }
        return status;
    }
}
