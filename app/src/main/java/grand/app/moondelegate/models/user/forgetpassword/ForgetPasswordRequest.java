package grand.app.moondelegate.models.user.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.Validate;
import grand.app.moondelegate.utils.storage.user.UserHelper;

public class ForgetPasswordRequest {

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";

    @SerializedName("check_phone")
    @Expose
    private String check_phone = "";



    public transient ObservableField emailError;
    public ForgetPasswordRequest(boolean isForgetPassword,String input) {
        this.type = Constants.DEFAULT_DELEGATE;
        if(isForgetPassword) {
            check_phone = "1";//forget password
            email = input;
        }else{
            check_phone = "2";//register and check phone
            phone = input;
        }
        emailError = new ObservableField();

    }

    public boolean validateInput() {
        boolean valid = true;
        if (!Validate.isValid(email)) {
            emailError.set(Validate.error);
            valid = false;
        }else
            emailError.set(null);

        return valid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }
}
