package grand.app.moondelegate.models.rate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;

public class RateRequest {
    @SerializedName("trip_id")
    @Expose
    public int trip_id;

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    @SerializedName("to_type")
    @Expose
    public String to_type = Constants.DEFAULT_USER;

    @SerializedName("rate")
    @Expose
    public float rate;

    @SerializedName("comment")
    @Expose
    public String feedback = "";

    public RateRequest(int trip_id, int rate) {
        this.trip_id = trip_id;
        this.rate = rate;
    }
}
