package grand.app.moondelegate.models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderConfirmRequest {
    @SerializedName("order_id")
    @Expose
    public Integer order_id;
    @SerializedName("status")
    @Expose
    public Integer status;

    public OrderConfirmRequest(int order_id) {
        this.order_id = order_id;
    }

    public OrderConfirmRequest(Integer order_id, Integer status) {
        this.order_id = order_id;
        this.status = status;
    }
}
