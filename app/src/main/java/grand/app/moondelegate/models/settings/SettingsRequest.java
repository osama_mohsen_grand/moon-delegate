package grand.app.moondelegate.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsRequest {
    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("app")
    @Expose
    public int app = 2;


    public SettingsRequest(int type) {
        this.type = type;
    }
}
