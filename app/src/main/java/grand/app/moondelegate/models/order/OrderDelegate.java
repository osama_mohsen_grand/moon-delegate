package grand.app.moondelegate.models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDelegate {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("order_number")
    @Expose
    public String orderNumber;
    @SerializedName("order_date")
    @Expose
    public String orderDate;
    @SerializedName("status_id")
    @Expose
    public Integer statusId;
    @SerializedName("order_status")
    @Expose
    public String orderStatus;
    @SerializedName("distance")
    @Expose
    public String distance;
    @SerializedName("cost")
    @Expose
    public String cost;

}
