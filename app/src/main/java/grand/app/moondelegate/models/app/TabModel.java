package grand.app.moondelegate.models.app;

import androidx.fragment.app.Fragment;

public class TabModel {
    public String name;
    public Fragment fragment;

    public TabModel(String name, Fragment fragment) {
        this.name = name;
        this.fragment = fragment;
    }
}
