package grand.app.moondelegate.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moondelegate.models.base.StatusMsg;

public class ChatDetailsResponse extends StatusMsg {
    @SerializedName("chats")
    @Expose
    public List<ChatDetailsModel> chats = null;

    @SerializedName("user")
    @Expose
    public ChatUser user = null;

    @SerializedName("delegate")
    @Expose
    public ChatDelegate delegate = null;

}
