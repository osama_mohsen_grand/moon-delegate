package grand.app.moondelegate.models.user.changepassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.utils.storage.user.UserHelper;

public class UpdatePasswordRequest {
    @SerializedName("driver_id")
    @Expose
    public int driver_id;
    @SerializedName("old_password")
    @Expose
    public String old_password;
    @SerializedName("password")
    @Expose
    public String password;

    public UpdatePasswordRequest(String old_password, String password) {
        this.driver_id = UserHelper.getUserDetails().id;
        this.old_password = old_password;
        this.password = password;
    }
}
