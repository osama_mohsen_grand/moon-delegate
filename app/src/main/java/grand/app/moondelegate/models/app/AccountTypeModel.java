package grand.app.moondelegate.models.app;

public class AccountTypeModel {
    public String id = "-1";
    public String type = "";

    public AccountTypeModel(String id, String type) {
        this.id = id;
        this.type = type;
    }
}
