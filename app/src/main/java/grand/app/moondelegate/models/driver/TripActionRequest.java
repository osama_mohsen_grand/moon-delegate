package grand.app.moondelegate.models.driver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moondelegate.utils.storage.location.LocationPosition;
import grand.app.moondelegate.utils.storage.user.UserHelper;

public class TripActionRequest {

    @SerializedName("trip_id")
    @Expose
    public int trip_id;
    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("locations")
    @Expose
    public ArrayList<LocationPosition> locations = null;

    public TripActionRequest(int trip_id,int status) {
        this.trip_id = trip_id;
        this.status = status;
    }
}
