package grand.app.moondelegate.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DescriptionResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("description")
        @Expose
        public String description;

    }
}
