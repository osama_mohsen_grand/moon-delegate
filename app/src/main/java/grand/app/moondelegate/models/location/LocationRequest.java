package grand.app.moondelegate.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.utils.storage.user.UserHelper;

public class LocationRequest {
    @SerializedName("available")
    @Expose
    public int available;
    @SerializedName("lat")
    @Expose
    public double lat;
    @SerializedName("lng")
    @Expose
    public double lng;

    public LocationRequest(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
