
package grand.app.moondelegate.models.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moondelegate.models.base.StatusMsg;

public class NotificationResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<Notification> data;


}
