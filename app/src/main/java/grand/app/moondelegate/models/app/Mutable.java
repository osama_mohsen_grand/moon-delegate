package grand.app.moondelegate.models.app;

import android.view.View;

public class Mutable {
    public String type;
    public int position;

    public View v = null;

    public Mutable(String type, int position) {
        this.type = type;
        this.position = position;
    }


    public Mutable(String type, int position, View v) {
        this.type = type;
        this.position = position;
        this.v = v;
    }
}
