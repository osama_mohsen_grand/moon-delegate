package grand.app.moondelegate.models.helper;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class MarkerHelper implements Parcelable {
    public String text_select = "";
    public LatLng latLng;

    public MarkerHelper(String text_select, LatLng latLng) {
        this.text_select = text_select;
        this.latLng = latLng;
    }

    protected MarkerHelper(Parcel in) {
        text_select = in.readString();
        latLng = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<MarkerHelper> CREATOR = new Creator<MarkerHelper>() {
        @Override
        public MarkerHelper createFromParcel(Parcel in) {
            return new MarkerHelper(in);
        }

        @Override
        public MarkerHelper[] newArray(int size) {
            return new MarkerHelper[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text_select);
        dest.writeParcelable(latLng, flags);
    }
}
