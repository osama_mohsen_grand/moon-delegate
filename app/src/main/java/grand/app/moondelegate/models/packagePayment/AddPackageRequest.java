package grand.app.moondelegate.models.packagePayment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.utils.storage.user.UserHelper;

public class AddPackageRequest {
    @SerializedName("type")
    @Expose
    public String type = "";

    @SerializedName("package_id")
    @Expose
    public int package_id;

    public AddPackageRequest(int package_id) {
        this.package_id = package_id;
        type = UserHelper.getUserDetails().type;
    }
}


