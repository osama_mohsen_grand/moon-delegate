
package grand.app.moondelegate.models.packagePayment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moondelegate.models.base.StatusMsg;


public class PackageResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<Datum> mData;


}
