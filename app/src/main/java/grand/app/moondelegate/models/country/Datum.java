package grand.app.moondelegate.models.country;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moondelegate.models.base.IdName;

public class Datum {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("country_name")
    @Expose
    public String countryName;
    @SerializedName("country_image")
    @Expose
    public String countryImage;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("cities")
    @Expose
    public List<City> cities = null;
    @SerializedName("trucks")
    @Expose
    public List<IdName> trucks = null;

    @SerializedName("currency")
    @Expose
    public String currency;
}
