package grand.app.moondelegate.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatDetailsModel {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("audio")
    @Expose
    public String audio;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("image")
    @Expose
    public String image;

    public String senderName = "";
    public String senderImage = "";

    public ChatDetailsModel() {
        message = "";
        audio = "";
        type = "";
        image = "";
    }
}
