package grand.app.moondelegate.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.base.StatusMsg;

public class ChatSendResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public ChatDetailsModel data;
}
