package grand.app.moondelegate.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moondelegate.models.base.StatusMsg;

public class ServiceResponse extends StatusMsg {
    @SerializedName("services")
    @Expose
    public List<Service> services = null;
}
