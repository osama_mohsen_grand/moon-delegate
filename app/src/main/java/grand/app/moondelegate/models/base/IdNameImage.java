package grand.app.moondelegate.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class IdNameImage implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image = "https://wallpaperbrowse.com/media/images/soap-bubble-1958650_960_720.jpg";
}
