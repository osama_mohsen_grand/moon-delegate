package grand.app.moondelegate.models.user.activation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.user.profile.User;


public class VerificationResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public User data;


//    public class Result{
//        @SerializedName("driver")
//        @Expose
//        public User driver;
//    }

}
