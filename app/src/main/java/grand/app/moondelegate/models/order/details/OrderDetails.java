package grand.app.moondelegate.models.order.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetails {

    @SerializedName("delegate")
    @Expose
    public Delegate delegate;

    @SerializedName("shop")
    @Expose
    public Shop shop;
    @SerializedName("sub_total")
    @Expose
    public String subTotal;
    @SerializedName("delivery")
    @Expose
    public String delivery;
    @SerializedName("total_price")
    @Expose
    public String totalPrice;

    @SerializedName("status_id")
    @Expose
    public Integer statusId;
    @SerializedName("order_status")
    @Expose
    public String orderStatus;

    @SerializedName("order_id")
    @Expose
    public int orderId;

    @SerializedName("products")
    @Expose
    public List<OrderProductDelegateDetails> products;

}
