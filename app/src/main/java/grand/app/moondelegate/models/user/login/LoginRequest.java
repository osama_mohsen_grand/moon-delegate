package grand.app.moondelegate.models.user.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.Validate;

public class LoginRequest {
    @SerializedName("email")
    @Expose
    private String email = "";
    @SerializedName("password")
    @Expose
    private String password = "";
    @SerializedName("type")
    @Expose
    private String type = "";


    public transient ObservableField typeError;
    public transient ObservableField emailError;
    public transient ObservableField passwordError;

    public LoginRequest() {
        typeError = new ObservableField();
        emailError = new ObservableField();
        passwordError = new ObservableField();
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        passwordError.set(null);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        typeError.set(null);
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(type)) {
            typeError.set(Validate.error);
            valid = false;
        } else
            typeError.set(null);
        if (!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
        } else
            emailError.set(null);
        if (!Validate.isValid(password)) {
            passwordError.set(Validate.error);
            valid = false;
        } else
            passwordError.set(null);
        return valid;
    }
}
