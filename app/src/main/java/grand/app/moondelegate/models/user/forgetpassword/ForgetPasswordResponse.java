package grand.app.moondelegate.models.user.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.base.StatusMsg;

public class ForgetPasswordResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public String data;

}
