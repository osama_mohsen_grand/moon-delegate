package grand.app.moondelegate.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaginationModel {
    @SerializedName("current_page")
    @Expose
    public Integer currentPage;
    @SerializedName("first_page_url")
    @Expose
    public String firstPageUrl;
    @SerializedName("from")
    @Expose
    public Integer from;
    @SerializedName("next_page_url")
    @Expose
    public Object nextPageUrl;
    @SerializedName("path")
    @Expose
    public String path;
    @SerializedName("per_page")
    @Expose
    public Integer perPage;
    @SerializedName("prev_page_url")
    @Expose
    public Object prevPageUrl;
    @SerializedName("to")
    @Expose
    public Integer to;
}
