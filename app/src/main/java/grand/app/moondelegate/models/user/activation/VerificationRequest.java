package grand.app.moondelegate.models.user.activation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.utils.Validate;

public class VerificationRequest {
    @SerializedName("phone")
    @Expose
    public String phone = "";
    @SerializedName("code")
    @Expose
    public String code = "";
    public transient ObservableField codeError;

    public VerificationRequest() {
        this.codeError = new ObservableField();
    }

    public boolean isValid() {
        if(!Validate.isValid(code)) {
            codeError.set(Validate.error);
            return false;
        }
        return true;
    }



}
