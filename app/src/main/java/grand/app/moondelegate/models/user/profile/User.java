package grand.app.moondelegate.models.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.driver.current.TripCurrent;

public class User {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("account_type")
    @Expose
    public String type;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("package_id")
    @Expose
    public Integer packageId;
    @SerializedName("is_shop")
    @Expose
    public Boolean isShop;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("jwt_token")
    @Expose
    public String jwtToken;

    @SerializedName("trip")
    @Expose
    public TripCurrent trip;
}
