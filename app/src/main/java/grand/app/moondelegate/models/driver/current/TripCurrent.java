package grand.app.moondelegate.models.driver.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TripCurrent implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("receiver_name")
    @Expose
    public String receiverName;
    @SerializedName("receiver_phone")
    @Expose
    public String receiverPhone;
    @SerializedName("goods_type")
    @Expose
    public Integer goodsType;
    @SerializedName("goods_weight")
    @Expose
    public String goodsWeight;
    @SerializedName("helper")
    @Expose
    public Integer helper;
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("start_lat")
    @Expose
    public String startLat;
    @SerializedName("start_lng")
    @Expose
    public String startLng;
    @SerializedName("start_address")
    @Expose
    public String startAddress;
    @SerializedName("end_lat")
    @Expose
    public String endLat;
    @SerializedName("end_lng")
    @Expose
    public String endLng;
    @SerializedName("end_address")
    @Expose
    public String endAddress;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("rate")
    @Expose
    public float rate;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("distance")
    @Expose
    public String distance;

    @SerializedName("trip_time")
    @Expose
    public String trip_time;
}
