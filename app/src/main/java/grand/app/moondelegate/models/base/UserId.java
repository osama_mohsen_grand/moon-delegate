package grand.app.moondelegate.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserId {
    @SerializedName("driver_id")
    @Expose
    public int driver_id;

    public UserId(int driver_id) {
        this.driver_id = driver_id;
    }
}
