
package grand.app.moondelegate.models.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @SerializedName("created_at")
    @Expose
    private String mCreatedAt;
    @SerializedName("id")
    @Expose
    private int mId;
    @SerializedName("message")
    @Expose
    private String mMessage;
    @SerializedName("title")
    @Expose
    private String mTitle;

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public int getmId() {
        return mId;
    }


    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

}
