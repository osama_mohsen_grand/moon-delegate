package grand.app.moondelegate.models.user.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.utils.Validate;
import grand.app.moondelegate.utils.storage.user.UserHelper;

public class SecondRegisterRequest {
    @SerializedName("driver_id")
    @Expose
    private int driver_id = -1;

    @SerializedName("model")
    @Expose
    private String model = "";

    @SerializedName("color")
    @Expose
    private String color = "";

    @SerializedName("plat_number")
    @Expose
    private String plat_number = "";


    public ObservableField modelError;
    public ObservableField colorError;
    public ObservableField plat_numberError;

    public SecondRegisterRequest() {
        driver_id = UserHelper.getUserDetails().id;
        modelError = new ObservableField();
        colorError = new ObservableField();
        plat_numberError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;
        if(!Validate.isValid(model)) {
            modelError.set(Validate.error);
            valid = false;
        }
        if(!Validate.isValid(color)) {
            colorError.set(Validate.error);
            valid = false;
        }
        if(!Validate.isValid(plat_number)) {
            plat_numberError.set(Validate.error);
            valid = false;
        }
        return valid;
    }


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
        modelError.set(null);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        colorError.set(null);
    }

    public String getPlat_number() {
        return plat_number;
    }

    public void setPlat_number(String plat_number) {
        this.plat_number = plat_number;
        plat_numberError.set(null);
    }
}
