package grand.app.moondelegate.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.base.StatusMsg;


public class SettingsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public String data;

}
