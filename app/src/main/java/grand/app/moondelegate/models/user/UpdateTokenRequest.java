package grand.app.moondelegate.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;

public class UpdateTokenRequest {
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("firebase_token")
    @Expose
    public String token;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("status")
    @Expose
    public String status = null;

    public UpdateTokenRequest(String token) {
        this.type = UserHelper.getUserDetails().type;
        this.token = token;
        this.lat = UserHelper.retrieveKey(Constants.LAT);
        this.lng = UserHelper.retrieveKey(Constants.LNG);
        this.address = UserHelper.retrieveKey(Constants.USER_ADDRESS);
    }

    public UpdateTokenRequest(String token,String status) {
        this.type = UserHelper.getUserDetails().type;
        this.token = token;
        this.lat = UserHelper.retrieveKey(Constants.LAT);
        this.lng = UserHelper.retrieveKey(Constants.LNG);
        this.address = UserHelper.retrieveKey(Constants.USER_ADDRESS);
        this.status = status;
    }

    public UpdateTokenRequest() {
        this.type = UserHelper.getUserDetails().type;
        this.token = UserHelper.retrieveKey(Constants.TOKEN);
        this.lat = UserHelper.retrieveKey(Constants.LAT);
        this.lng = UserHelper.retrieveKey(Constants.LNG);
        this.address = UserHelper.retrieveKey(Constants.USER_ADDRESS);
    }
}
