package grand.app.moondelegate.models.user.changepassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.Validate;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import timber.log.Timber;

public class ChangePasswordRequest {
    @SerializedName("old_password")
    @Expose
    private String oldPassword = "";
    @SerializedName("new_password")
    @Expose
    private String newPassword = "";

    private String confirmPassword = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";
    @SerializedName("type")
    @Expose
    private String type;

    public transient ObservableField oldPasswordError,newPasswordError,confrimPasswordError,phoneError;


    public boolean isLogin = false;

    public ChangePasswordRequest() {
        if(UserHelper.getUserId() != -1) isLogin = true;
        oldPasswordError = new ObservableField();
        newPasswordError = new ObservableField();
        confrimPasswordError = new ObservableField();
        phoneError = new ObservableField();
        type = UserHelper.getUserDetails().type;
    }


    public boolean validateInput() {
        boolean valid = true;
        String enter = "  " + ResourceManager.getString(R.string.please_enter) + " ";
        if (isLogin && !Validate.isValid(oldPassword)) {
            Timber.e("old password");
            oldPasswordError.set(Validate.error);
            valid = false;
        }else
            oldPasswordError.set(null);

        if (!Validate.isValid(newPassword)) {
            Timber.e("new password:"+Validate.error);
            newPasswordError.set(Validate.error);
            valid = false;
        }else newPasswordError.set(null);

        if (!Validate.isValid(confirmPassword)) {
            Timber.e("confirm password:"+Validate.error);
            confrimPasswordError.set(Validate.error);
            valid = false;
        }else
            confrimPasswordError.set(null);

        if (!Validate.isValid(phone) && !isLogin) {
            Timber.e("phone:"+Validate.error);
            phoneError.set(Validate.error);
            valid = false;
        }else
            phoneError.set(null);

        if(valid && !Validate.isMatchPassword(newPassword,confirmPassword)){
            newPasswordError.set(ResourceManager.getString(R.string.both_password_are_not_same));
            confrimPasswordError.set(ResourceManager.getString(R.string.both_password_are_not_same));
            valid = false;
        }else if(valid){
            newPasswordError.set(null);
            confrimPasswordError.set(null);
        }
        return valid;
    }


    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
        oldPasswordError.set(null);
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
        newPasswordError.set(null);
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
        confrimPasswordError.set(null);
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }
}
