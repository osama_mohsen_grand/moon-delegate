package grand.app.moondelegate.models.app;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moondelegate.models.base.StatusMsg;


public class PrivacyResponse  extends StatusMsg {
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("translation")
        @Expose
        public Translation translation;

    }

    public class Translation {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("privacy_policy_id")
        @Expose
        public Integer privacyPolicyId;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("locale")
        @Expose
        public String locale;

    }
}
