package grand.app.moondelegate.models.base;

public class UserRequest {
    public String user_id;
    public String jwt;

    public UserRequest(String user_id, String jwt) {
        this.user_id = user_id;
        this.jwt = jwt;
    }
}
