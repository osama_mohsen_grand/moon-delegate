package grand.app.moondelegate.models.country;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class City {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("city_name")
    @Expose
    public String cityName;
    @SerializedName("regions")
    @Expose
    public List<Region> regions = null;

}
