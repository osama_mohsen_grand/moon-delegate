package grand.app.moondelegate.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.ItemOrderDetailsDelegateBinding;
import grand.app.moondelegate.models.order.details.OrderProductDelegateDetails;
import grand.app.moondelegate.viewmodels.order.delegate.details.ItemOrderDelegateDetailsViewModel;


public class OrderDetailsDelegateAdapter extends RecyclerView.Adapter<OrderDetailsDelegateAdapter.OrderView> {
    private List<OrderProductDelegateDetails> orderProductDelegateDetails;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();

    public OrderDetailsDelegateAdapter(List<OrderProductDelegateDetails> orderProductDelegateDetails) {
        this.orderProductDelegateDetails = orderProductDelegateDetails;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public OrderView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemOrderDetailsDelegateBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_details_delegate, parent, false);
        return new OrderView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderView holder, final int position) {
        ItemOrderDelegateDetailsViewModel itemOrderViewModel = new ItemOrderDelegateDetailsViewModel(orderProductDelegateDetails.get(position), position);
        holder.itemOrderBinding.setItemOrderDetailsViewModel(itemOrderViewModel);
        setEvent(itemOrderViewModel);
    }

    private void setEvent(ItemOrderDelegateDetailsViewModel itemOrderViewModel) {
        itemOrderViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return orderProductDelegateDetails.size();
    }

    public class OrderView extends RecyclerView.ViewHolder {

        private ItemOrderDetailsDelegateBinding itemOrderBinding;

        public OrderView(@NonNull ItemOrderDetailsDelegateBinding itemOrderBinding) {
            super(itemOrderBinding.getRoot());
            this.itemOrderBinding = itemOrderBinding;
        }
    }
}
