package grand.app.moondelegate.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.ItemOrderDelegateBinding;
import grand.app.moondelegate.models.order.OrderDelegate;
import grand.app.moondelegate.viewmodels.home.ItemOrderDelegateViewModel;


public class OrderDelegateAdapter extends RecyclerView.Adapter<OrderDelegateAdapter.OrderDelegateView> {
    private List<OrderDelegate> orders;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public OrderDelegateAdapter(List<OrderDelegate> orders) {
        this.orders = orders;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public OrderDelegateView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemOrderDelegateBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_delegate,parent,false);
        return new OrderDelegateView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDelegateView holder, final int position) {
        ItemOrderDelegateViewModel itemOrderDelegateViewModel = new ItemOrderDelegateViewModel(orders.get(position),position);
        holder.itemDelegateOrderBinding.setItemOrderDelegateViewModel(itemOrderDelegateViewModel);
        setEvent(itemOrderDelegateViewModel);
    }

    private void setEvent(ItemOrderDelegateViewModel itemOrderDelegateViewModel) {
        itemOrderDelegateViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return orders.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class OrderDelegateView extends RecyclerView.ViewHolder{

        private ItemOrderDelegateBinding itemDelegateOrderBinding;
        public OrderDelegateView(@NonNull ItemOrderDelegateBinding  itemDelegateOrderBinding) {
            super(itemDelegateOrderBinding.getRoot());
            this.itemDelegateOrderBinding = itemDelegateOrderBinding;
        }
    }
}
