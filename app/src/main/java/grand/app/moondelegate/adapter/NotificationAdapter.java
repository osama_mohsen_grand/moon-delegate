package grand.app.moondelegate.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.ItemNotificationBinding;
import grand.app.moondelegate.models.notifications.Notification;
import grand.app.moondelegate.viewmodels.notification.ItemNotificationViewModel;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationView> {
    private List<Notification> notifications;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public NotificationAdapter(List<Notification> notifications) {
        this.notifications = notifications;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public NotificationView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemNotificationBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_notification,parent,false);
        return new NotificationView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationView holder, final int position) {
        ItemNotificationViewModel itemNotificationViewModel = new ItemNotificationViewModel(notifications.get(position),position);
        holder.itemNotificationBinding.setItemNotificationViewModel(itemNotificationViewModel);
        setEvent(itemNotificationViewModel);
    }

    private void setEvent(ItemNotificationViewModel itemNotificationViewModel) {
        itemNotificationViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        notifications.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public class NotificationView extends RecyclerView.ViewHolder{

        private ItemNotificationBinding itemNotificationBinding;
        public NotificationView(@NonNull ItemNotificationBinding itemNotificationBinding) {
            super(itemNotificationBinding.getRoot());
            this.itemNotificationBinding = itemNotificationBinding;
        }
    }
}
