package grand.app.moondelegate.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.ItemCountryBinding;
import grand.app.moondelegate.models.country.Datum;
import grand.app.moondelegate.viewmodels.country.ItemCountryViewModel;


public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryView> {
    private List<Datum> countries;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public CountryAdapter(List<Datum> countries) {
        this.countries = countries;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CountryView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemCountryBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_country,parent,false);
        return new CountryView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryView holder, final int position) {
        ItemCountryViewModel itemCountryViewModel = new ItemCountryViewModel(countries.get(position),position,(this.position == position));
        holder.itemCountryBinding.setItemCountryViewModel(itemCountryViewModel);
        setEvent(itemCountryViewModel);
    }

    private void setEvent(ItemCountryViewModel itemCountryViewModel) {
        itemCountryViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                int pos = (int) aVoid;
                mMutableLiveData.setValue(pos);
            }
        });
    }


    @Override
    public int getItemCount() {
        return countries.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class CountryView extends RecyclerView.ViewHolder{

        private ItemCountryBinding itemCountryBinding;
        public CountryView(@NonNull ItemCountryBinding itemCountryBinding) {
            super(itemCountryBinding.getRoot());
            this.itemCountryBinding = itemCountryBinding;
        }
    }
}
