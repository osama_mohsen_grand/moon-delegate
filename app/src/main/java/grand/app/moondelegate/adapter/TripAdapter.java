package grand.app.moondelegate.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.ItemHistoryBinding;
import grand.app.moondelegate.models.trip.TripHistoryResponse;
import grand.app.moondelegate.viewmodels.trip.ItemTripHistoryViewModel;


public class TripAdapter extends RecyclerView.Adapter<TripAdapter.TripView> {
    private List<TripHistoryResponse.Datum> trips;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    private static final String TAG = "TripAdapter";
    public TripAdapter(List<TripHistoryResponse.Datum> trips) {
        this.trips = trips;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public TripView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemHistoryBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_history,parent,false);
        return new TripView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TripView holder, final int position) {
        ItemTripHistoryViewModel itemTripViewModel = new ItemTripHistoryViewModel(trips.get(position),position);
        holder.itemTripBinding.setItemTripHistoryViewModel(itemTripViewModel);
        setEvent(itemTripViewModel);
    }

    private void setEvent(ItemTripHistoryViewModel itemTripViewModel) {
        itemTripViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return trips.size();
    }

    public class TripView extends RecyclerView.ViewHolder{

        private ItemHistoryBinding itemTripBinding;
        public TripView(@NonNull ItemHistoryBinding itemTripBinding) {
            super(itemTripBinding.getRoot());
            this.itemTripBinding = itemTripBinding;
        }
    }
}
