package grand.app.moondelegate.base;

import android.content.Context;

import java.util.Observable;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.disposables.CompositeDisposable;

public class ParentObservableViewModel extends Observable {
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    Context context = null;

    public ParentObservableViewModel(Context context) {
        this.context = context;
        initializeProgress();
    }

    public ParentObservableViewModel(){

    }

    public void initializeProgress() {

    }
    public void showProgress(){

    }

    public void hideProgress(){

    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
