package grand.app.moondelegate.base;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.disposables.CompositeDisposable;

public class ParentBaseObservableViewModel extends BaseObservable {
    public MutableLiveData<Object> mMutableLiveDataBaseObservable  = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
}
