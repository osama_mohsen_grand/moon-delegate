package grand.app.moondelegate.base;

import java.util.List;

import androidx.recyclerview.widget.DiffUtil;

public class MyDiffUtilCallBack extends DiffUtil.Callback {

    private List<Object> oldList;
    private List<Object> newList;

    public MyDiffUtilCallBack(List<Object> oldList, List<Object> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldItemPosition == newItemPosition;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition) == newList.get(newItemPosition);
    }
}
