package grand.app.moondelegate.base;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.squareup.picasso.Picasso;

import androidx.databinding.BindingAdapter;
import grand.app.moondelegate.R;
import grand.app.moondelegate.customviews.views.CustomWebView;
import grand.app.moondelegate.customviews.views.HtmlAudioHelper;
import grand.app.moondelegate.customviews.views.WebInterface;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.images.ImageLoaderHelper;
import grand.app.moondelegate.utils.images.zoom.ImageMatrixTouchHandler;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.vollyutils.AppHelper;
import grand.app.moondelegate.vollyutils.MyApplication;
import libs.mjn.scaletouchlistener.ScaleTouchListener;
import timber.log.Timber;

public class ApplicationBinding {


    @BindingAdapter("animation_submit")
    public static void animation(View view, IAnimationSubmit animationSubmit) {
        view.setOnTouchListener(new ScaleTouchListener() {
            @Override
            public void onClick(View v) {
                animationSubmit.animationSubmit();
            }
        });
    }


    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, String image) {

//        Picasso.with(imageView.getContext())
//                .load(image)
//                .placeholder(R.mipmap.ic_launcher)
//                .error(R.mipmap.ic_launcher)
//                .into(imageView);

        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
    }


    @BindingAdapter("width")
    public static void setWidth(LinearLayout linearLayout, int span) {
        int width = AppHelper.getScreenWidth(linearLayout.getContext()) / span;
        linearLayout.getLayoutParams().width = width;
    }


    @BindingAdapter("width")
    public static void setWidth(RelativeLayout relativeLayout, int span) {
        int width = AppHelper.getScreenWidth(relativeLayout.getContext()) / span;
        relativeLayout.getLayoutParams().width = width;
    }


    @BindingAdapter("drawable")
    public static void setBackgroundDrawable(RelativeLayout relativeLayout, Drawable drawable) {
        relativeLayout.setBackground(drawable);
    }


    @BindingAdapter("imageZoomUrl")
    public static void loadZoomImage(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
        imageView.setOnTouchListener(new ImageMatrixTouchHandler(MyApplication.getInstance()));
    }


    @BindingAdapter("audio")
    public static void loadAudio(PlayerView playerView, String audio) {
        SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(playerView.getContext());
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                .createMediaSource(Uri.parse(audio));
        player.setPlayWhenReady(false);
        player.prepare(mediaSource, true, false);
        playerView.setPlayer(player);
    }

    @BindingAdapter("audioHtml")
    public static void media(final CustomWebView webView,String url) {
        if(url != null) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webView.addJavascriptInterface(new WebInterface(webView.getContext()), "Android");
            String html = new HtmlAudioHelper().html(url);
            webView.loadData(html, "text/html", "UTF-8");
//            setExoPlayerView(simpleExoPlayerView,video);
        }

    }


    @BindingAdapter("timeMin")
    public static void time(TextView textView, String text) {
        textView.setText(text+" "+ ResourceManager.getString(R.string.min));
    }




    @BindingAdapter("color")
    public static void color(ImageView imageView, String color) {
        Timber.e("color:"+color);
        if(color != null && !color.equals("") && color.charAt(0) == '#') {
            imageView.setBackgroundColor(Color.parseColor(color));
        }
    }


    @BindingAdapter("txtColor")
    public static void setColor(TextView textView , int color){
        textView.setTextColor(color);
    }


    @BindingAdapter("rate")
    public static void setRate(final RatingBar ratingBar, float rate) {
        ratingBar.setRating(rate);
    }

    @BindingAdapter("images")
    public static void setImages(final SliderLayout sliderLayout, String[] images) {
        Log.e("slider","start");
        if(images != null) {
            sliderLayout.removeAllSliders();
            Log.e("setImageSlider", "" + images.length);
            try {
                for (int i = 0; i < images.length; i++) {
                    final String image = images[i];
                    try {

                        DefaultSliderView defaultSliderView;
                        defaultSliderView = new DefaultSliderView(sliderLayout.getContext());
                        defaultSliderView
                                .image(image)
                                .setScaleType(BaseSliderView.ScaleType.Fit);
                        defaultSliderView.bundle(new Bundle());
                        defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                                Intent intent = new Intent(sliderLayout.getContext(), BaseActivity.class);
                                intent.putExtra(Constants.PAGE, Constants.ZOOM);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constants.IMAGE, image);
                                intent.putExtra(Constants.BUNDLE,bundle);
                                sliderLayout.getContext().startActivity(intent);

                            }
                        });
                        sliderLayout.addSlider(defaultSliderView);
                        sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
                    } catch (Exception e) {
                        Log.e("setImageSliderException", "" + e.getMessage());
                        //  Toast.makeText(getApplicationContext(),"",Toast.LENGTH_LONG).show();
                    }

                }

                sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                sliderLayout.getPagerIndicator().setDefaultIndicatorColor(sliderLayout.getContext().getResources().getColor(R.color.colorAccent), sliderLayout.getContext().getResources().getColor(R.color.colorSilver));
                sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
            } catch (Exception e) {
                Log.e("Exception", "" + e.getMessage());
                e.getStackTrace();
            }
        }
    }



    public static DrawableCrossFadeFactory factory =
            new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();

//    public static void loadImage(ImageView imageView,String imageUrl){
//        Glide.with(imageView.getContext())
//                .load(imageUrl)
//                .transition(withCrossFade(factory))
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .placeholder(R.mipmap.ic_launcher)
//                .into(imageView);
//    }

}
