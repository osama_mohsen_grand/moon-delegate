package grand.app.moondelegate.base;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import es.dmoral.toasty.Toasty;
import grand.app.moondelegate.R;
import grand.app.moondelegate.notification.NotificationGCMModel;
import grand.app.moondelegate.repository.FirebaseRepository;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.MyContextWrapper;
import grand.app.moondelegate.utils.maputils.background.LocationBackground;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.activities.TrackingActivity;
import timber.log.Timber;

//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;


public class ParentActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,
//                ParentActivity.class));
        initializeLanguage();
//        initializeToken();
        initializeProgress();
    }

    protected void initializeLanguage() {
        LanguagesHelper.getCurrentLanguage();
    }


    protected void initializeToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(ParentActivity.this,
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        Timber.e("update Token");
                        String newToken = instanceIdResult.getToken();
                        UserHelper.saveKey(Constants.TOKEN, newToken);
                        Timber.e("Token:" + UserHelper.retrieveKey(Constants.TOKEN));
                        if (UserHelper.getUserId() != -1)
                            new FirebaseRepository().updateToken(newToken);
                    }
                });
    }

    Intent backgroundService = null;

    public void startBackgroundService() {
        if (!AppUtils.isServiceRunning(this, LocationBackground.class)) {
            backgroundService = new Intent(this, LocationBackground.class);
            stopService(backgroundService);
            startService(backgroundService);
        }
    }


    public void stopBackgroundService() {
        if (backgroundService != null) {
            stopService(backgroundService);
        }
    }

    BroadcastReceiver newOrder = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finishAffinity();
            Bundle bundle = intent.getBundleExtra(Constants.BUNDLE_NOTIFICATION);
            NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
            Intent intentTrack;
            if(notificationGCMModel.status != 2 && notificationGCMModel.status != 5) {
                intentTrack = new Intent(ParentActivity.this, TrackingActivity.class);
                intentTrack.putExtra(Constants.TRIP_ID, notificationGCMModel.order_id);
                intentTrack.putExtra(Constants.TYPE, notificationGCMModel.notification_type);
            }else{
                intentTrack = new Intent(ParentActivity.this, MainActivity.class);
            }
            finishAffinity();
            startActivity(intentTrack);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(newOrder, new IntentFilter(Constants.NEW_ORDER_TRACKING));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(newOrder);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(MyContextWrapper.wrap(newBase, LanguagesHelper.getCurrentLanguage()));
        } else {
            super.attachBaseContext(newBase);
        }
    }


    protected Dialog dialogLoader;


    public void initializeProgress() {
        View view = LayoutInflater.from(this).inflate(R.layout.loader_animation, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.customDialog);
        builder.setView(view);
        dialogLoader = builder.create();
    }

    public void showProgress() {
        //show dialog
        if (dialogLoader != null && !this.isFinishing()) {
            Log.e("dialog", "HERE2");
            dialogLoader.show();
        }

    }

    public void hideProgress() {
        if (dialogLoader != null && dialogLoader.isShowing() && !this.isFinishing())
            dialogLoader.dismiss();
    }


    public void handleActions(String action, String baseError) {
        Timber.e(action);
        if (action.equals(Constants.SHOW_PROGRESS)) showProgress();
        else if (action.equals(Constants.HIDE_PROGRESS)) hideProgress();
        else if (action.equals(Constants.ERROR_RESPONSE)) showError(baseError);
        else if (action.equals(Constants.SERVER_ERROR)) {
            hideProgress();
            showError(ResourceManager.getString(R.string.msg_server_error));
        } else if (action.equals(Constants.FAILURE_CONNECTION)) {
            hideProgress();
            noConnection();
        } else if (action.equals(Constants.LOGOUT)) {
            if (UserHelper.getUserId() != -1) {
                UserHelper.clearUserDetails();
                finishAffinity();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LOGIN);
                startActivity(intent);
            }
        }
    }

    public void noConnection() {
//        Snackbar snackBar = Snackbar.make(findViewById(R.id.ll_base_container),
//                getString(R.string.please_check_connection), Snackbar.LENGTH_LONG);
//        View view = snackBar.getView();
//        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
//        TextView textView = view.findViewById(R.id.snackbar_text);
//        textView.setGravity(Gravity.CENTER_VERTICAL);
//        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning, 0, 0, 0);
//        snackBar.show();
        Intent intent = new Intent(this, BaseActivity.class);
        intent.putExtra(Constants.PAGE, Constants.FAILURE_CONNECTION);
        finishAffinity();
        startActivity(intent);
    }

    public void showError(String msg) {
        Timber.e(msg);
        if (msg.equals(Constants.ERROR_LOGIN_RESPONSE))
            msg = getString(R.string.please_login_first);
        Snackbar snackBar = Snackbar.make(findViewById(R.id.ll_base_container),
                msg, Snackbar.LENGTH_LONG);
        View view = snackBar.getView();
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack));
        TextView textView = view.findViewById(R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning, 0, 0, 0);
        snackBar.show();
    }

    public void toastMessage(String message, int icon, int color) {
        Toasty.custom(this, message, icon, color, Toasty.LENGTH_SHORT, true, true).show();
    }

    public void toastMessage(String message) {
        Toasty.custom(this, message, R.drawable.ic_check_white, R.color.colorPrimary, Toasty.LENGTH_SHORT, true, true).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
                assert fragment != null;
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception ex) {
            Log.e("exceptionBase", ex.getMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Timber.e("location " + location.getLatitude() + "," + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
