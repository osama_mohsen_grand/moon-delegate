
package grand.app.moondelegate.base;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.vollyutils.MyApplication;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public abstract class ParentViewModel extends ParentBaseObservableViewModel {
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    public ObservableField<String> noDataText = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.data));
    public ObservableBoolean noDataTextDisplay = new ObservableBoolean(false);
    public ObservableBoolean show = new ObservableBoolean(false);


    public static ObservableInt progress;
    public String baseError = "";

    Context context = null;
    Animation hyperspaceJumpAnimation;

    public ParentViewModel(Context context) {
        this.context = context;

        hyperspaceJumpAnimation = AnimationUtils.loadAnimation(MyApplication.getInstance(), R.anim.bonuce);


        progress = new ObservableInt(View.GONE);
    }

    public ParentViewModel(){

    }


    public void noData(){
        noDataTextDisplay.set(true);
        notifyChange();
    }


    public void haveData(){
        noDataTextDisplay.set(false);
        notifyChange();
    }

    public void showPage(boolean show){
        this.show.set(show);
        notifyChange();
    }



    public void selectImageFromGallery(Fragment fragment){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(fragment.getActivity()),
//                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                fragment.requestPermissions(
//                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                        FilesUtilsFragment.RESULT_LOAD_FROM_GALLERY);
//            } else {
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.setType("image/*");
//                fragment.startActivityForResult(intent, FilesUtilsFragment.PICK_IMAGE_REQUEST);
//            }
//        }
    }


    public void selectImageFromCamera(Fragment fragment){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(fragment.getActivity()),
//                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                fragment.requestPermissions(
//                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                        FilesUtilsFragment.RESULT_LOAD_VIDEO_FROM_GALLERY);
//            } else {
//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                fragment.startActivityForResult(intent, FilesUtilsFragment.PICK_IMAGE_CAMERA);
//            }
//        }
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
