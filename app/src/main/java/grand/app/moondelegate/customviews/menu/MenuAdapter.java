package grand.app.moondelegate.customviews.menu;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.ItemMenuBinding;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuView> {
    private List<MenuModel> menuModels;
    private LayoutInflater layoutInflater;
    public String selectId = "";
    boolean isActive = false;
    public MutableLiveData<Integer> mutableLiveDataAdapter = new MutableLiveData<>();

    public MenuAdapter(List<MenuModel> menuModels) {
        this.menuModels = menuModels;
    }


    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemMenuBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_menu,parent,false);
        return new MenuView(binding);
    }

    ItemMenuViewModel itemMenuViewModel = null;
    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        itemMenuViewModel = new ItemMenuViewModel(menuModels.get(position),position);
        holder.itemMenuBinding.setItemMenuViewModel(itemMenuViewModel);
        itemMenuViewModel.getOnGetDataListener().observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                selectId =  menuModels.get(position).id;
                mutableLiveDataAdapter.setValue(position);
            }
        });

    }



    public void update(List<MenuModel> dataList) {
        this.menuModels = dataList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class MenuView extends RecyclerView.ViewHolder{

        public ItemMenuBinding itemMenuBinding;
        public MenuView(@NonNull ItemMenuBinding itemMenuBinding) {
            super(itemMenuBinding.getRoot());
            this.itemMenuBinding = itemMenuBinding;
        }
    }
}
