package grand.app.moondelegate.customviews.views;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class CustomWebView extends WebView {
    public CustomWebView(Context context) {
        super(context);
        init();
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        getSettings().setAppCachePath(getContext().getCacheDir().getPath());
        getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setDisplayZoomControls(true);
        getSettings().setAppCacheEnabled(true);
    }
}
