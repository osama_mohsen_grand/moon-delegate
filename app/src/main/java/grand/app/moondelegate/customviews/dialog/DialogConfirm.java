package grand.app.moondelegate.customviews.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.LayoutDialogRemoveBinding;
import grand.app.moondelegate.utils.dialog.DialogHelperInterface;
import grand.app.moondelegate.utils.resources.ResourceManager;


public class DialogConfirm {
    Context context;
    String title = ResourceManager.getString(R.string.title), message = ResourceManager.getString(R.string.do_you_want_delete_product);
    String actionText = ResourceManager.getString(R.string.remove_item);
    String actionCancel = ResourceManager.getString(R.string.cancel);

    public DialogConfirm(Context context) {
        this.context = context;
        init();
    }

    public DialogConfirm setTitle(String title){
        this.title = title;
        return this;
    }

    public DialogConfirm setMessage(String message){
        this.message = message;
        return this;
    }

    public DialogConfirm setActionText(String actionText) {
        this.actionText = actionText;
        return this;
    }

    public DialogConfirm setActionCancel(String actionCancel) {
        this.actionCancel = actionCancel;
        return this;
    }

    LayoutDialogRemoveBinding layoutDialogRemoveBinding = null;

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutDialogRemoveBinding  = DataBindingUtil.inflate(layoutInflater, R.layout.layout_dialog_remove, null, true);
    }

    public void show(DialogHelperInterface dialogHelperInterface){
        View view = layoutDialogRemoveBinding.getRoot();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialogAnimationUpBottom;
        dialog.setView(view);
        dialog.show();

        layoutDialogRemoveBinding.tvDialogTitle.setText(title);
        layoutDialogRemoveBinding.tvDialogMessage.setText(message);
        layoutDialogRemoveBinding.btnDialogSubmit.setText(actionText);
        layoutDialogRemoveBinding.btnDialogCancel.setText(actionCancel);
        layoutDialogRemoveBinding.btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        layoutDialogRemoveBinding.imgDialogClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        layoutDialogRemoveBinding.btnDialogSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialogHelperInterface.OnClickListenerContinue(dialog,v);
            }
        });
    }
}
