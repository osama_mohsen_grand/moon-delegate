package grand.app.moondelegate.customviews.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;
import grand.app.moondelegate.utils.LanguagesHelper;

public class CustomImageView extends AppCompatImageView {


    public CustomImageView(Context context) {
        super(context);
        init();
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if(LanguagesHelper.getCurrentLanguage().equals("ar")){
            setRotation(getRotation()+180);
        }
    }
}
