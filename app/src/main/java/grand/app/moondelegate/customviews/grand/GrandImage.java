package grand.app.moondelegate.customviews.grand;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;

import grand.app.moondelegate.R;


public class GrandImage extends androidx.appcompat.widget.AppCompatImageView
{
    public GrandImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        setImage(context);
    }

    public void setImage(final Context context){
        this.setImageResource(R.drawable.by_grand_black);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.2grand.net"));
                context.startActivity(browserIntent);
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }


}
