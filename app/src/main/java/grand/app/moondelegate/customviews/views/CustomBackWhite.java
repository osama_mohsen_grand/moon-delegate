package grand.app.moondelegate.customviews.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.LanguagesHelper;


/**
 * Created by osama on 20/03/2018.
 */

public class CustomBackWhite extends androidx.appcompat.widget.AppCompatImageView
{
    Context context = null;

    public CustomBackWhite(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setImage();
        setRotation();
        setEvent();
    }

    private void setEvent() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity)context).onBackPressed();
            }
        });
    }

    private void setImage() {
        this.setImageResource( R.drawable.ic_back_white);
    }

    public void setRotation(){
        if(LanguagesHelper.getCurrentLanguage().equals("ar")){
            setRotation(getRotation()+180);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }


}
