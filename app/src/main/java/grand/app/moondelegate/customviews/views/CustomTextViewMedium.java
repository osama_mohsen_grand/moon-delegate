package grand.app.moondelegate.customviews.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by mohamedatef on 1/8/19.
 */

public class CustomTextViewMedium extends AppCompatTextView {

    public CustomTextViewMedium(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);

        init();
        style(attrs);
    }

    public CustomTextViewMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        style(attrs);
    }

    private void style(AttributeSet attrs) {
//        int style = attrs.getStyleAttribute();
//        if(style == R.style.btn_submit_primary){
//            ScaleTouchListener.Config config = new ScaleTouchListener.Config(
//                    300,    // Duration
//                    0.75f,  // ScaleDown
//                    0.75f); // Alpha
//
//            setOnTouchListener(new ScaleTouchListener(config) {     // <--- pass config object
//                @Override
//                public void onClick(View v) {
//                    //OnClickListener
//
//                }
//            });
//        }
    }

    public CustomTextViewMedium(Context context) {
        super(context);
        init();
    }

    private void init() {

//        if (!isInEditMode()) {
            Typeface font = null;
            font = Typeface.createFromAsset(getContext().getAssets(), "fonts/medium.otf");
            setTypeface(font);
//        }

        if(getGravity()== Gravity.START || getGravity() == 8388659){
            setTextAlignment(TEXT_ALIGNMENT_VIEW_START);
        }

    }
}