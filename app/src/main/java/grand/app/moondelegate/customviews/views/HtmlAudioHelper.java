package grand.app.moondelegate.customviews.views;


public class HtmlAudioHelper {
    public String html(String audioUrl){
        String html = "";

        html += "<!doctype html>\n" +
                "<html>\n" +
                "    <div style=\"margin: 0 auto; width: 50px; display: table;\">\n" +
                "\n" +
                "        <audio controls=\"controls\" controlsList=\"nodownload\">\n" +
                "            <source src=\""+audioUrl+"\" type=\"audio/mpeg\">\n" +
                "        </audio>\n" +
                "    </div>\n" +
                "    </body>\n" +
                "</html>\n";

        return html;
    }
}
