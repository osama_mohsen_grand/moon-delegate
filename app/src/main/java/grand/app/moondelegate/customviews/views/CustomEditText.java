package grand.app.moondelegate.customviews.views;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.animation.AnimationUtils;

import com.google.android.material.textfield.TextInputEditText;

import grand.app.moondelegate.R;
import grand.app.moondelegate.vollyutils.MyApplication;

public class CustomEditText extends TextInputEditText {
    private Validator validator;
    Context context = null;
    public interface Validator {
        boolean validate(String input);
    }

    private void checkType() {
        if(getInputType() == InputType.TYPE_CLASS_PHONE)
            setTextDirection(TEXT_DIRECTION_LTR);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        checkType();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        checkType();
    }

    public CustomEditText(Context context) {
        super(context);
        this.context = context;
        checkType();
    }

    @Override
    public void setError(CharSequence error) {
        super.setError(error);
        if(error != null && context != null) {
            startAnimation(AnimationUtils.loadAnimation(MyApplication.getInstance(), R.anim.shake_vibrate));
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
//        setSelection(lengthAfter);
    }
}
