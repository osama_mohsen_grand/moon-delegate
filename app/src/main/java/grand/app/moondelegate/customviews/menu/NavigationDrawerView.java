package grand.app.moondelegate.customviews.menu;

/**
 * Created by mohamedatef on 12/30/18.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentActivity;
import grand.app.moondelegate.customviews.actionbar.HomeActionBarView;
import grand.app.moondelegate.models.user.profile.User;
import grand.app.moondelegate.databinding.LayoutNavigationDrawerBinding;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.MovementHelper;
import grand.app.moondelegate.utils.images.ImageLoaderHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.MainActivity;
import grand.app.moondelegate.views.fragments.auth.ProfileFragment;
import grand.app.moondelegate.views.fragments.contactAndSupport.ContactUsFragment;
import grand.app.moondelegate.views.fragments.contactAndSupport.TechnicalSupportFragment;
import grand.app.moondelegate.views.fragments.credit.CreditFragment;
import grand.app.moondelegate.views.fragments.home.HomeDelegateFragment;
import grand.app.moondelegate.views.fragments.home.HomeMapFragment;
import grand.app.moondelegate.views.fragments.notification.NotificationFragment;
import grand.app.moondelegate.views.fragments.order.OrderHistoryFragment;
import grand.app.moondelegate.views.fragments.settings.LanguageFragment;
import grand.app.moondelegate.views.fragments.settings.SettingsFragment;
import grand.app.moondelegate.views.fragments.trip.TripHistoryFragment;
import timber.log.Timber;


public class NavigationDrawerView extends RelativeLayout {
    public MutableLiveData<Object> mutableLiveData = new MutableLiveData<>();
    public LayoutNavigationDrawerBinding layoutNavigationDrawerBinding;
    Context context;
    HomeActionBarView homeActionBarView;

    public NavigationDrawerView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public NavigationDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public NavigationDrawerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {


//                MovementHelper.addFragment(getContext(),new Fragment(),"fda");
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true);
        setHeader();
        setList();
        setEvents();
    }

    public void setHeader() {
        if (UserHelper.getUserId() != -1) {
            User user = UserHelper.getUserDetails();
            layoutNavigationDrawerBinding.tvNavigationName.setText(user.name);
            ImageLoaderHelper.ImageLoaderLoad(getContext(), user.image, layoutNavigationDrawerBinding.civMainUserImage);
        }
    }

    MenuAdapter menuAdapter = null;
    List<MenuModel> menu = new ArrayList<>();

    private void setList() {
        //Add List Here
        menuAdapter = new MenuAdapter(menu);
        menu.add(new MenuModel(Constants.HOME, context.getString(R.string.label_home), ResourceManager.getDrawable(R.drawable.ic_home_primary), ""));
        menu.add(new MenuModel(Constants.PROFILE, context.getString(R.string.profile), ResourceManager.getDrawable(R.drawable.ic_profile_primary), ""));
        menu.add(new MenuModel(Constants.ORDERS, context.getString(R.string.orders), ResourceManager.getDrawable(R.drawable.ic_order), ""));
        if(UserHelper.getUserDetails().type != null && UserHelper.getUserDetails().type.equals(Constants.DEFAULT_DELEGATE))
            menu.add(new MenuModel(Constants.CREDIT, context.getString(R.string.credit_card), ResourceManager.getDrawable(R.drawable.ic_credit), ""));
        menu.add(new MenuModel(Constants.NOTIFICATION, context.getString(R.string.label_notification), ResourceManager.getDrawable(R.drawable.ic_notification_strock), ""));
        menu.add(new MenuModel(Constants.CONTACT_US, context.getString(R.string.contact_us), ResourceManager.getDrawable(R.drawable.ic_contact), ""));
        menu.add(new MenuModel(Constants.TECHNICAL_SUPPORT, context.getString(R.string.technical_support), ResourceManager.getDrawable(R.drawable.ic_support), ""));
        menu.add(new MenuModel(Constants.SHARE, context.getString(R.string.label_share_app), ResourceManager.getDrawable(R.drawable.ic_share_primary), ""));

        menu.add(new MenuModel(Constants.RATE, context.getString(R.string.rate_app), ResourceManager.getDrawable(R.drawable.ic_star_primary), ""));
        menu.add(new MenuModel(Constants.LOGOUT, context.getString(R.string.label_logout), ResourceManager.getDrawable(R.drawable.ic_logout_primary), ""));


//        menu.add(new MenuModel(Constants.SUPPORT, context.getString(R.string.technical_support), ResourceManager.getDrawable(R.drawable.ic_help_and_support_primary), ""));
//        menu.add(new MenuModel(Constants.PRIVACY_POLICY, context.getString(R.string.label_privacy_policy), ResourceManager.getDrawable(R.drawable.ic_be_privacy_primary), ""));


        AppUtils.initVerticalRV(layoutNavigationDrawerBinding.rvNavigationDrawerList, context, 1);
        layoutNavigationDrawerBinding.rvNavigationDrawerList.setAdapter(menuAdapter);
    }

    private static final String TAG = "NavigationDrawerView";


    public MenuAdapter getAdapter() {
        return menuAdapter;
    }

    //home - profile - ads - stories - chat_history - gallery - orders - reviews - famous_people - credit_card
    //label_notification - photographer_people - help_and_support - label_privacy_policy - label_language
    // label_share_app - rate_app - be_shop - label_logout
    private void setEvents() {

        menuAdapter.mutableLiveDataAdapter.observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer position) {
                // select menu
                int pos = position;
                String id = menu.get(pos).id;
                if (id.equals(Constants.HOME)) {
                    homePage();
                } else if (id.equals(Constants.ORDERS)) {
                    //orders
                   previousOrders();
                } else if (id.equals(Constants.PROFILE)) {
                    //profile
                    MovementHelper.replaceFragmentTag(context, new ProfileFragment(), Constants.PROFILE, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.profile));
                } else if (id.equals(Constants.NOTIFICATION)) {
                    //notifications
                    NotificationFragment notificationFragment = new NotificationFragment();
                    MovementHelper.replaceFragmentTag(context, notificationFragment, Constants.NOTIFICATION, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.label_notification));
                } else if (id.equals(Constants.CREDIT)) {
                    MovementHelper.replaceFragmentTag(context, new CreditFragment(), Constants.CREDIT, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.credit_card));
                } else if (id.equals(Constants.SUPPORT)) {
                    //support
                    SettingsFragment settingsFragment = new SettingsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, Constants.TYPE_SUPPORT);
                    settingsFragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, settingsFragment, Constants.SETTINGS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.help_and_support));
                } else if (id.equals(Constants.PRIVACY_POLICY)) {
                    //privacy
                    SettingsFragment settingsFragment = new SettingsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, Constants.TYPE_PRIVACY);
                    settingsFragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, settingsFragment, Constants.SETTINGS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.label_privacy_policy));
                } else if (id.equals(Constants.LANG)) {
                    //language
                    MovementHelper.replaceFragmentTag(context, new LanguageFragment(), Constants.LANGUAGE, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.label_language));
                    homeActionBarView.setSearchVisibility(Constants.LANGUAGE);
                } else if (id.equals(Constants.RATE)) {
                    //rate app
                    MovementHelper.startWebPage(context, AppUtils.getPlayStoreLink(context));
                } else if (id.equals(Constants.SHARE)) {
                    //share app
                    AppUtils.shareUrl(AppUtils.getPlayStoreLink(context), context);
                } else if (id.equals(Constants.CONTACT_US)) {
                    //logout
                    MovementHelper.replaceFragmentTag(context, new ContactUsFragment(), Constants.CONTACT_US, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.contact_us));
                    homeActionBarView.setSearchVisibility(Constants.CONTACT_US);
                }else if (id.equals(Constants.TECHNICAL_SUPPORT)) {
                    //logout
                    MovementHelper.replaceFragmentTag(context, new TechnicalSupportFragment(), Constants.TECHNICAL_SUPPORT, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.technical_support));
                    homeActionBarView.setSearchVisibility(Constants.TECHNICAL_SUPPORT);
                } else if (id.equals(Constants.LOGOUT)) {
                    //logout
                    UserHelper.clearUserDetails();
                    ((ParentActivity) context).finishAffinity();
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.LOGIN);
                    context.startActivity(intent);
                }
            }
        });

    }

    public void setActionBar(HomeActionBarView homeActionBarView) {
        this.homeActionBarView = homeActionBarView;
    }


    public void homePage() {
        if(UserHelper.getUserDetails().type.equals(Constants.DEFAULT_DELEGATE)) {
            MovementHelper.replaceFragmentTag(context, new HomeDelegateFragment(), Constants.HOME, "");
        }else{
            MovementHelper.replaceFragmentTag(context, new HomeMapFragment(), Constants.HOME, "");
        }
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.label_home));
        homeActionBarView.setSearchVisibility(Constants.HOME);
    }

    public void previousOrders() {
        if(UserHelper.getUserDetails().type.equals(Constants.DEFAULT_DELEGATE)) {
            MovementHelper.replaceFragmentTag(context, new OrderHistoryFragment(), Constants.ORDERS, "");
            MovementHelper.popAllFragments(context);
            mutableLiveData.setValue(context.getString(R.string.orders));
            homeActionBarView.setSearchVisibility(Constants.ORDERS);
        }else if(UserHelper.getUserDetails().type.equals(Constants.DEFAULT_TAXI)){
            TripHistoryFragment tripHistoryFragment = new TripHistoryFragment();
            Bundle bundle = new Bundle();
            tripHistoryFragment.setArguments(bundle);
            MovementHelper.replaceFragmentTag(context, tripHistoryFragment, Constants.TAXI, "");
            MovementHelper.popAllFragments(context);
            mutableLiveData.setValue(context.getString(R.string.taxi_history));
        }else if (UserHelper.getUserDetails().type.equals(Constants.DEFAULT_TRANSPORTATION)) {
            TripHistoryFragment tripHistoryFragment = new TripHistoryFragment();
            Bundle bundle = new Bundle();
            tripHistoryFragment.setArguments(bundle);
            MovementHelper.replaceFragmentTag(context, tripHistoryFragment, Constants.TRUCKS, "");
            MovementHelper.popAllFragments(context);
            mutableLiveData.setValue(context.getString(R.string.trucks_history));

        }
    }
}

