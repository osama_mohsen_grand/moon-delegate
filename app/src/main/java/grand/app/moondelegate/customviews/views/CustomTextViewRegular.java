package grand.app.moondelegate.customviews.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.appcompat.widget.AppCompatTextView;
import libs.mjn.scaletouchlistener.ScaleTouchListener;

/**
 * Created by mohamedatef on 1/8/19.
 */

public class CustomTextViewRegular extends AppCompatTextView {

    ScaleTouchListener.Config config;

    public CustomTextViewRegular(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);

        init(context);
        style(attrs);
    }

    public CustomTextViewRegular(Context context, AttributeSet attrs) {

        super(context, attrs);

        init(context);
        style(attrs);

    }

    private void style(AttributeSet attrs) {
//        int style = attrs.getStyleAttribute();
//        if(style == R.style.btn_submit_primary){
//
//            setOnTouchListener(new ScaleTouchListener(config) {     // <--- pass config object
//                @Override
//                public void onClick(View v) {
//                    //OnClickListener
//                }
//            });
//
//        }
    }


    public CustomTextViewRegular(Context context) {
        super(context);

        init(context);

    }

    private void init(Context context) {
        Typeface font = null;
        font = Typeface.createFromAsset(getContext().getAssets(), "fonts/medium.otf");
        setTypeface(font);
        if (getGravity() == Gravity.START || getGravity() == 8388659) {
            setTextAlignment(TEXT_ALIGNMENT_VIEW_START);
        }
    }

}