package grand.app.moondelegate.customviews.views;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ProgressBar;

import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import timber.log.Timber;

//import grand.app.moondelegate.adapter.AutoCompleteShopAdapter;
//import grand.app.moondelegate.models.famous.search.FamousSearchShopResponse;


public class CustomAutoCompleteTextView extends AppCompatAutoCompleteTextView {
    private static final int MESSAGE_TEXT_CHANGED = 100;
    private static final int DEFAULT_AUTOCOMPLETE_DELAY = 750;
    private int mAutoCompleteDelay = DEFAULT_AUTOCOMPLETE_DELAY;
    private ProgressBar mLoadingIndicator;
    private Activity activity;

    public String oldText = "", newText = "";

    public CustomAutoCompleteTextView(Context context) {
        super(context);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            CustomAutoCompleteTextView.super.performFiltering((CharSequence) msg.obj, msg.arg1);
            String name = msg.obj.toString();
            Timber.e("name:" + name);
//            getShopByName(name);
        }
    };


    long lastPress = 0l;

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        super.addTextChangedListener(watcher);

    }


//    public void getShopByName(String name) {
//        showProgress();
//        new ConnectionHelper(new ConnectionListener() {
//            @Override
//            public void onRequestSuccess(Object response) {
//                Timber.e("response:" + name);
//                hideProgress();
//                FamousSearchShopResponse famousSearchShopResponse = (FamousSearchShopResponse) response;
//                if (famousSearchShopResponse != null) {
//                    AutoCompleteShopAdapter famousShopSearchAdapter = (AutoCompleteShopAdapter) getAdapter();
//                    Timber.e("before_size:" + famousShopSearchAdapter.shopSearchResults.size());
//                    famousShopSearchAdapter.update(famousSearchShopResponse.data);
//                    Timber.e("response_size:" + famousShopSearchAdapter.shopSearchResults.size());
////                    setAdapter(famousShopSearchAdapter);
////                    ((AutoCompleteShopAdapter) getAdapter()).notifyDataSetChanged();
//
//                    showDropDown();
//                    requestFocus();
//                    //famousShopSearchAdapter.notifyDataSetChanged();
//
//                }
//            }
//
//            @Override
//            public void onRequestError(Object error) {
//                super.onRequestError(error);
//                Toast.makeText(getContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
//                hideProgress();
//            }
//        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_SEARCH_SHOP + "?name=" + name, null, FamousSearchShopResponse.class);
//    }


    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        newText = text.toString();
        Timber.e("textChange:" + text);
        if (System.currentTimeMillis() - lastPress > mAutoCompleteDelay) {
            lastPress = System.currentTimeMillis();
            Timber.e("press" + lastPress);
        }
    }



    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setLoadingIndicator(Activity activity , ProgressBar progressBar) {
        this.activity = activity;
        mLoadingIndicator = progressBar;
    }

    public void setAutoCompleteDelay(int autoCompleteDelay) {
        Timber.e("Start");
        mAutoCompleteDelay = autoCompleteDelay;
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do your work
//                Timber.e("oldText:"+oldText);
//                Timber.e("newText:"+newText);
//                if(!oldText.equals(newText)){
//                    oldText = newText;
//                    getShopByName(oldText);
//                }
//            }
//        },mAutoCompleteDelay);


        Timer timer = new Timer();
        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run() {
                // your code here...
                if (!oldText.equals(newText)) {
                    oldText = newText;
//                    getShopByName(oldText);
                }
            }
        };

// schedule the task to run starting now and then every hour...
        timer.schedule(hourlyTask, 0l, mAutoCompleteDelay);

    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
//        if (mLoadingIndicator != null) {
//            mLoadingIndicator.setVisibility(View.VISIBLE);
//        }
//        mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
//        mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_CHANGED, text), mAutoCompleteDelay);
    }

    @Override
    public void onFilterComplete(int count) {
//        if (mLoadingIndicator != null) {
//            mLoadingIndicator.setVisibility(View.GONE);
//        }
        super.onFilterComplete(count);
    }

    public void showProgress() {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // Stuff that updates the UI
                mLoadingIndicator.setVisibility(VISIBLE);

            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
            showDropDown();
        return super.onTouchEvent(event);
    }

    public void hideProgress() {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // Stuff that updates the UI
                mLoadingIndicator.setVisibility(GONE);

            }
        });
    }
}
