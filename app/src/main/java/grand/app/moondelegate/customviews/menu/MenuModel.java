package grand.app.moondelegate.customviews.menu;

import android.graphics.drawable.Drawable;

public class MenuModel {
    public String id;
    public String title;
    public String price;
    public Drawable icon;

    public MenuModel(String id,String title, Drawable icon,String price) {
        this.id = id;
        this.title = title;
        this.icon = icon;
        this.price = price;
    }
}
