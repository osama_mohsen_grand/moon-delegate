package grand.app.moondelegate.customviews.actionbar;

/**
 * Created by mohamedatef on 12/30/18.
 */


import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import grand.app.moondelegate.R;
import grand.app.moondelegate.databinding.LayoutActionBarTitleBackBinding;
//import r.royak.grandmvvm.databinding.LayoutActionBarBackBinding;


public class BackTitleActionBarView extends RelativeLayout {
    LayoutActionBarTitleBackBinding layoutActionBarTitleBackBinding;

    public BackTitleActionBarView(Context context) {
        super(context);
        init();
    }

    public BackTitleActionBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public BackTitleActionBarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarTitleBackBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_title_back, this, true);
        setEvents();
    }

    private void setEvents() {
        layoutActionBarTitleBackBinding.imgActionBarCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
                ((Activity) getContext()).finish();
            }
        });
    }

    public void setTitle(String title) {
        layoutActionBarTitleBackBinding.tvActionBarTitle.setText(title);
    }


    public void setTextBack(String textBack) {
        layoutActionBarTitleBackBinding.tvActionBarTitleMessage.setText(textBack);
    }


}
