package grand.app.moondelegate.customviews.menu;

import android.graphics.drawable.Drawable;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.base.ParentBaseObservableViewModel;


public class ItemMenuViewModel extends ParentBaseObservableViewModel {
    private MenuModel menuModel = null;
    private int position = 0;
    public int height = 0;


    public ItemMenuViewModel(MenuModel menuModel, int position) {
        this.menuModel = menuModel;
        this.position = position;
    }


    public void submitMenu(){
        mMutableLiveDataBaseObservable.setValue(position);
    }

    public MutableLiveData<Object> getOnGetDataListener() {
        if(mMutableLiveDataBaseObservable==null)mMutableLiveDataBaseObservable = new MutableLiveData<>();
        return mMutableLiveDataBaseObservable;
    }



    @Bindable
    public String getTitle(){
        return menuModel.title;
    }

    @Bindable
    public String getPrice(){
        return menuModel.price;
    }


    @Bindable
    public Drawable getIcon(){
        return menuModel.icon;
    }

}
