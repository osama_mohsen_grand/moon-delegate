package grand.app.moondelegate.customviews.actionbar;

/**
 * Created by mohamedatef on 12/30/18.
 */


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import grand.app.moondelegate.R;
import grand.app.moondelegate.customviews.menu.NavigationDrawerView;
import grand.app.moondelegate.databinding.LayoutActionBarHomeBinding;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.views.activities.BaseActivity;

public class HomeActionBarView extends RelativeLayout {
    public LayoutActionBarHomeBinding layoutActionBarHomeBinding;
    NavigationDrawerView navigationDrawerView;
    DrawerLayout drawerLayout;
    ScrollView svMenu;

    public HomeActionBarView(Context context) {
        super(context);
        init();
    }


    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarHomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_home, this, true);
        setEvents();
    }

    private void setEvents() {
        layoutActionBarHomeBinding.imgHomeBarMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                connectDrawer(HomeActionBarView.this.drawerLayout, false);
            }
        });
        layoutActionBarHomeBinding.imgHomeBarSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BaseActivity.class);
            }
        });
        if(UserHelper.getUserId() != -1 && !UserHelper.getUserDetails().type.equals(Constants.DEFAULT_DELEGATE)) {
            layoutActionBarHomeBinding.switchAvailable.setVisibility(VISIBLE);
            switchStatus();
        }
    }

    public void switchStatus(){
        if(UserHelper.getUserDetails().status == 2) {
            layoutActionBarHomeBinding.switchAvailable.setChecked(false);
            layoutActionBarHomeBinding.switchAvailable.setText(ResourceManager.getString(R.string.offline));
        }else {
            layoutActionBarHomeBinding.switchAvailable.setChecked(true);
            layoutActionBarHomeBinding.switchAvailable.setText(ResourceManager.getString(R.string.online));
        }
    }

    public void setTitle(String title) {
        layoutActionBarHomeBinding.tvHomeBarText.setText(title);
    }

    public void connectDrawer(DrawerLayout drawerLayout, boolean firstConnect) {

        if (firstConnect) {
            this.drawerLayout = drawerLayout;
            return;
        } else {
            if (drawerLayout.isDrawerOpen(GravityCompat.END))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                drawerLayout.openDrawer(GravityCompat.START);
        }
    }


    public void setScroll(ScrollView svMenu) {
        this.svMenu = svMenu;
    }

    public void setSearchVisibility(String id) {
        if (AppMoon.searchVisibility(id))
            layoutActionBarHomeBinding.imgHomeBarSearch.setVisibility(VISIBLE);
        else
            layoutActionBarHomeBinding.imgHomeBarSearch.setVisibility(GONE);
    }

    public void setNavigation(NavigationDrawerView navigationDrawerView) {
        this.navigationDrawerView = navigationDrawerView;
    }
}
