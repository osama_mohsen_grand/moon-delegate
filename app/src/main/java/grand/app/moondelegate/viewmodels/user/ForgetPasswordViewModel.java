
package grand.app.moondelegate.viewmodels.user;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moondelegate.repository.LoginRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ForgetPasswordViewModel extends PhoneVerificationViewModel {
    public ForgetPasswordRequest changePasswordRequest;
    LoginRepository loginRepository;
    public MutableLiveData<Object> mMutableLiveDataForget  = new MutableLiveData<>();;

    public ForgetPasswordViewModel() {
        changePasswordRequest = new ForgetPasswordRequest(true,"");
        loginRepository = new LoginRepository(mMutableLiveDataForget);
    }

    public void submit(){
        if(changePasswordRequest.validateInput()){
            loginRepository.forgetPassword(changePasswordRequest);
        }
    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
