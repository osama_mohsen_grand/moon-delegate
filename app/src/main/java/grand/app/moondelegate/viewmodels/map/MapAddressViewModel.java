package grand.app.moondelegate.viewmodels.map;

import android.content.Context;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.base.ParentViewModel;


public class MapAddressViewModel extends ParentViewModel {
    public Context context;
    public ObservableField<LatLng> mMapLatLng;
    public static ObservableField<Double> latitude;
    public static ObservableField<Double> longitude;
    public static ObservableField<String> address;
    public static ObservableField<String> arabicAddress;
    public ObservableField<String> placeSelectedObservable;

    public MutableLiveData<Integer> registerLiveData;
    public MutableLiveData<Integer> mutableLiveData;
    public MutableLiveData<Integer> locationLiveData;
    public static double lat = 0,lng = 0;
    public GoogleMap mMap;


    public MapAddressViewModel(Context context) {
        super(context);
        this.context = context;
        mMapLatLng = new ObservableField<>();
        registerLiveData = new MutableLiveData<>();
        locationLiveData = new MutableLiveData<>();
        latitude = new ObservableField<>();
        longitude = new ObservableField<>();
        address = new ObservableField<>();
        placeSelectedObservable = new ObservableField<>();
        arabicAddress = new ObservableField<>();
        mutableLiveData = new MutableLiveData<>();
    }


    public void config(){

    }


    @BindingAdapter("initMap")
    public static void initMap(final MapView mapView, final LatLng latLng) {
//        if (mapView != null) {
//            mapView.onCreate(new Bundle());
//            mapView.getMapAsync(new OnMapReadyCallback() {
//                @Override
//                public void onMapReady(final GoogleMap googleMap) {
//                }
//            });
//        }
    }

    public void confirm(){

    }

    public void reset() {
        unSubscribeFromObservable();
        context = null;
        mMapLatLng.set(null);
        locationLiveData.setValue(null);
        latitude.set(null);
        longitude.set(null);
        address.set(null);
        compositeDisposable = null;
    }

    public void moveToPlace(Place place) {
        placeSelectedObservable.set(place.getAddress().toString());
        if(mMap != null)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15.0f));

    }
}