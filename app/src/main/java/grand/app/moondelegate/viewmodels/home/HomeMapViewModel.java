package grand.app.moondelegate.viewmodels.home;

import com.google.android.gms.location.LocationRequest;

import java.util.Timer;
import java.util.TimerTask;

import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.user.UpdateTokenRequest;
import grand.app.moondelegate.repository.LocationRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.storage.SharedPreferenceHelper;

public class HomeMapViewModel extends ParentViewModel {
    private static final String TAG = "HomeViewModel";
    LocationRepository locationRepository;
    public MapConfig mapConfig;

    public HomeMapViewModel() {
        locationRepository = new LocationRepository(mMutableLiveData);
    }


    public void updateLocation(){
        Timer timer = new Timer();
        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run () {
                locationRepository.updateLocation(new UpdateTokenRequest(),false);
            }
        };

// schedule the task to run starting now and then every hour...
        timer.schedule (hourlyTask, 0l, 1000*10*60);   // 1000*10*60 every 10 minut
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
