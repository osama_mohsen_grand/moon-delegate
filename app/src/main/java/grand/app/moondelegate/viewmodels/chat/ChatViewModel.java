
package grand.app.moondelegate.viewmodels.chat;


import android.util.Log;

import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.chat.ChatRequest;
import grand.app.moondelegate.repository.ChatRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ChatViewModel extends ParentViewModel {

    ChatRepository chatRepository;
    public String text = "";
    public int id,type,receiver_id;
    public boolean allowChat;

    public ChatViewModel(int id, int type, boolean allowChat) {
        chatRepository = new ChatRepository(mMutableLiveData);
        this.id = id;
        this.type = type;
        this.allowChat = allowChat;
//        this.receiver_id = receiver_id;
        Log.d(TAG,this.receiver_id+"");
        chatRepository.getChatDetails(id,type);
    }


    public void setFile(VolleyFileObject volleyFileObject){
        chatRepository.send(new ChatRequest(id,receiver_id,text,type),volleyFileObject);
    }

    private static final String TAG = "ChatViewModel";
    public void sendMessage(){
        if(!text.trim().equals("")) {
//            Log.d(TAG,receiver_id+"");
            chatRepository.send(new ChatRequest(id,receiver_id,text,type),null);
        }
    }

    public ChatRepository getChatRepository() {
        return chatRepository;
    }

    public void camera(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }

    public void setImage(VolleyFileObject volleyFileObject){
        chatRepository.send(new ChatRequest(id,receiver_id,text,type),volleyFileObject);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
