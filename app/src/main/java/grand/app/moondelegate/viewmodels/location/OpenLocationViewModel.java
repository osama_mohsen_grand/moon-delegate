
package grand.app.moondelegate.viewmodels.location;

import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class OpenLocationViewModel extends ParentViewModel {


    public OpenLocationViewModel() {

        notifyChange();
    }

    public void submit() {
        mMutableLiveData.setValue(Constants.LOCATION_ENABLE);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
