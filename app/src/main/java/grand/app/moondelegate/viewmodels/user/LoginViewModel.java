
package grand.app.moondelegate.viewmodels.user;

import androidx.databinding.ObservableField;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.user.login.LoginRequest;
import grand.app.moondelegate.repository.LoginRepository;
import grand.app.moondelegate.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class LoginViewModel extends ParentViewModel {
    public LoginRequest loginRequest ;
    LoginRepository loginRepository;
    public static ObservableField<String> type = new ObservableField<>("");


    public LoginViewModel() {
        loginRequest = new LoginRequest();
        loginRepository = new LoginRepository(mMutableLiveData);
    }

    public void loginSubmit() {
        if(loginRequest.isValid()) {
            loginRepository.loginUser(loginRequest);
        }
    }

    public void signUp() {
//        mMutableLiveData.setValue(Constants.SHOW_PROGRESS);
        mMutableLiveData.setValue(Constants.REGISTRATION);

    }

    public void forgetPassword() {
//        mMutableLiveData.setValue(Constants.SHOW_PROGRESS);
        mMutableLiveData.setValue(Constants.FORGET_PASSWORD);

    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
