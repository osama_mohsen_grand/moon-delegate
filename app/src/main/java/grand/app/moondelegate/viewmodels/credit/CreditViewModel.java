
package grand.app.moondelegate.viewmodels.credit;

import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.credit.CreditResponse;
import grand.app.moondelegate.repository.CreditRepository;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CreditViewModel extends ParentViewModel {

    CreditRepository creditRepository;
    CreditResponse creditResponse = new CreditResponse();
    public CreditViewModel() {
        creditRepository = new CreditRepository(mMutableLiveData);
        creditRepository.getCredit();
    }

    public String getBalance(){
        if(creditResponse != null && creditResponse.data != null)
            return creditResponse.data.balance +" "+ UserHelper.retrieveCurrency();
        return "";
    }

    public CreditResponse getCreditResponse() {
        return creditResponse;
    }

    public void setCreditResponse(CreditResponse creditResponse) {
        this.creditResponse = creditResponse;
    }

    public CreditRepository getCreditRepository() {
        return creditRepository;
    }
}
