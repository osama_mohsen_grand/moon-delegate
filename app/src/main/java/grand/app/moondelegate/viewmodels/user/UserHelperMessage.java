package grand.app.moondelegate.viewmodels.user;

import android.text.InputType;

import grand.app.moondelegate.R;
import grand.app.moondelegate.models.user.profile.ProfileRequest;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;

public class UserHelperMessage {
    public String title= "", message = "",hint = "";
    public int submit = -1;
    public ProfileRequest profileRequest;
    public String type;
    public UserHelperMessage(String type){
        this.type = type;
        init();
    }

    public String init(){
        if(type.equals(Constants.NAME)){
            title = ResourceManager.getString(R.string.update_your_name);
            message = ResourceManager.getString(R.string.your_name_makes_it_easy);
            hint = ResourceManager.getString(R.string.enter_your_full_name);
            submit = InputType.TYPE_CLASS_TEXT;
        }else if(type.equals(Constants.EMAIL)){
            title = ResourceManager.getString(R.string.update_your_email);
            message = ResourceManager.getString(R.string.receive_info_about_new_update);
            hint = ResourceManager.getString(R.string.enter_your_new_email_address);
            submit = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
        }else if(type.equals(Constants.PHONE) || type.equals(Constants.FORGET_PASSWORD)){
            title = ResourceManager.getString(R.string.update_your_mobile_number);
            message = ResourceManager.getString(R.string.we_will_send_a_code);
            hint = ResourceManager.getString(R.string.enter_the_phone_number);
            submit = InputType.TYPE_CLASS_PHONE;
        }

        return title;
    }


}
