
package grand.app.moondelegate.viewmodels.app;

import grand.app.moondelegate.base.ParentViewModel;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ZoomViewModel extends ParentViewModel {

    public String image;
    public ZoomViewModel(String image) {
        this.image = image;
    }

    public String getZoomImageUrl() {
        return image;
    }


}
