
package grand.app.moondelegate.viewmodels.home;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.repository.NotificationRepository;
import grand.app.moondelegate.repository.OrderDelegateRepository;
import grand.app.moondelegate.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class HomeDelegateViewModel extends ParentViewModel {

    OrderDelegateRepository orderDelegateRepository;
    public String noOrders = ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.orders);
    public HomeDelegateViewModel(int status) {
        orderDelegateRepository = new OrderDelegateRepository(mMutableLiveData);
        orderDelegateRepository.getOrders(status);
    }

    public OrderDelegateRepository getOrderDelegateRepository() {
        return orderDelegateRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
