
package grand.app.moondelegate.viewmodels.app;

import androidx.databinding.ObservableBoolean;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.repository.SettingsRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class SettingsViewModel extends ParentViewModel {

    public ObservableBoolean visible = new ObservableBoolean(false);
    private SettingsRepository settingsRepository;
    public String text = "";

    /* if type = 1 link terms and  if type = 2 link about us */

    public SettingsViewModel(int type) {
        settingsRepository = new SettingsRepository(mMutableLiveData);
        settingsRepository.getSettings(type);
    }


    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
