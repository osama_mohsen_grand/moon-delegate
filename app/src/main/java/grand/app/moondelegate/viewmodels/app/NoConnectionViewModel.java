
package grand.app.moondelegate.viewmodels.app;


import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.utils.AppUtils;
import grand.app.moondelegate.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class NoConnectionViewModel extends ParentViewModel {

    public void reload() {
        if (AppUtils.isNetworkAvailable()) {
            mMutableLiveData.setValue(Constants.RELOAD);
        } else {
            mMutableLiveData.setValue(Constants.FAILURE_CONNECTION);
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
