
package grand.app.moondelegate.viewmodels.user;

import com.hbb20.CountryCodePicker;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moondelegate.repository.LoginRepository;
import grand.app.moondelegate.repository.VerificationFirebaseSMSRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.Validate;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class PhoneVerificationViewModel extends ParentViewModel {
    VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    public MutableLiveData<Object> mMutableLiveDataLogin  = new MutableLiveData<>();;
    LoginRepository loginRepository;

    public String cpp="";
    private String phone = "";
    public String phoneCpp = "";
    public ObservableField phoneError;


    public PhoneVerificationViewModel() {
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        loginRepository = new LoginRepository(mMutableLiveDataLogin);
        phoneError = new ObservableField();
    }

    public String getCpp() {
        return cpp;
    }


    public static void setCpp(CountryCodePicker countryCodePicker, String text) {
        countryCodePicker.setCountryForNameCode(text);
    }

    private boolean isValid(){
        boolean valid = true;
        if(!Validate.isValid(cpp+phone, Constants.PHONE)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("phone:error");
        }
        return valid;
    }

    public void verificationSubmit(){
        if(isValid()){
            phoneCpp = cpp+phone;
            Timber.e("phone:"+phoneCpp);
            loginRepository.checkPhone(new ForgetPasswordRequest(false,phoneCpp));
        }
    }

    public void sendCode(){
        verificationFirebaseSMSRepository.sendVerificationCode(phoneCpp);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
