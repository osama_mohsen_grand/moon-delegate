package grand.app.moondelegate.viewmodels.chat;

import android.util.LayoutDirection;
import android.widget.RelativeLayout;

import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.models.chat.ChatDetailsModel;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import timber.log.Timber;


public class ItemChatViewModel extends ParentViewModel {

    public ChatDetailsModel chat;
    public int position;
    public ObservableBoolean isMessage = new ObservableBoolean(false);
    public ObservableBoolean isImage = new ObservableBoolean(false);
    public ObservableBoolean isAudio = new ObservableBoolean(false);
    public int layout;
    public String userName = "";
//
//    public Drawable backgroundColor;
//
    public ItemChatViewModel(ChatDetailsModel chat , int position) {
        this.chat = chat;
        this.position = position;
        if(chat.type.equals(AppMoon.getUserType())) {
            layout = LayoutDirection.RTL;
            userName = UserHelper.getUserDetails().name;
        }else {
            layout = LayoutDirection.LTR;
            userName = chat.senderName;
        }


        if(!chat.message.equals(""))
            isMessage.set(true);
        if(!chat.image.equals(""))
            isImage.set(true);
        if(!chat.audio.equals(""))
            isAudio.set(true);

        Timber.e("==");
        Timber.e("message:"+chat.message);
        Timber.e("image:"+chat.image);
        Timber.e("audio:"+chat.audio);
        Timber.e("==");

        notifyChange();
    }

    public String imageUrl(){
        if(chat.type.equals(AppMoon.getUserType()))
            return UserHelper.getUserDetails().image;
        return chat.senderImage;
    }

    public int getLayout(){
        return layout;
    }

    @BindingAdapter("layout")
    public static void setLayout(RelativeLayout linearLayout, int layout){
        linearLayout.setLayoutDirection(layout);
    }

    public String getAudio(){
        return chat.audio;
    }

    @BindingAdapter("audio")
    public static void setAudio(AudioSenseiPlayerView audio, String url){
        audio.setAudioTarget(url);
    }


//    @Bindable
//    public Drawable getBackgroundColor() {
//        return backgroundColor;
//    }
//
//    @BindingAdapter("bgColor")
//    public static void setBackgroundColor(LinearLayout linearLayout , Drawable backgroundColor) {
//        linearLayout.setBackground(backgroundColor);
//    }
//
//
//    public int getLayout(){
//        return layout;
//    }
//
//    @BindingAdapter("layout")
//    public static void setLayout(LinearLayout linearLayout,int layout){
//        linearLayout.setLayoutDirection(layout);
//    }
//
//    @Bindable
//    public String getImageUrl(){
//        return chat.userImage;
//    }
//
//    @BindingAdapter("imageUrl")
//    public static void loadImage(ImageView imageView, String image) {
//        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
//    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
