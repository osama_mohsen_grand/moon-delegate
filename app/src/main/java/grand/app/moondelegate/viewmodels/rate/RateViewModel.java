
package grand.app.moondelegate.viewmodels.rate;

import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.driver.current.TripCurrent;
import grand.app.moondelegate.models.rate.RateRequest;
import grand.app.moondelegate.repository.TripRepository;
import grand.app.moondelegate.utils.images.ImageLoaderHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class RateViewModel extends ParentViewModel {
    public RateRequest rateRequest ;
    public TripCurrent tripCurrent;
    public ObservableInt rating = new ObservableInt(0);
    TripRepository tripRepository;
    public String total = "";
    //tripDetailsResponse.data.user.name

    public RateViewModel(TripCurrent tripCurrent, String total) {
        tripRepository = new TripRepository(mMutableLiveData);
        this.tripCurrent = tripCurrent;
        rateRequest = new RateRequest(tripCurrent.id,0);
        this.total = ResourceManager.getString(R.string.price_is)+" "+total +" "+ UserHelper.retrieveCurrency();
        notifyChange();
    }

    public void showPayment(){
        notifyChange();
    }

    public void sendSubmit() {
        tripRepository.rateTrip(rateRequest);
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        rateRequest.rate = rating;
        ratingBar.setRating(rating);
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
    }


    public TripRepository getTripRepository() {
        return tripRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
