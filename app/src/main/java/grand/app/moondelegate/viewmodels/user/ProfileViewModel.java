
package grand.app.moondelegate.viewmodels.user;

import android.widget.ImageView;

import java.util.ArrayList;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.user.profile.ProfileRequest;
import grand.app.moondelegate.models.user.profile.ProfileResponse;
import grand.app.moondelegate.models.user.profile.User;
import grand.app.moondelegate.repository.LoginRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.images.ImageLoaderHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.VolleyFileObject;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ProfileViewModel extends ParentViewModel {

    LoginRepository loginRepository;
    public ProfileRequest profileRequest = new ProfileRequest();
    public StatusMsg statusMsg = null;


    public ProfileViewModel() {
        loginRepository = new LoginRepository(mMutableLiveData);
    }


    public void submit(){
        if(profileRequest.isValid() ){
            if(profileRequest.volleyFileObject == null)
                loginRepository.updateProfile(profileRequest);
            else
                loginRepository.updateProfile(profileRequest,profileRequest.volleyFileObject);
        }
    }

    public String getImageUrl(){
        return UserHelper.getUserDetails().image;
    }

    public void selectImage(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(VolleyFileObject volleyFileObject) {
        profileRequest.volleyFileObject = volleyFileObject;
    }

    public void changePassword(){
        mMutableLiveData.setValue(Constants.CHANGE_PASSWORD);
    }

    public void addressSubmit(){
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public void changeLanguageSubmit(){
        mMutableLiveData.setValue(Constants.LANGUAGE);
    }

    public void changeCountrySubmit(){
        mMutableLiveData.setValue(Constants.COUNTRIES);
    }


    public void setAddress(double lat, double lng) {
        profileRequest.setLat(lat);
        profileRequest.setLng(lng);
        profileRequest.setAddress(ResourceManager.getString(R.string.done_selected_address));
        notifyChange();
    }


    public LoginRepository getLoginRepository() {
        return loginRepository;
    }
}
