package grand.app.moondelegate.viewmodels.order.delegate.details;


import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.order.OrderConfirmRequest;
import grand.app.moondelegate.repository.OrderDelegateRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;

public class OrderDetailsDelegateViewModel extends ParentViewModel {
    private String subTotal = "", delivery = "", total = "",imageUrl = "",imageShopUrl = "",name="";
    int order_id ;
    private OrderDelegateRepository orderRepository;
    public boolean haveDelegate = false;
    public boolean showConfirmCancel = false,showStatus = false,allowChat = false;
    public OrderConfirmRequest orderConfirmRequest;

    public OrderDetailsDelegateViewModel(int order_id, int order_status_id) {
        orderRepository = new OrderDelegateRepository(mMutableLiveData);
        orderConfirmRequest = new OrderConfirmRequest(order_id);
        this.order_id = order_id;
        if(order_status_id == 1)
            showConfirmCancel = true;
        orderRepository.getOrderDetails(order_id);
    }

    public String getSubTotal() {
        if (orderRepository.getOrderDetailsResponse() != null)
            subTotal = orderRepository.getOrderDetailsResponse().data.subTotal + " " + UserHelper.retrieveCurrency();
        return subTotal;
    }


    public String getImageUrl(){
        if (orderRepository.getOrderDetailsResponse() != null && haveDelegate)
            imageUrl = orderRepository.getOrderDetailsResponse().data.delegate.image;
        return imageUrl;
    }

    public String getImageShopUrl(){
        if (orderRepository.getOrderDetailsResponse() != null && haveDelegate)
            imageUrl = orderRepository.getOrderDetailsResponse().data.shop.image;
        return imageUrl;
    }

    public String getName(){
        if (orderRepository.getOrderDetailsResponse() != null && haveDelegate)
            name = orderRepository.getOrderDetailsResponse().data.delegate.name;
        return name;
    }

    public String getDelivery() {
        if (orderRepository.getOrderDetailsResponse() != null)
            delivery = orderRepository.getOrderDetailsResponse().data.delivery + " " + UserHelper.retrieveCurrency();
        return delivery;
    }

    public String getTotal() {
        if (orderRepository.getOrderDetailsResponse() != null)
            total = orderRepository.getOrderDetailsResponse().data.totalPrice + " " + UserHelper.retrieveCurrency();
        return total;
    }

    public void chatSubmit(){
        mMutableLiveData.setValue(Constants.CHAT);
    }

    //1: reject , 2:accept
    public void orderSubmit(int status){
        if(status != 0) {
            orderConfirmRequest.status = status;
        }
        orderRepository.confirmOrder(orderConfirmRequest);
    }

    public void shopLocation(){
        mMutableLiveData.setValue(Constants.SHOP_LOCATION);
    }

    public void delegateLocation(){
        mMutableLiveData.setValue(Constants.DELEGATE_LOCATION);
    }

    public OrderDelegateRepository getOrderRepository() {
        return orderRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setStatusChat() {
        if(orderRepository.getOrderDetailsResponse().data.statusId < 2){
            allowChat = false;
        }else{
            allowChat = true;
        }
    }
}
