package grand.app.moondelegate.viewmodels.packagePayment;


import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.packagePayment.Datum;

public class ItemPackageViewModel extends ParentViewModel {
    public Datum _package = null;
    public int position = 0;

    public ItemPackageViewModel(Datum _package, int position) {
        this._package = _package;
        this.position = position;
    }

    public void _packageSubmit(){
        mMutableLiveData.setValue(position);
    }
}
