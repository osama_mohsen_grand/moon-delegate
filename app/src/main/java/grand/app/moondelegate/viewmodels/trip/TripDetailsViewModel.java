package grand.app.moondelegate.viewmodels.trip;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.trip.TripHistoryResponse;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;


public class TripDetailsViewModel extends ParentViewModel {
    public TripHistoryResponse.Datum trip = null;
    public String price,dateTime,carColorModel;

    public TripDetailsViewModel(TripHistoryResponse.Datum trip) {
        this.trip = trip;
        price = trip.price + UserHelper.retrieveCurrency();
        if(trip.dateSchedule != null && trip.timeSchedule != null){
            dateTime = trip.dateSchedule+" , "+trip.timeSchedule;
        }else
            dateTime = trip.tripDate;

        carColorModel = trip.car_name+" , "+trip.car_model;
    }

    @Bindable
    public Float getRating(){
        return trip.rate;
    }


    public static void setRating(RatingBar rating , Float rate){
        rating.setRating(rate);
    }
}
