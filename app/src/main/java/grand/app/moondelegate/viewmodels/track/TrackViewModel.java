
package grand.app.moondelegate.viewmodels.track;

import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.driver.TripActionRequest;
import grand.app.moondelegate.models.driver.current.TripCurrent;
import grand.app.moondelegate.repository.TripRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.images.ImageLoaderHelper;
import grand.app.moondelegate.utils.maputils.base.MapConfig;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.location.LocationHelper;
import grand.app.moondelegate.utils.storage.user.UserHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class TrackViewModel extends ParentViewModel {

    public TripRepository tripRepository;
    public TripCurrent trip;
    public ObservableField userSubmitText = new ObservableField(ResourceManager.getString(R.string.arrive));
    private static final String TAG = "TrackViewModel";
    public ObservableBoolean showDriverInfoDialog = new ObservableBoolean(false);
    public ObservableBoolean showDistanceDialog = new ObservableBoolean(false);
    public ObservableBoolean showUserInfo = new ObservableBoolean(false);
    public ObservableBoolean showDialogStart = new ObservableBoolean(false);
    public ObservableBoolean showDialogFinish = new ObservableBoolean(false);

    public String type;
    public ObservableBoolean acceptInfo = new ObservableBoolean(false);
    public int submitAction = -1;
    public com.google.android.gms.maps.model.LatLng latLng = null;
    public String distance_time_text = "", price = "", distance = "", time = "";


    public MapConfig mapConfig;

    public TrackViewModel() {
        tripRepository = new TripRepository(mMutableLiveData);
    }

    public void setTrip(TripCurrent trip) {
        this.trip = trip;
        price = trip.price + " " + UserHelper.retrieveCurrency();
        distance = trip.distance + " " + ResourceManager.getString(R.string.kilo_meter);
        time = trip.trip_time + " " + ResourceManager.getString(R.string.min);
    }


    public void showDialogDriver() {
        showDriverInfoDialog.set(true);
        notifyChange();
    }

    public void showDistanceDialog() {
        showDriverInfoDialog.set(false);
        showDistanceDialog.set(true);
        notifyChange();
    }


    //show acceptance dialog
    public void showDialogAcceptance() {
        Log.e(TAG, "showDialogAcceptance: Done");
        acceptInfo.set(true);
//        mapConfig.setPaddingRunTime(0, 0, 20, 450);
        notifyChange();
    }

    @BindingAdapter("userImageUrl")
    public static void loadImageUser(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(), image, imageView);
    }

    @BindingAdapter("carImageUrl")
    public static void loadImageCar(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(), image, imageView);
    }

    @BindingAdapter("rating")
    public static void setRating(RatingBar rating, Float rate) {
        rating.setRating(rate);
    }

    //1=>accept , 2=>reject  , 3=>arrive  , 4=>start  , 5=> cancel after accept ,  6=>finished trip
    public void orderAction(int submit) {
        this.submitAction = submit;
        if (submit == 1) {//accept order
            tripRepository.tripAction(new TripActionRequest(trip.id, submit));
        } else if (submit == 2) {//reject order
            tripRepository.tripAction(new TripActionRequest(trip.id, submit));
        } else if (submit == 3) {//arrive location
            tripRepository.tripAction(new TripActionRequest(trip.id, submit));
        } else if (submit == 4) {//start trip
            tripRepository.tripAction(new TripActionRequest(trip.id, submit));
        } else if (submit == 5) {//cancel after accept
            tripRepository.tripAction(new TripActionRequest(trip.id, submit));
        } else if (submit == 6) {//finish trip
            TripActionRequest tripActionRequest = new TripActionRequest(trip.id, submit);
            tripActionRequest.locations = LocationHelper.getLocations();
            tripRepository.tripAction(tripActionRequest);
        }
    }

    //show dialog user info
    public void showDialogUserInfo(boolean isAnimation) {
        acceptInfo.set(false);
        showDialogFinish.set(false);
        showDialogStart.set(false);
        showUserInfo.set(true);
        if (isAnimation)
            mMutableLiveData.setValue(Constants.HIDE_NEW_ORDER);//start animation for show user data after accept
        mapConfig.getGoogleMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mapConfig.navigation(marker.getPosition().latitude, marker.getPosition().longitude);
                return false;
            }
        });
        mapConfig.setPaddingRunTime(0, 0, 20, 400);
        notifyChange();
    }

    public void showDialogStartTrip(boolean isAnimation) {
        acceptInfo.set(false);
        showUserInfo.set(false);
        showDialogFinish.set(false);
        showDialogStart.set(true);
        if (isAnimation)
            mMutableLiveData.setValue(Constants.HIDE_NEW_ORDER);//start animation for show user data after accept
        mapConfig.getGoogleMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mapConfig.navigation(marker.getPosition().latitude, marker.getPosition().longitude);
                return false;
            }
        });
        mapConfig.setPaddingRunTime(0, 0, 20, 400);
        notifyChange();
    }

    public TripRepository getTripRepository() {
        return tripRepository;
    }


    public void callSubmit() {
        Log.e(TAG, "callSubmit: call");
        mMutableLiveData.setValue(Constants.CALL);
    }


    public void chatSubmit() {
        mMutableLiveData.setValue(Constants.CHAT);
    }

    public void showDialogFinishTrip(boolean isAnimation) {
        acceptInfo.set(false);
        showDialogStart.set(false);
        showUserInfo.set(false);
        showDialogFinish.set(true);

        if (isAnimation)
            mMutableLiveData.setValue(Constants.HIDE_NEW_ORDER);//start animation for show user data after accept
        mapConfig.getGoogleMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mapConfig.navigation(marker.getPosition().latitude, marker.getPosition().longitude);
                return false;
            }
        });
        mapConfig.setPaddingRunTime(0, 0, 20, 400);
        notifyChange();
    }

    public void setOrderId(int order_id) {
        tripRepository.getTripDetails(order_id);
    }
}
