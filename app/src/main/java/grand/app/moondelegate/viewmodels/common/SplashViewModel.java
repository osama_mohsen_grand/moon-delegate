
package grand.app.moondelegate.viewmodels.common;

import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.repository.CountryRepository;
import grand.app.moondelegate.repository.TripRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class SplashViewModel extends ParentViewModel {

    private TripRepository tripRepository;
    public SplashViewModel() {
        tripRepository = new TripRepository(mMutableLiveData);
        tripRepository.getCurrentTrip(false);

    }

    public TripRepository getTripRepository() {
        return tripRepository;
    }

    public boolean isDelegate() {
        boolean isDelegate = true;
        if(UserHelper.getUserId() != -1 &&  ( UserHelper.getUserDetails().type.equals(Constants.DEFAULT_TRANSPORTATION) || UserHelper.getUserDetails().type.equals(Constants.DEFAULT_TAXI))){
            isDelegate = false;
        }
        return isDelegate;
    }
}
