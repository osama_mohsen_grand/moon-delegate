
package grand.app.moondelegate.viewmodels.trip;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.repository.TripRepository;
import grand.app.moondelegate.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class TripHistoryViewModel extends ParentViewModel {

    TripRepository tripHistoryRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.ride_history));
    public ObservableBoolean noData = new ObservableBoolean(false);
    public TripHistoryViewModel() {
        tripHistoryRepository = new TripRepository(mMutableLiveData);
        tripHistoryRepository.getTrips();
    }

    public TripRepository getTripRepository() {
        return tripHistoryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
