
package grand.app.moondelegate.viewmodels.user;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.user.register.RegisterRequest;
import grand.app.moondelegate.models.user.register.VolleyFileObjectSerializable;
import grand.app.moondelegate.repository.CountryRepository;
import grand.app.moondelegate.repository.RegisterRepository;
import grand.app.moondelegate.repository.ServiceRepository;
import grand.app.moondelegate.repository.VerificationFirebaseSMSRepository;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class RegisterViewModel extends ParentViewModel {
    private VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    public RegisterRequest registerShopRequest = new RegisterRequest();
    public MutableLiveData<Object> mMutableLiveDataCountry  = new MutableLiveData<>();;
    RegisterRepository registerRepository;
    CountryRepository countryRepository;
    public StatusMsg statusMsg = null;
    public String image_select = "";
    boolean[] images = {false, false, false, false, false,false};
    public VolleyFileObjectSerializable volleyFileObjectSerializable;
    public static ObservableField<String> type = new ObservableField<>("");
    public ArrayList<String> imagesPath = new ArrayList<>();
    public ArrayList<String> imagesKey = new ArrayList<>();
    public ObservableBoolean isDriver = new ObservableBoolean(false);


    public RegisterViewModel(String phone) {
        volleyFileObjectSerializable = new VolleyFileObjectSerializable();
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        registerShopRequest.setPhone(phone);
        registerRepository = new RegisterRepository(mMutableLiveData);
        countryRepository = new CountryRepository(mMutableLiveDataCountry);
        countryRepository.getCountries(true);
    }

    public void color(){
        mMutableLiveData.setValue(Constants.COLOR);
    }

    public RegisterRepository getRegisterRepository() {
        return registerRepository;
    }


    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    public void signUp() {
        if (registerShopRequest.isValid() && images[0] && images[1] && images[2] && images[3] && images[4]) {
            if((isDriver.get() && images[5] )|| !isDriver.get()) {
                Iterator myVeryOwnIterator = volleyFileObjectSerializable.volleyFileObjectMap.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    imagesKey.add(key);
                    imagesPath.add(volleyFileObjectSerializable.volleyFileObjectMap.get(key).getFilePath());
                }
                registerRepository.registerUser(registerShopRequest, imagesKey, imagesPath);
            }else{
                completeFormMsg();
            }
        } else if (!images[0]) {
            baseError = ResourceManager.getString(R.string.select_image_profile);
            mMutableLiveData.setValue(Constants.ERROR);
        } else {
            completeFormMsg();
        }
    }

    public void completeFormMsg(){
        baseError = ResourceManager.getString(R.string.please_complete_form);
        mMutableLiveData.setValue(Constants.ERROR);
    }

    public void selectImage(String image) {
        image_select = image;
        if (image_select.equals("image") || image_select.equals("car_image")) {
            mMutableLiveData.setValue(Constants.SELECT_IMAGE);
        } else {
            mMutableLiveData.setValue(Constants.SELECT_IMAGE_MULTIPLE);
        }
    }


    public void terms() {
        mMutableLiveData.setValue(Constants.TERMS);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    /*
    image (1)           => 0
    ID_image (2)        => 1,2
    license_image (2)   => 3,4
    car_image (1)       => 5
     */

    public void setImage(VolleyFileObject volleyFileObject) {
        volleyFileObjectSerializable.volleyFileObjectMap.put(image_select, volleyFileObject);
        if (image_select.equals("image")) {
            //Done Select ImageVideo
            images[0] = true;
        } else if (image_select.equals(Constants.ID_IMAGE)) {
            //Done Select image
            images[1] = true;
            images[2] = true;
        } else if (image_select.equals("license_image")) {
            //Done select licence
            images[3] = true;
            images[4] = true;
            notifyChange();
        }else if(image_select.equals("car_image")){
            images[5] = true;
            notifyChange();
        }
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }


    public void setAddress(double lat, double lng,String address) {
        registerShopRequest.setLat(lat);
        registerShopRequest.setLng(lng);
        registerShopRequest.setAddress(address);
        notifyChange();
    }

    public void updateType() {
        if(registerShopRequest.getType().equals(Constants.DEFAULT_DELEGATE)){
            isDriver.set(false);
        }else
            isDriver.set(true);
        notifyChange();
    }
}
