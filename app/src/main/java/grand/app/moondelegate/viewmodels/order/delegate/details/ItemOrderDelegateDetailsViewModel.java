package grand.app.moondelegate.viewmodels.order.delegate.details;

import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.order.details.OrderProductDelegateDetails;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;


public class ItemOrderDelegateDetailsViewModel extends ParentViewModel {
    public OrderProductDelegateDetails order = null;
    private int position = 0;
    public String price,price_addition,count;
    public boolean haveAddition = false,haveColor = false,haveSize=false,haveSpecialRequest=false;

    public ItemOrderDelegateDetailsViewModel(OrderProductDelegateDetails order, int position) {
        this.order = order;
        this.position = position;
        this.price = order.price+" "+ UserHelper.retrieveCurrency();
        this.count = order.qty+"x";
        if( order.additions != null && !order.additions.equals("")) {
            haveAddition = true;
            price_addition = order.price_addition +" "+UserHelper.retrieveCurrency();
        }
        if(order.color != null && !order.color.equals("")){
            haveColor = true;
        }
        if(order.size != null && !order.size.equals("")){
            haveSize = true;
        }
        if(order.specialRequest!=null && !order.specialRequest.trim().equals("")){
            haveSpecialRequest = true;
        }
    }


    public void submit(){
        mMutableLiveDataBaseObservable.setValue(position);
    }


}
