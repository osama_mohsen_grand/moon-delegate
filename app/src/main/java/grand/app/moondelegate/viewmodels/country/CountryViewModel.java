
package grand.app.moondelegate.viewmodels.country;

import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.repository.CountryRepository;
import grand.app.moondelegate.utils.Constants;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CountryViewModel extends ParentViewModel {

    private CountryRepository countryRepository;

    public CountryViewModel() {
        countryRepository = new CountryRepository(mMutableLiveData);
        countryRepository.getCountries(true);
    }


    public void submit() {
        Timber.e("submit");
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
