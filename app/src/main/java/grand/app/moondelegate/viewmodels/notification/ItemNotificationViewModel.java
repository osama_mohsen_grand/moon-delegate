package grand.app.moondelegate.viewmodels.notification;


import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.notifications.Notification;

public class ItemNotificationViewModel extends ParentViewModel {
    public Notification notification = null;
    public String image;
    public int position = 0;

    public ItemNotificationViewModel(Notification notification, int position) {
        this.notification = notification;
        this.position = position;
        notifyChange();
    }

    public void notificationSubmit(){
        mMutableLiveData.setValue(position);
    }
}
