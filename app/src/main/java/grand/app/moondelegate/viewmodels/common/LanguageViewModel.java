
package grand.app.moondelegate.viewmodels.common;

import android.content.Context;
import android.view.View;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class LanguageViewModel extends ParentViewModel {
    public String language_select ;
    public boolean language_ar=false,language_en=false;

    private Context context;
    public LanguageViewModel(Context context) {
        super(context);
        this.context = context;
        currentLanguage();
    }

    private void currentLanguage() {
        String lang = LanguagesHelper.getCurrentLanguage();
        if(lang.equals("ar"))
            language_ar = true;
        else
            language_en = true;
        language_select = lang;
        notifyChange();
    }


    public void onSplitTypeChanged(RadioGroup radioButtonGroup, int id) {
        int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
        View radioButton = radioButtonGroup.findViewById(radioButtonID);
        int idx = radioButtonGroup.indexOfChild(radioButton);
        if(idx == 0)
            language_select = "ar";
        else
            language_select = "en";
    }


    @Bindable
    public boolean isLanguage_ar() {
        return language_ar;
    }

    public void setLanguage_ar(boolean language_ar) {
        this.language_ar = language_ar;
        language_select = "ar";
        notifyChange();
    }

    @Bindable
    public boolean isLanguage_en() {
        return language_en;
    }

    public void setLanguage_en(boolean language_en) {
        this.language_en = language_en;
        language_select = "en";
        notifyChange();

    }

    public void cancelSubmit(){
        mMutableLiveData.setValue(Constants.LANGUAGE_CANCEL);
    }

    public void okSubmit(){
        mMutableLiveData.setValue(Constants.LANGUAGE_OK);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

}
