
package grand.app.moondelegate.viewmodels.language;

import grand.app.moondelegate.R;
import grand.app.moondelegate.base.IAnimationSubmit;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class LanguageViewModel extends ParentViewModel implements IAnimationSubmit {

    public int select = -1;

    public String error = "";

    public void onTypeChecked(int i) {
        // if it is a check. set the type
        select = i;
        notifyChange();
    }

    public LanguageViewModel getAnimationSubmit(){
        return this;
    }

    public String getLanguage() {
        String lng = "";
        if (select == -1) {
            lng = "";
        } else {
            if (select == 1) {
                lng = Constants.LANGUAGE_EN;
            } else if (select == 2) {
                lng = Constants.LANGUAGE_AR;
            } else if (select == 3) {
                lng = Constants.LANGUAGE_FR;
            } else if (select == 4) {
                lng = Constants.LANGUAGE_GR;
            } else if (select == 5) {
                lng = Constants.LANGUAGE_IT;
            } else if (select == 6) {
                lng = Constants.LANGUAGE_HY;

            } else if (select == 7) {
                lng = Constants.LANGUAGE_CS;

            } else if (select == 8) {
                lng = Constants.LANGUAGE_HI;

            } else if (select == 9) {
                lng = Constants.LANGUAGE_KO;

            } else if (select == 10) {
                lng = Constants.LANGUAGE_PL;

            } else if (select == 11) {
                lng = Constants.LANGUAGE_TR;

            } else if (select == 12) {
                lng = Constants.LANGUAGE_RU;

            } else if (select == 13) {
                lng = Constants.LANGUAGE_IND;

            } else if (select == 14) {
                lng = Constants.LANGUAGE_BN;

            } else if (select == 15) {
                lng = Constants.LANGUAGE_TH;

            } else if (select == 16) {
                lng = Constants.LANGUAGE_FIL;

            }
        }
        return lng;
    }



    @Override
    public void animationSubmit() {
        if(!getLanguage().equals("")){
            LanguagesHelper.setLanguage(getLanguage());
            mMutableLiveData.setValue(Constants.LANGUAGE);
        }else {
            error = ResourceManager.getString(R.string.please_choose_your_language);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
