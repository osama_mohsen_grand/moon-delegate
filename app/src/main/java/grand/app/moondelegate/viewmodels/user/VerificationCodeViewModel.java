package grand.app.moondelegate.viewmodels.user;

import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.user.activation.VerificationRequest;
import grand.app.moondelegate.repository.RegisterRepository;
import grand.app.moondelegate.repository.VerificationFirebaseSMSRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class VerificationCodeViewModel extends ParentViewModel {

    public VerificationRequest activeRequest = new VerificationRequest();
    String verify_id;
    VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    RegisterRepository registerRepository;
    public VerificationCodeViewModel(String verify_id) {
        this.verify_id = verify_id;
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        registerRepository = new RegisterRepository(mMutableLiveData);
    }

    public void verificationSubmit(){
        if(activeRequest.isValid()) {
            verificationFirebaseSMSRepository.verifyCode(verify_id,activeRequest.code);
        }
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }

    public RegisterRepository getRegisterRepository() {
        return registerRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}