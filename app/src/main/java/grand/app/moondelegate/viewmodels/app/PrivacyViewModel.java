
package grand.app.moondelegate.viewmodels.app;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.repository.PrivacyRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class PrivacyViewModel extends ParentViewModel {

    public ObservableBoolean visible = new ObservableBoolean(false);
    PrivacyRepository privacyRepository;
    String text = "";

    public PrivacyViewModel() {
        privacyRepository = new PrivacyRepository(mMutableLiveData);
    }


    @Bindable
    public String getText() {
        return text;
    }

    public void setText(String text){
        this.text = text;
    }

    public void setText() {
        this.text = getPrivacyRepository().getPrivacyResponse().data.translation.name;
        setText(this.text);
        visible.set(true);
        notifyChange();
    }

    public PrivacyRepository getPrivacyRepository() {
        return privacyRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
