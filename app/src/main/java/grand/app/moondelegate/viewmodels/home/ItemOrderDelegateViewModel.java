package grand.app.moondelegate.viewmodels.home;

import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.order.OrderDelegate;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;


public class ItemOrderDelegateViewModel extends ParentViewModel {
    public OrderDelegate orderDelegate = null;
    private int position = 0;
    public String price = "";
    public ItemOrderDelegateViewModel(OrderDelegate orderDelegate, int position) {
        this.orderDelegate = orderDelegate;
        price = orderDelegate.cost+" "+ UserHelper.retrieveCurrency();
        this.position = position;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
