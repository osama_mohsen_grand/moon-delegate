package grand.app.moondelegate.viewmodels.trip;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import grand.app.moondelegate.R;
import grand.app.moondelegate.base.ParentViewModel;
import grand.app.moondelegate.models.trip.TripHistoryResponse;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.user.UserHelper;


public class ItemTripHistoryViewModel extends ParentViewModel {
    public TripHistoryResponse.Datum trips = null;
    public String image;
    public int position = 0;
    public String price;

    public ItemTripHistoryViewModel(TripHistoryResponse.Datum trips, int position) {
        this.trips = trips;
        this.position = position;
        this.price = trips.price+ UserHelper.retrieveCurrency();
        notifyChange();
    }

    @Bindable
    public Float getRating(){
        return trips.rate;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public static void setRating(RatingBar rating , Float rate){
        rating.setRating(rate);
    }
}
