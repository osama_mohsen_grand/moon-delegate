package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.driver.TripActionRequest;
import grand.app.moondelegate.models.driver.current.TripCurrentResponse;
import grand.app.moondelegate.models.location.UpdateLocation;
import grand.app.moondelegate.models.rate.RateRequest;
import grand.app.moondelegate.models.trip.TripActionResponse;
import grand.app.moondelegate.models.trip.TripHistoryResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.location.LocationHelper;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;
import timber.log.Timber;

public class TripRepository extends BaseRepository {
    TripCurrentResponse tripCurrentResponse = null;
    TripHistoryResponse tripHistoryResponse = null;
    StatusMsg statusMsg = null;
    TripActionResponse tripActionResponse = null;
    public TripRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getCurrentTrip(boolean isLoadProgress) {
        if(isLoadProgress)
            getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    if(isLoadProgress)
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tripCurrentResponse = (TripCurrentResponse) response;
                    if (tripCurrentResponse != null) {
                        setMessage(tripCurrentResponse.status,tripCurrentResponse.msg);
                        if (tripCurrentResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.TRIP);
                        }else {
                            if(isLoadProgress)
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.DRIVER_HOME+"?type="+UserHelper.getUserDetails().type, null, TripCurrentResponse.class);
    }

    public void tripAction(TripActionRequest tripActionRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tripActionResponse = (TripActionResponse) response;
                    if (tripActionResponse != null) {
                        setMessage(tripActionResponse.status, tripActionResponse.msg);
                        if (tripActionResponse.status == Constants.RESPONSE_SUCCESS) {
                            if(tripActionRequest.status == 6) LocationHelper.clear();
                            getmMutableLiveData().setValue(Constants.TRIP_ACTION);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(tripActionResponse.status, tripActionResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.TRIP_ACTION, tripActionRequest, TripActionResponse.class);
    }

    public void getTrips() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tripHistoryResponse = (TripHistoryResponse) response;
                    if (tripHistoryResponse != null) {
                        setMessage(tripHistoryResponse.status, tripHistoryResponse.msg);
                        if (tripHistoryResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.HISTORY);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.TRIP_HISTORY+"?type="+UserHelper.getUserDetails().type, null, TripHistoryResponse.class);
    }

    public TripHistoryResponse getTripHistory() {
        return tripHistoryResponse;
    }


    public void getTripDetails(int trip_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tripCurrentResponse = (TripCurrentResponse) response;
                    if (tripCurrentResponse != null) {
                        setMessage(tripCurrentResponse.status, tripCurrentResponse.msg);
                        if (tripCurrentResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.TRIP_DETAILS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(tripCurrentResponse.status, tripCurrentResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.TRIP_DETAILS+"?type="+ UserHelper.getUserDetails().type+"&id="+trip_id, null, TripCurrentResponse.class);
    }

    public TripCurrentResponse getTripCurrentResponse() {
        return tripCurrentResponse;
    }

    public void updateLocations(UpdateLocation updateLocation) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        Timber.e("LocationBackground:Done");
                        setMessage(statusMsg.status, statusMsg.msg);
                    }
                }
            }
        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_LOCATION, updateLocation, StatusMsg.class);
    }

    public TripActionResponse getTripActionResponse() {
        return tripActionResponse;
    }

    public void rateTrip(RateRequest rateRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.RATE);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.POST, URLS.RATE, rateRequest, StatusMsg.class);
    }
}



