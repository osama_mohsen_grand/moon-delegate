package grand.app.moondelegate.repository;

import com.android.volley.Request;

import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.settings.SettingsResponse;
import grand.app.moondelegate.models.user.UpdateTokenRequest;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;
import timber.log.Timber;


public class FirebaseRepository {
    SettingsResponse settingsResponse = null;
    StatusMsg statusMsg = null;

    public void updateToken(String token) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FIREBASE, new UpdateTokenRequest(token), StatusMsg.class);
    }

    public SettingsResponse getSettingsResponse() {
        return settingsResponse;
    }
}



