package grand.app.moondelegate.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.user.changepassword.ChangePasswordRequest;
import grand.app.moondelegate.models.user.changepassword.UpdatePasswordRequest;
import grand.app.moondelegate.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moondelegate.models.user.forgetpassword.ForgetPasswordResponse;
import grand.app.moondelegate.models.user.login.LoginRequest;
import grand.app.moondelegate.models.user.login.LoginResponse;
import grand.app.moondelegate.models.user.profile.ProfileRequest;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;
import grand.app.moondelegate.vollyutils.VolleyFileObject;

public class LoginRepository extends BaseRepository {
    LoginResponse loginResponse = null;
    ForgetPasswordResponse forgetPasswordResponse = null;

    public LoginRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void loginUser(LoginRequest loginRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status, loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
//                            if(loginResponse.data.packageId == 0)
//                                getmMutableLiveData().setValue(Constants.PACKAGES);
//                            else
                                getmMutableLiveData().setValue(Constants.HOME);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status, loginResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.LOGIN, loginRequest, LoginResponse.class);
    }

    public LoginResponse getUser() {
        return loginResponse;
    }


    public StatusMsg statusMsg;

    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.LOGIN);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.FORGET_PASSWORD, changePasswordRequest, StatusMsg.class);
    }


    public void checkPhone(ForgetPasswordRequest forgetPasswordRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    forgetPasswordResponse = (ForgetPasswordResponse) response;
                    if (forgetPasswordResponse != null) {
                        setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        if (forgetPasswordResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.CHECK_PHONE, forgetPasswordRequest, ForgetPasswordResponse.class);
    }

    public void forgetPassword(ForgetPasswordRequest forgetPasswordRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    forgetPasswordResponse = (ForgetPasswordResponse) response;
                    if (forgetPasswordResponse != null) {
                        setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        if (forgetPasswordResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.FORGET_PASSWORD, forgetPasswordRequest, ForgetPasswordResponse.class);
    }

    public void updateProfile(ProfileRequest profileRequest, VolleyFileObject volleyFileObject) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();
        volleyFileObjects.add(volleyFileObject);


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status, loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status, loginResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.UPDATE_PROFILE,profileRequest,volleyFileObjects, LoginResponse.class);
    }

    public void updateProfile(ProfileRequest profileRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status, loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status, loginResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PROFILE,profileRequest, LoginResponse.class);
    }

    public void updatePassword(UpdatePasswordRequest updatePasswordRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.UPDATE_PROFILE);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PROFILE, updatePasswordRequest, StatusMsg.class);
    }


    public ForgetPasswordResponse getForgetPasswordResponse() {
        return forgetPasswordResponse;
    }
}



