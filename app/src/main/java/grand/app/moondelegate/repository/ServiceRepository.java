package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.service.ServiceResponse;
import grand.app.moondelegate.models.service.ShopServiceMethod;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;


public class ServiceRepository extends BaseRepository {
    ServiceResponse serviceResponse = null;
    public ServiceRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getServices(int type_id) {

        ShopServiceMethod shopServiceMethod = new ShopServiceMethod(type_id);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    serviceResponse = (ServiceResponse) response;
                    if (serviceResponse != null) {
                        setMessage(serviceResponse.status,serviceResponse.msg);
                        if (serviceResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(serviceResponse.status,serviceResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SERVICES+"?account_type="+shopServiceMethod.account_type+"&type="+shopServiceMethod.type, shopServiceMethod, ServiceResponse.class);
    }


    public void setService(int type_id) {
        ShopServiceMethod shopServiceMethod = new ShopServiceMethod(type_id);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    serviceResponse = (ServiceResponse) response;
                    if (serviceResponse != null) {
                        setMessage(serviceResponse.status,serviceResponse.msg);
                        if (serviceResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(serviceResponse.status,serviceResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SERVICES+"?account_type="+shopServiceMethod.account_type+"&type="+shopServiceMethod.type, shopServiceMethod, ServiceResponse.class);
    }

    public ServiceResponse getServiceResponse() {
        return serviceResponse;
    }
}



