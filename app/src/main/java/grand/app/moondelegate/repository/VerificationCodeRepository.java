package grand.app.moondelegate.repository;

import android.util.Log;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.user.activation.VerificationRequest;
import grand.app.moondelegate.models.user.activation.VerificationResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;


public class VerificationCodeRepository extends BaseRepository {
    VerificationResponse verificationResponse = null;
    private static final String TAG = "VerificationCodeReposit";
    public VerificationCodeRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);

    }

    public void request(VerificationRequest verificationRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                Log.e(TAG, "onRequestSuccess: Start");
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                if(!catchErrorResponse(response)) {
                    verificationResponse = (VerificationResponse) response;
                    if (verificationResponse != null) {
                        setMessage(verificationResponse.status, verificationResponse.msg);
                        if (verificationResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(verificationResponse.data);
                            UserHelper.clearUserId();
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(verificationResponse.status,verificationResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.CODE_CHECK, verificationRequest, VerificationResponse.class);
    }


    public VerificationResponse response() {
        return verificationResponse;
    }
}



