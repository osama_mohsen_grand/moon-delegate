package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.credit.CreditResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;


public class CreditRepository extends BaseRepository {
    CreditResponse creditResponse = null;
    public CreditRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
        getCredit();
    }

    public void getCredit() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    creditResponse = (CreditResponse) response;
                    if (creditResponse != null) {
                        setMessage(creditResponse.status,creditResponse.msg);
                        if (creditResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOPS_CREDIT_CARD+"?type="+ UserHelper.getUserDetails().type, null, CreditResponse.class);
    }

    public CreditResponse getCreditResponse() {
        return creditResponse;
    }
}



