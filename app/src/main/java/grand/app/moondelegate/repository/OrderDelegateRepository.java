package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.order.OrderConfirmRequest;
import grand.app.moondelegate.models.order.list.OrdersDelegateListResponse;
import grand.app.moondelegate.models.order.details.OrderDetailsResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;


public class OrderDelegateRepository extends BaseRepository {
    OrdersDelegateListResponse ordersDelegateListResponse = null;
    OrderDetailsResponse orderDetailsResponse = null;
    StatusMsg statusMsg = null;
    public OrderDelegateRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getOrders(int status) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    ordersDelegateListResponse = (OrdersDelegateListResponse) response;
                    if (ordersDelegateListResponse != null) {
                        setMessage(ordersDelegateListResponse.status,ordersDelegateListResponse.msg);
                        if (ordersDelegateListResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(ordersDelegateListResponse.status,ordersDelegateListResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.ORDER_DELEGATE+"?type="+ UserHelper.getUserDetails().type+"&status="+status, null, OrdersDelegateListResponse.class);
    }

    public void getOrderDetails(int order_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    orderDetailsResponse = (OrderDetailsResponse) response;
                    if (orderDetailsResponse != null) {
                        setMessage(orderDetailsResponse.status,orderDetailsResponse.msg);
                        if (orderDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ORDER_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(ordersDelegateListResponse.status,ordersDelegateListResponse.msg);

                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.ORDER_DETAILS+"?order_id="+ order_id+"&type="+UserHelper.getUserDetails().type, null, OrderDetailsResponse.class);
    }


    public void confirmOrder(OrderConfirmRequest orderConfirmRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ORDER_SUBMIT);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(ordersDelegateListResponse.status,ordersDelegateListResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.ORDER_CONFIRM, orderConfirmRequest, StatusMsg.class);
    }


    public OrdersDelegateListResponse getOrdersDelegateListResponse() {
        return ordersDelegateListResponse;
    }

    public OrderDetailsResponse getOrderDetailsResponse() {
        return orderDetailsResponse;
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }
}



