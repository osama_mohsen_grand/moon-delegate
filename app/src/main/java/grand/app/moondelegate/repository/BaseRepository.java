package grand.app.moondelegate.repository;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.utils.Constants;

public class BaseRepository {
    private int status = 0;
    private String message = "";
    private MutableLiveData<Object> mMutableLiveData;
    private static final String TAG = "BaseRepository";

    public BaseRepository(MutableLiveData<Object> mMutableLiveData) {
        this.mMutableLiveData = mMutableLiveData;
    }

    public boolean catchErrorResponse(Object response){
        if (response instanceof VolleyError) {
            VolleyError volleyError = (VolleyError) response;
            message = volleyError.getMessage();
            if (mMutableLiveData != null) {
                mMutableLiveData.setValue(Constants.HIDE_PROGRESS);
                if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    mMutableLiveData.setValue(Constants.SERVER_ERROR);
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError || volleyError instanceof NetworkError || volleyError instanceof AuthFailureError || volleyError instanceof TimeoutError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    mMutableLiveData.setValue(Constants.FAILURE_CONNECTION);
                }
            }

            return true;
        }
        return false;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }


    public void setMessage(int status, String message) {
        this.status = status;
        this.message = message;
        if(status == Constants.RESPONSE_JWT_EXPIRE){
            if(mMutableLiveData != null) mMutableLiveData.setValue(Constants.LOGOUT);
        }
    }

    public MutableLiveData<Object> getmMutableLiveData() {
        return mMutableLiveData;
    }
}
