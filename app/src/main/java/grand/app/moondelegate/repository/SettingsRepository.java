package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.contact.ContactUsRequest;
import grand.app.moondelegate.models.settings.SettingsRequest;
import grand.app.moondelegate.models.settings.SettingsResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;


public class SettingsRepository extends BaseRepository {
    SettingsResponse settingsResponse = null;
    public SettingsRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getSettings(int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    settingsResponse = (SettingsResponse) response;
                    if (settingsResponse != null) {
                        setMessage(settingsResponse.status,settingsResponse.msg);
                        if (settingsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(settingsResponse.status,settingsResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.SETTINGS, new SettingsRequest(type), SettingsResponse.class);
    }

    StatusMsg statusMsg;

    public void contactUs(ContactUsRequest request) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CONTACT_US, request, StatusMsg.class);
    }


    public SettingsResponse getSettingsResponse() {
        return settingsResponse;
    }
}



