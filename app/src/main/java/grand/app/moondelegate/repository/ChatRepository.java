package grand.app.moondelegate.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.chat.ChatDetailsResponse;
import grand.app.moondelegate.models.chat.ChatRequest;
import grand.app.moondelegate.models.chat.ChatSendResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;
import grand.app.moondelegate.vollyutils.VolleyFileObject;


public class ChatRepository extends BaseRepository {
    ChatDetailsResponse chatDetailsResponse;
    ChatSendResponse chatSendResponse;
    public ChatRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void getChatDetails(int id, int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatDetailsResponse = (ChatDetailsResponse) response;
                    if (chatDetailsResponse != null) {
                        setMessage(chatDetailsResponse.status,chatDetailsResponse.msg);
                        if (chatDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(chatDetailsResponse.status,chatDetailsResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.CHAT_DETAILS+"?order_id="+id+"&type="+ AppMoon.getUserType()+"&receiver_type="+type, null, ChatDetailsResponse.class);
    }

    public void send(ChatRequest chatRequest, VolleyFileObject volleyFileObject) {
        ArrayList<VolleyFileObject> volleyFileObjects = null;
        if(volleyFileObject != null){
            volleyFileObjects = new ArrayList<>();
            volleyFileObjects.add(volleyFileObject);
        }
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatSendResponse = (ChatSendResponse) response;
                    if (chatSendResponse != null) {
                        setMessage(chatSendResponse.status,chatSendResponse.msg);
                        if (chatSendResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT_SEND);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.CHAT_SEND, chatRequest,volleyFileObjects, ChatSendResponse.class);
    }

    public ChatDetailsResponse getChatDetailsResponse() {
        return chatDetailsResponse;
    }

    public ChatSendResponse getChatSendResponse() {
        return chatSendResponse;
    }
}



