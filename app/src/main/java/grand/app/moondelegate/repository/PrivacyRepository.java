package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.app.PrivacyResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;


public class PrivacyRepository extends BaseRepository {
    PrivacyResponse privacyResponse = null;
    public PrivacyRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
        privacy();
    }
    public void privacy() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    privacyResponse = (PrivacyResponse) response;
                    if (privacyResponse != null) {
                        setMessage(privacyResponse.status,privacyResponse.msg);
                        if (privacyResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.PRIVACY_POLICY);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(privacyResponse.status,privacyResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.REGISTER_USER, null, PrivacyResponse.class);
    }

    public PrivacyResponse getPrivacyResponse() {
        return privacyResponse;
    }
}



