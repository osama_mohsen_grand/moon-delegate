package grand.app.moondelegate.repository;

import android.util.Log;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.location.LocationRequest;
import grand.app.moondelegate.models.user.UpdateTokenRequest;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;

public class LocationRepository extends BaseRepository {
    StatusMsg statusMsg = null;
    private static final String TAG = "LocationRepository";
    public LocationRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void updateLocation(UpdateTokenRequest updateTokenRequest, boolean isForeground) {
        if(isForeground) getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    if(isForeground) getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
//                            Driver driver = UserHelper.getUserDetails();
//                            driver.available = locationRequest.available;
//                            UserHelper.saveUserDetails(driver);
//                            Log.e(TAG, "update_location Done"+UserHelper.getUserDetails());
                        }else {
                            if(isForeground){
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            }
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.FIREBASE, updateTokenRequest, StatusMsg.class);
    }
}



