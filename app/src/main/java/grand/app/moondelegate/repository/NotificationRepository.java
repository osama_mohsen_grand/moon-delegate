package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.base.UserId;
import grand.app.moondelegate.models.notifications.NotificationResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;

public class NotificationRepository extends BaseRepository {
    NotificationResponse notificationResponse = null;
    public NotificationRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getNotification() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    notificationResponse = (NotificationResponse) response;
                    if (notificationResponse != null) {
                        setMessage(notificationResponse.status,notificationResponse.msg);
                        if (notificationResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.NOTIFICATION);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(notificationResponse.status,notificationResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.NOTIFICATION+"?type="+UserHelper.getUserDetails().type, null, NotificationResponse.class);
    }

    public NotificationResponse getData() {
        return notificationResponse;
    }
}



