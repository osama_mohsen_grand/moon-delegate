package grand.app.moondelegate.repository;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.base.StatusMsg;
import grand.app.moondelegate.models.user.activation.VerificationResponse;
import grand.app.moondelegate.models.user.login.LoginResponse;
import grand.app.moondelegate.models.user.register.RegisterRequest;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.utils.upload.FileOperations;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;
import grand.app.moondelegate.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class RegisterRepository extends BaseRepository {
    StatusMsg statusMsg = null;

    public RegisterRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void registerUser(RegisterRequest registerRequest, ArrayList<String> imagesKeys, ArrayList<String> imagesPath) {

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();


        for (int i = 0; i < imagesKeys.size(); i++) {
            Timber.e("key:" + imagesKeys.get(i) + ",value:" + imagesPath.get(i));


            volleyFileObjects.add(FileOperations.getVolleyFileObject(imagesKeys.get(i), imagesPath.get(i), Constants.FILE_TYPE_IMAGE));
        }


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.REGISTRATION);
                        } else {
                            Timber.e("status"+statusMsg.status);
                            Timber.e("msg"+statusMsg.msg);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.REGISTER, registerRequest, volleyFileObjects, StatusMsg.class);
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

}



