package grand.app.moondelegate.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moondelegate.models.country.CountriesResponse;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.vollyutils.ConnectionHelper;
import grand.app.moondelegate.vollyutils.ConnectionListener;
import grand.app.moondelegate.vollyutils.URLS;


public class CountryRepository extends BaseRepository {
    private CountriesResponse countriesResponse = null;
    public CountryRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getCountries(boolean showProgress) {
        if(showProgress) getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    if(showProgress) getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    countriesResponse = (CountriesResponse) response;
                    if (countriesResponse != null) {
                        setMessage(countriesResponse.status,countriesResponse.msg);
                        if (countriesResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveCountries(countriesResponse);
                            getmMutableLiveData().setValue(Constants.COUNTRIES);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(countriesResponse.status,countriesResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.GET_ALL_COUNTRIES, null, CountriesResponse.class);
    }

    public CountriesResponse getCountriesResponse() {
        return countriesResponse;
    }
}



