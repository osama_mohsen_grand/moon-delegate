package grand.app.moondelegate.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.RequiresApi;
import grand.app.moondelegate.R;
import grand.app.moondelegate.utils.Constants;


public class NotificationHelper extends ContextWrapper {

    public static String CHANNEL_NAME = "";
    public static String CHANNEL_DESCRIPTION = "";
    public NotificationManager manager = null;

    public NotificationHelper(Context base) {
        super(base);
        CHANNEL_NAME = base.getString(R.string.app_name);
        CHANNEL_DESCRIPTION = base.getString(R.string.app_name);
    }


    public void createChannels(String title , String message , Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, importance);
            mChannel.setDescription(Constants.CHANNEL_DESCRIPTION);
            mChannel.setAllowBubbles(true);
            mChannel.setShowBadge(true);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            getManager().createNotificationChannel(mChannel);
            MyNotificationManager.getInstance(this).displayNotification(title, message,intent);

        }
    }


    public NotificationManager getManager() {
        if(manager == null)
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return manager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getChannel(String title, String body, PendingIntent pendingIntent,int number){
        return new Notification.Builder(getApplicationContext(), Constants.CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setContentIntent(pendingIntent)
                .setFullScreenIntent(pendingIntent,true)
                .setNumber(number)
//                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT))
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true);
    }
}
