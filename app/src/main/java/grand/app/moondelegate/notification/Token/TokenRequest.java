package grand.app.moondelegate.notification.Token;

import com.google.gson.annotations.SerializedName;

/**
 * Created by osama on 21/01/2018.
 */

public class TokenRequest {
    @SerializedName("notification_token")
    public String notification_token;

    public TokenRequest( String notification_token) {
        this.notification_token = notification_token;
    }
}
