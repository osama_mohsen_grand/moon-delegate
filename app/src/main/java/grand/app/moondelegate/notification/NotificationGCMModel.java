package grand.app.moondelegate.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationGCMModel implements Serializable {

    /*
        type:
        1: new order
        2: chat

        type_data forChat
        1: message
        2: image
        3: else
     */

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("body")
    @Expose
    public String body;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("audio")
    @Expose
    public String audio;
    @SerializedName("notification_type")
    @Expose
    public int notification_type;
    @SerializedName("type")
    @Expose
    public int type;
    @SerializedName("sender_id")
    @Expose
    public String senderId;
    @SerializedName("order_id")
    @Expose
    public int order_id;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("type_data")
    @Expose
    public int type_data;
    @SerializedName("receiver_type")
    @Expose
    public int receiver_type;
    @SerializedName("sender_type")
    @Expose
    public int sender_type;
}
