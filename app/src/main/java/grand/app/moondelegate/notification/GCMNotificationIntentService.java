package grand.app.moondelegate.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

import androidx.core.app.NotificationCompat;
import grand.app.moondelegate.R;
import grand.app.moondelegate.models.app.AppMoon;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.resources.ResourceManager;
import grand.app.moondelegate.utils.storage.SharedPreferenceHelper;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import grand.app.moondelegate.views.activities.BaseActivity;
import grand.app.moondelegate.views.activities.TrackingActivity;
import timber.log.Timber;

import static android.app.NotificationManager.IMPORTANCE_HIGH;


public class GCMNotificationIntentService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        SharedPreferenceHelper.saveKey("token",s);
        Log.e("token", s);
    }


    private static final String TAG = "GCMNotificationIntentSe";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //ar_text , en_text
        Timber.e("notification: start Here");
        if(UserHelper.getUserId() != -1) {
            String details = remoteMessage.getData().get(Constants.MESSAGE);
            NotificationGCMModel notificationGCMModel = AppMoon.getNotification(details);
            String title = notificationGCMModel.title, message = notificationGCMModel.body;
            Intent intent =  null;
            if(notificationGCMModel.notification_type == Integer.parseInt(Constants.DEFAULT_TRANSPORTATION) || notificationGCMModel.notification_type == Integer.parseInt(Constants.DEFAULT_TAXI)){
                intent =  new Intent(this, TrackingActivity.class);
                intent.putExtra(Constants.TRIP_ID,notificationGCMModel.order_id);
                intent.putExtra(Constants.TYPE,notificationGCMModel.notification_type);
            }else{
                intent =  new Intent(this, BaseActivity.class);;
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_NOTIFICATION,notificationGCMModel);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION,bundle);
            sendBroadCast(notificationGCMModel);

            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder notification = new NotificationCompat.Builder(this);

            notification.setColor(getResources().getColor(R.color.colorPrimary));
            notification.setPriority(IMPORTANCE_HIGH);
            notification.setContentTitle(title);
            notification.setColor(getResources().getColor(R.color.colorPrimary));
            notification.setContentText(message);
            notification.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            notification.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            //        notification.setAutoCancel(false);
            notification.setAutoCancel(true);
            notification.setShowWhen(true);


//            try {
//                Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), uri);
//                r.play();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            notification.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
            notification.setContentIntent(pendingIntent);

            int currentApiVersion = Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
                notification
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
            } else {
                notification.setSmallIcon(R.mipmap.ic_launcher);
            }

            int random = new Random().nextInt(10000000) + 1;
            //Android O
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationHelper helper = new NotificationHelper(getApplicationContext());
//            helper.createChannels(title, message, intent);
//                helper.createChannels(title, message, intent);
                Notification.Builder builder = helper.getChannel(title, message, pendingIntent,random);
                helper.getManager().notify(random, builder.build());
            }else
                notificationManager.notify(random, notification.build());
        }

    }

    private void sendBroadCast(NotificationGCMModel notificationGCMModel) {
        Intent intent = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_NOTIFICATION,notificationGCMModel);
        if(notificationGCMModel.notification_type == 1){//new order
            intent = new Intent(Constants.NEW_ORDER);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION,bundle);
            getApplicationContext().sendBroadcast(intent);
        }else if(notificationGCMModel.notification_type == 2){//chat
            intent = new Intent(Constants.CHAT);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION,bundle);
            getApplicationContext().sendBroadcast(intent);
        }else if(notificationGCMModel.notification_type == Integer.parseInt(Constants.DEFAULT_TRANSPORTATION) || notificationGCMModel.notification_type == Integer.parseInt(Constants.DEFAULT_TAXI)){
            intent = new Intent(Constants.NEW_ORDER_TRACKING);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION,bundle);
            getApplicationContext().sendBroadcast(intent);
        }
    }

}
