package grand.app.moondelegate.vollyutils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import grand.app.moondelegate.R;
import grand.app.moondelegate.retrofitutils.RetrofitConnectionHelper;
import grand.app.moondelegate.utils.Constants;
import grand.app.moondelegate.utils.LanguagesHelper;
import grand.app.moondelegate.utils.storage.user.UserHelper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;


//here you can find functions that relate to web_service or make connections in other way here you find functions that send and recive data from  and to server

public class ConnectionHelper {
    private static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.color.colorWhite)
            .showImageOnLoading(R.color.colorWhite)
            .showImageOnFail(R.color.colorWhite)
            .cacheInMemory(true)
            .cacheOnDisk(true).build();
    private static ImageLoader imageLoader = ImageLoader.getInstance();
    private ConnectionListener connectionListener;
    private RequestQueue queue;
    private static final int TIME_OUT = 10000;
    private Gson gson;

    private static final String TAG = "ConnectionHelper";

    public ConnectionHelper(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
        queue = MyApplication.getInstance().getRequestQueue();
        gson = new Gson();
    }

    public void requestJsonObject(int method, String url, Object requestData, final Class<?> responseType) {
        final Gson gson = new Gson();
        String link = URLS.BASE_URL + url;
//        if(method != Request.Method.GET)
            link = link.replaceAll(" ", "%20");
        JSONObject jsonObject = null;
        try {
//            if(method != Request.Method.GET)
                jsonObject = new JSONObject(gson.toJson(requestData));
        } catch (Exception e) {
            e.getStackTrace();
        }
        Log.e(TAG, link);
        if (jsonObject != null) {
            Log.e("Request :", jsonObject.toString());
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(method, link, jsonObject,
                response -> {
                    Timber.e(response.toString());
                    parseData(response, responseType);
                }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse: " + volleyError.getMessage());
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (message != null) {
                    Timber.e("errorConnectionURL: "+url);
                    Timber.e("errorConnectionURL: "+message);
                    Toast.makeText(MyApplication.getInstance(), "" + message, Toast.LENGTH_SHORT).show();

                }
                showErrorDetails(volleyError);
                connectionListener.onRequestError(volleyError);
            }
        }) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                return getCustomHeaders();
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    private MultipartBody prepareImages(List<VolleyFileObject> volleyFileObjects) {
        MultipartBody requestBody = null;
        ArrayList<File> files = new ArrayList<>();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        Timber.e("size:"+volleyFileObjects.size());
        for (VolleyFileObject volleyFileObject : volleyFileObjects) {
            File file = volleyFileObject.getFile();
            if(volleyFileObject.getFileType() == Constants.FILE_TYPE_IMAGE) {
                Timber.e("type:"+Constants.FILE_TYPE_IMAGE);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                OutputStream os = null;
                try {
                    os = new BufferedOutputStream(new FileOutputStream(volleyFileObject.getFilePath()));
                    volleyFileObject.getBitmap().compress(Bitmap.CompressFormat.JPEG, 50, os);
                    os.close();
                    if (file.exists()) {
                        files.add(file);
                        Timber.e("KeyNameImage:"+volleyFileObject.getParamName());
                        builder.addFormDataPart(volleyFileObject.getParamName(),
                                volleyFileObject.getParamName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                builder.addFormDataPart(volleyFileObject.getParamName(),
                        volleyFileObject.getParamName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
            }
        }
        if (files.size() > 0)
            requestBody = builder.build();
        return requestBody;
    }

    public void multiPartConnect(String url, final Object requestData,
                                 final List<VolleyFileObject> volleyFileObjects, final Class<?> responseType) {
        String link = URLS.BASE_URL + url;
        Log.e(TAG, link);
        Map<String, String> map = getParameters(requestData);
        Call<JsonObject> call = null;
        MultipartBody.Part file = null;
        if (volleyFileObjects != null && volleyFileObjects.size() == 1) {
            File myFile = volleyFileObjects.get(0).getFile();
            RequestBody mFile = null;
            if(volleyFileObjects.get(0).getFileType() == Constants.FILE_TYPE_IMAGE) {
                mFile = RequestBody.create(MediaType.parse("image/*"), myFile);
            }else{
                mFile = RequestBody.create(MediaType.parse("video/*"),myFile);
            }
            file = MultipartBody.Part.createFormData(volleyFileObjects.get(0).getParamName(), myFile.getName(), mFile);
            call = RetrofitConnectionHelper.webService().upload(url, map, file);
        } else if (volleyFileObjects != null &&volleyFileObjects.size() > 1) {
            MultipartBody multipartBody = prepareImages(volleyFileObjects);
            if (multipartBody != null) {
                call = RetrofitConnectionHelper.webService().upload(url, map, multipartBody);
            } else {
                Timber.e("error in prepare images to upload");
                call = RetrofitConnectionHelper.webService().upload(url, map);
            }
        } else {
            call = RetrofitConnectionHelper.webService().upload(url, map);
        }
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Timber.e(TAG + ":response fetch to done");
                Gson gson = new Gson();
                String jsonString = gson.toJson(response.body());
                connectionListener.onRequestSuccess(gson.fromJson(jsonString, responseType));
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Timber.e(TAG + ":response fetch to error,"+t.getMessage());
                connectionListener.onRequestSuccess(t);
            }
        });
    }

    public void multiPartConnect(String url, final HashMap<String, String> requestData, final List<VolleyFileObject> volleyFileObjects, final Class<?> responseType) {
        String link = URLS.BASE_URL + url;
        Log.e(TAG, "url: " + link);
        link = link.replaceAll(" ", "%20");
        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, link, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse networkResponse) {
                String responseString = new String(networkResponse.data);
                JSONObject response = null;
                try {
                    response = new JSONObject(responseString);
                } catch (Exception e) {
                    e.getStackTrace();
                }
                parseData(response, responseType);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showErrorDetails(volleyError);
                connectionListener.onRequestError(volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return requestData;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getCustomHeaders();
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                return getFileParameters(volleyFileObjects);
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(multipartRequest);
    }

    private HashMap getCustomHeaders() {
        HashMap headers = new HashMap();
        headers.put("jwt", UserHelper.getUserDetails().jwtToken);
        headers.put("lang", LanguagesHelper.getCurrentLanguage());
        Log.e(TAG, "jwt: " + UserHelper.getUserDetails().jwtToken);
        Log.e(TAG, "lang: " + LanguagesHelper.getCurrentLanguage());
        return headers;
    }

    public static void loadImage(final ImageView image, String imageUrl) {
        Log.d("ImageVideo Url", "" + imageUrl);
        imageLoader.displayImage(imageUrl, image, options);
    }

    private void showErrorDetails(VolleyError volleyError) {
        String body;
        try {
            final String statusCode = String.valueOf(volleyError.networkResponse.statusCode);
            body = new String(volleyError.networkResponse.data, "UTF-8");
            Log.e("TAG", "Error Body " + body + " StatusCode " + statusCode);
        } catch (Exception e) {
            Log.e(TAG, "showErrorDetails_update: " + e.getMessage());
            e.getStackTrace();
        }
        connectionListener.onRequestSuccess(volleyError);
    }

    private void parseData(JSONObject response, final Class<?> responseType) {
        try {
            if (response.toString().equals("")) {
                connectionListener.onRequestError(null);
            } else {
                connectionListener.onRequestSuccess(gson.fromJson(response.toString(), responseType));
            }
        } catch (Exception e) {
            Log.e(TAG, "parseData: " + e.getMessage());
            connectionListener.onRequestError(e.getMessage());
        }
    }

    private Map<String, String> getParameters(final Object requestData) {
        Map<String, String> params = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject(gson.toJson(requestData));
            for (int i = 0; i < jsonObject.names().length(); i++) {
                params.put(jsonObject.names().getString(i), jsonObject.get(jsonObject.names().getString(i)) + "");
                Log.e("PARAMS", jsonObject.names().getString(i)+":"+jsonObject.get(jsonObject.names().getString(i)) + "");
            }
            Log.e("PARAMS", params.size() + "");
        } catch (Exception e) {
            Log.e("PARAMS", e.getStackTrace() + "");
            e.getStackTrace();
        }
        return params;
    }

    private Map<String, VolleyMultipartRequest.DataPart> getFileParameters(List<VolleyFileObject> volleyFileObjects) {
        Map<String, VolleyMultipartRequest.DataPart> filesParams = new HashMap<>();
        if (volleyFileObjects == null) {
            return filesParams;
        }
        for (int i = 0; i < volleyFileObjects.size(); i++) {
            final File filePath = new File(volleyFileObjects.get(i).getFilePath());
//            Timber.e("bytes:"+volleyFileObjects.get(i).getBitmap().getByteCount());
            filesParams.put(volleyFileObjects.get(i).getParamName(), new VolleyMultipartRequest.DataPart(filePath.getName(), volleyFileObjects.get(i).getBytes()));
        }
        Log.e("PARAMS", filesParams.size() + "");
        //volleyFileObjects.clear();
        return filesParams;
    }
}