package grand.app.moondelegate.vollyutils;

/**
 * Created by mohamedatef on 12/29/18.
 */

public class URLS {


    public static final String ORDER_DELEGATE = "order/user/list";
    public static String BASE_URL = "http://www.moon4online.com/api/";

    public static String FIREBASE = "firebase";
    public static String CONTACT_US = "contact-us";

    public static String LOGIN = "login";

    public static String REGISTER_USER = "register";

    public static String REGISTER_SHOP = "shops/addShop";

    public static String GET_ALL_COUNTRIES = "getCountries";

    public static String CODE_SEND = "driver/code_send";

    public static String FORGET_PASSWORD = "checkPhone";
    public static String CHANGE_PASSWORD = "changeForgetPassword";

    public static String HOME = "services";
    public static String CHECK_PHONE = "checkPhone";


    public static String SERVICE_DETAILS = "shops_list";

    public static final String TAGS = "tags";
    public static final String DISCOVER = "discover";

    public static String ADD_CATEGORY = "shops/addCategory";

    public static String DELETE_CATEGORY = "shops/deleteCategory";

    public static String SHOP_SIZE_COLOR = "shops/SizesColors";
    public static String REGISTER = "register";

    public static String NOTIFICATION = "notification";
    public static String DRIVER_HOME = "taxi/driver/home";



    public static String TRIP_ACTION = "taxi/driver/accept_trip";
    public static String UPDATE_LOCATION = "taxi/driver/calculate_trip";
    public static String SHOP_DETAILS = "shopDetails";
    public static String SHOP_PRODUCTS = "getProductFromCategory";
    public static String SHOP_PRODUCT_DETAILS = "getDetailsProduct";
    public static String SHOP_FOLLOW = "create_follow";


    public static String SERVICES = "shops_services";


    public static String FAMOUS = "famous";

    public static String FAMOUS_ADD_ALBUM = "famous/addAlbum";

    public static String FAMOUS_ALBUMS = "famous/albums";

    public static String FAMOUS_FILTER = "famous/filter";

    public static String FAMOUS_FILTER_SHOP_ADS = "famous/filterShopAdds";

    public static String ALBUM_IMAGES = "famous/albumsImages";

    public static String FAMOUS_SEARCH_SHOP = "famous/searchShop";

    public static String FAMOUS_DETAILS = "famous/details";

    public static String FAMOUS_FOLLOW = "famous/create_follow";

    public static String FAMOUS_DELETE_IMAGE = "famous/deleteImage";

    public static String FAMOUS_EDIT_ALBUM = "famous/edit_album";

    public static String FAMOUS_DISCOVER = "famous/discover";

    public static String FAMOUS_ALBUM_SEARCH = "famous/searchAlbum";

    public static String FAMOUS_SEARCH = "famous/searchFamous";

    public static String SHOPS_CREDIT_CARD = "shops/creditCard";

    public static final String BE_SHOP = "famous/addShop";

    public static String ADVERTISE_DETAILS = "famous/advertisement_details";


    public static String UPDATE_PROFILE = "profile/edit";

    public static String PACKAGES = "packages";

    public static String ADD_PACKAGE = "addPackage";

    public static String PRODUCTS = "shops/products";

    public static String PRODUCT_DETAILS = "shops/productDetails";

    public static String ADD_PRODUCT = "shops/addProduct";

    public static String EDIT_PRODUCT = "shops/editProduct";

    public static String DELETE_PRODUCT = "shops/deleteProduct";

    public static String STATUS_LIST = "shops/Status";

    public static String ADD_STATUS = "shops/addStatus";

    public static String DELETE_STATUS = "shops/deleteStatus";

    public static String SETTINGS = "settings";

    public static String TRIP_HISTORY = "taxi/trip_history";


    public static String OFFERS = "shops/offers";

    public static String DELETE_OFFER = "shops/deleteOffer";

    public static String ADD_OFFER = "shops/addOffer";

    public static String SHOP_REVIEW = "userReviewShop";

    public static String SHOP_ADD_REVIEW = "addReviewShop";

    public static String TRIP_ARRIVE = "driver/arrive";

    public static String TRIP_CANCEL_AFTER_ACCEPT = "driver/cancel_trip_after_accept";

    public static String TRIP_DETAILS = "taxi/trip_details";

    public static String CODE_CHECK = "driver/code_check";

    public static String RATE = "taxi/driver/rate";

    public static String RATE_TRIP = "taxi/rate";

    public static String ORDER_CONFIRM = "order/delegate/confirmOrder";

    public static String ORDER_SHIPPING = "user/getShippingCompany";

    public static String ORDER_LIST = "order/user/list";

    public static String ORDER_DETAILS = "order/user/orderDetails";



    public static String CART_ADD = "user/cart/add_or_update";

    public static String CART_DELETE = "user/cart/delete";

    public static String CART_USER = "user/cart";

    public static String CART_SHIPPING = "user/cart";

    public static String CREDIT = "driver/credit";

    public static String UPDATE_PAYMENT = "driver/update_payment";

    public static String CHAT_GET = "chat/all_chats";

    public static String CHAT_DETAILS = "order/user/chat";

    public static String CHAT_SEND = "order/user/orderChat";


}
