package grand.app.moondelegate.vollyutils;


import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.multidex.BuildConfig;
import androidx.multidex.MultiDexApplication;
import grand.app.moondelegate.base.ApplicationComponent;
import grand.app.moondelegate.utils.images.ImageLoaderHelper;
import timber.log.Timber;

/**
 * Created by Akshay Raj on 7/17/2016.
 * Snow Corporation Inc.
 * www.snowcorp.org
 */
public class MyApplication extends MultiDexApplication {
    public static final String TAG = MyApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        new ImageLoaderHelper(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }
        DataBindingUtil.setDefaultComponent(new ApplicationComponent());

        /*
        new ScaleTouchListener() {
            @Override
            public void onClick(View v) {
                animationSubmit.animationSubmit();
            }
        }
         */

    }


    private static class CrashReportingTree extends Timber.Tree {
        @Override protected void log(int priority, String tag, @NonNull String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }
        }
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

}